import requests
import time
import csv
from bs4 import BeautifulSoup as bs
from bs4 import UnicodeDammit

def gen_date(title):
    if "/" in title:
        return title.split("/")[1].strip()
    if "\\" in title:
        return title.split("\\")[1].strip()
    return ""

def collectYear(year, start, end):
    counter = start
    errors = []
    with open('out/' + str(year) + '.csv','w',newline='') as f:
        f_writer = csv.writer(f, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        f_writer.writerow(["lrID","Title","Date","Body","URL"])
        while counter <= end:
            url = 'https://www.sec.gov/litigation/litreleases/lr' + str(counter) + ".htm"
            if year == 17859:
                url = 'https://www.sec.gov/litigation/litreleases/lr' + str(counter) + "a.htm"
            response = requests.get(url)
            soup = bs(response.text, features="lxml")
            if soup.find("font"):
                top = soup.find("font").text.strip()
                title = soup.title.text.strip()
                date = gen_date(title)
                if len(date) < 5:
                    date = str(year)
                else:
                    date = str(year)
                try:
                    f_writer.writerow([counter,title,date,top,url])
                except:
                    top = UnicodeDammit(top, ["windows-1252"], smart_quotes_to="ascii").unicode_markup
                    title = UnicodeDammit(title, ["windows-1252"], smart_quotes_to="ascii").unicode_markup
                    try:
                        f_writer.writerow([counter,title,date,top,url])
                    except:
                        title = bytes(title, 'utf-8').decode('ascii','ignore')
                        top = bytes(top, 'utf-8').decode('ascii','ignore')
                        try:
                            f_writer.writerow([counter,title,date,top,url])
                        except:
                            errors.append(title)
            counter+=1
            time.sleep(1)
    if len(errors) > 0:
        print("There were errors with the following:")
        for i in errors:
            print(i)

collectYear(2006, 19517, 19955)