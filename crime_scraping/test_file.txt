==========================================START OF PAGE 1======

                SECURITIES AND EXCHANGE COMMISSION
                         Washington, D.C.

LITIGATION RELEASE NO. 15203 / January 2, 1997

SECURITIES AND EXCHANGE COMMISSION v. HARRY C. MORGAN, JOHN G.
MARTINES AND JOHN F. WALSH, III, Civil Action No. 3:CV97-0001
(M.D. Pa.) (filed January 2, 1997)

     On January 2, 1997, the Commission filed a complaint in the
United States District Court for the Middle District of
Pennsylvania against Harry C. Morgan of Scranton, Pennsylvania,
John G. Martines of Carbondale, Pennsylvania, and John F. Walsh,
III of Dalton, Pennsylvania, alleging insider trading in the
securities of First Eastern Corporation (""First Eastern"") prior
to the announcement on July 27, 1993 by PNC Bank Corp. (""PNC"")
that PNC would purchase First Eastern.  

     The Commission's complaint alleges that Morgan, a director
of First Eastern, tipped a long-time friend who realized profits
of $25,093, and communicated information about the First Eastern
merger to Martines at a dinner held on July 23, 1993 at the
Scranton Country Club in Scranton, Pennsylvania.  The complaint
alleges that Martines executed a purchase of First Eastern stock,
earning a profit of $7,175, and also caused three additional
purchases of First Eastern stock by friends who earned a total
profit of $16,342.  The complaint further alleges that Walsh, a
broker and supervisor of the Clarks Summit, Pennsylvania office
of Rutherford, Brown & Catherwood (""Rutherford""), made purchases
of First Eastern stock for himself and several clients after
learning about Morgan's tip to Martines from Martines' broker at
Rutherford.  The total profit from these trades was $21,228.

     The complaint seeks injunctions against Morgan, Martines and
Walsh for violations of Section 10(b) of the Securities Exchange
Act of 1934 (""Exchange Act"") and Rule 10b-5 therunder, as well as
disgorgement of the profits earned by the defendants and their
tippees, and civil penalties pursuant to the Insider Trading and
Sanctions Act of 1984.  Simultaneous with the filing of the
complaint, without admitting or denying the allegations of the
complaint, Morgan and Martines consented to the entry of Final
Judgments permanently enjoining each of them from violating
Section 10(b) of the Exchange Act, and Rule 10b-5 therunder,
ordering Morgan to pay $25,093 in disgorgement, $7,987 in
prejudgment interest and a $48,610 penalty for a total amount of
$81,690, and ordering Martines to pay $23,517.00 in disgorgement,
$7,485.22 in prejudgment interest, and a $23,517.00 penalty for a
total amount of $54,519.22.

     The Commission's investigation is continuing