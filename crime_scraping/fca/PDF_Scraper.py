# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 09:35:14 2019

@author: AaronBridgeman
"""

import requests
import time
import csv
from bs4 import BeautifulSoup as bs
#from bs4 import UnicodeDammit

def collect_PDF(start, end):
    counter = start
    pdf_url = []
    date = []
    body = []
    title = []
    with open('Documents/FCA_PDF_.csv','w',newline='') as f:
        f_writer = csv.writer(f, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        f_writer.writerow(["Date","PDF_URL","TITLE","BODY"])
        while counter <= end:
            url = ("https://www.fca.org.uk/publications/search-results?np_category=notices%20and%20decisions-final%20notices&start=" + str(counter))
            response = requests.get(url)
            soup = bs(response.text, features="lxml")
        #print(soup.text)
            for b in soup.find_all("div", {"class":"search-item__body"}):
                body.append(b.text.strip())
            for el in soup.find_all("a", {"class": "search-item__clickthrough"}):
                pdf_url.append(el["href"])
            for el in soup.find_all("a", {"class": "search-item__clickthrough"}):
                title.append(el.text.strip()[:-6])
            for d in soup.find_all("span", {"class":"meta-item published-date"}):
                #print(d.get_text())                
                date.append(d.text.strip()[10:])
            counter += 10       
            time.sleep(1)

            
            
        print(date, pdf_url, body, title)
        for d, u, b, t in zip(date, pdf_url, body, title):
            f_writer.writerow([d,u,t,b])
             #   date = soup.title.text.strip()
collect_PDF(1, 2841)