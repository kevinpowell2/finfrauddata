#!/usr/bin/env python
# coding: utf-8

# In[15]:


import flask
import os
import pandas as pd
from neo4j import GraphDatabase


# In[16]:


uri = "bolt://54.208.4.212:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "password"))

os.getcwd()


# In[17]:


os.chdir('C:/Users/CodyGraham/Documents/InternBitbucket/finfrauddata/crime_scraping/out')


# In[18]:


data = pd.read_csv('sec_2019.csv', encoding="windows-1252")


# In[19]:


for col in data.columns:
    print(col)


# In[26]:


def add_secData(tx, lrID, Title, Date, FirstParagraph, Body, URL):
    lrID=str(lrID)
    
    tx.run("MERGE (a:lrID {name: $lrID}) "
           "MERGE (a)<-[:TITLE]-(b:Title {name: $Title})"
           "MERGE (a)<-[:DATE]-(c:Date {name: $Date})"
           "MERGE (a)<-[:FIRSTPARAGRAPH]-(d:FirstParagraph {name: $FirstParagraph})"
           "MERGE (a)<-[:BODY]-(e:Body {name: $Body})"
           "MERGE (a)<-[:URL]-(f:Url {name: $URL})",
           lrID=lrID, Title=Title, Date=Date, FirstParagraph=FirstParagraph, Body=Body, URL=URL)


# In[27]:


for i in range(len(data)):
    with driver.session() as session:
        session.write_transaction(add_secData, data['lrID'][i], data['Title'][i], data['Date'][i], data['FirstParagraph'][i], data['Body'][i], data['URL'][i])


# In[ ]:




