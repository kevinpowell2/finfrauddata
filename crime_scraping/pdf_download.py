# -*- coding: utf-8 -*-
"""
Created on Mon Jul 22 14:52:11 2019

@author: AaronBridgeman
"""
import requests
import time
import csv
from bs4 import BeautifulSoup as bs
from bs4 import UnicodeDammit
import urllib.request
import io
import os
import pandas as pd
from urllib.request import Request, urlopen

from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}


os.chdir(r'C:\Users\AaronBridgeman\Documents\Financial Fraud Project\finfrauddata\crime_scraping')
FCA_PDF = pd.read_csv("FCA_PDF_.csv", encoding = 'ISO-8859-1')

def download_file(download_url,name):
    print(download_url)
    req = Request(download_url, headers=hdr)
    response1 = urlopen(response)
    file = open('out/pdf/' + name + '.pdf', 'wb')
    file.write(response1.read())
    file.close()
    
    
for x in range(len(FCA_PDF.loc[:,"PDF_URL"])):
    download_file(FCA_PDF.loc[x, "PDF_URL"], str(x))

def extract_text_from_pdf(pdf_path):
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    converter = TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)
 
    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh, 
                                      caching=True,
                                      check_extractable=True):
            page_interpreter.process_page(page)
 
        text = fake_file_handle.getvalue()
 
    # close open handles
    converter.close()
    fake_file_handle.close()
    filename = pdf_path.split('/')[-1].split('.')[0]
    if text:
        text = bytes(text, 'utf-8').decode('ascii','ignore')
        with open('out/txt/' + filename + '.txt','w') as f:
            f.write(text)
        return text,filename
    
text = extract_text_from_pdf('out/pdf/2018-01.pdf')

def collect_PDF(start, end):
    counter = start
    name = []
    pdf_url = []
    with open('Documents/FCA_PDF_.csv','w',newline='') as f:
        f_writer = csv.writer(f, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        f_writer.writerow(["Date","PDF_URL","TITLE","BODY"])
        while counter <= end:
            url = ("https://www.fca.org.uk/publications/search-results?np_category=notices%20and%20decisions-final%20notices&start=" + str(counter))
            response = requests.get(url)
            soup = bs(response.text, features="lxml")
            for el in soup.find_all("a", {"class": "search-item__clickthrough"}):
                pdf_url.append(el["href"])
                name.append(el.text.strip()[:-6])
            counter += 10       
            time.sleep(1)
        for pdf, n in zip(pdf_url,name):
            download_file(pdf,n)
