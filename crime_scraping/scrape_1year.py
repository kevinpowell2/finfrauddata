import requests
import time
import csv
from bs4 import BeautifulSoup as bs
from bs4 import UnicodeDammit 

def collect_lit0017(year, start, end):
    counter = start
    print(str(year))
    with open('out/sec_' + year + '.csv', 'w') as f:
        f_writer = csv.writer(f, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        f_writer.writerow(['lrID', 'Title', 'Date', 'FirstParagraph', 'Body', 'URL'])
        while counter <= end:
            if counter == 16586:
                counter += 1
            if int(year) < 2017:
                url = 'https://www.sec.gov/litigation/litreleases/lr' + str(counter) + '.htm'
            else:
                url = 'https://www.sec.gov/litigation/litreleases/' + year + '/lr' + str(counter) + '.htm'
            response = requests.get(url)
            soup = bs(response.text)
            if "Oops!" not in soup.text:
                try:
                    body = soup.find_all("table")[2]
                except:
                    body = soup.find_all("table")[0]
                title = soup.title.text.strip()
                date = ""
                if len(title.split("\\")) > 1:
                    date = title.split("\\")[1].strip()
                elif len(title.split("/")) > 1:
                    date = title.split("/")[1].strip()
            #                 date = soup.head.find_all("meta")[0]["content"]
                if len(body.find_all("p")) < 2:
                    firstpara = body.find_all("p")[0].text.strip()
                else:
                    firstpara = body.find_all("p")[1].text.strip()
                ps = []
                for p in body.find_all("p"):
                    ps.append(p.text)
                full = " ".join(ps)
                try:
                    title = UnicodeDammit.detwingle(title)
                    firstpara_clean = UnicodeDammit.detwingle(firstpara.replace(r'^https?:\/\/.*[\r\n]*', ''))
                except:
                    firstpara_clean = firstpara.replace(r'^https?:\/\/.*[\r\n]*', '')
                try:
                    f_writer.writerow([counter,title,date,firstpara_clean.strip(),full,url])
                except:
                    full = UnicodeDammit(full, ["windows-1252"], smart_quotes_to="ascii").unicode_markup
                    title = UnicodeDammit(title, ["windows-1252"], smart_quotes_to="ascii").unicode_markup
                    firstpara_clean = UnicodeDammit(firstpara_clean,["windows-1252"], smart_quotes_to="ascii").unicode_markup
                    try:
                        f_writer.writerow([counter,title,date,firstpara_clean,full,url])
                    except:
                        print("not happening: " + title)
            counter += 1        