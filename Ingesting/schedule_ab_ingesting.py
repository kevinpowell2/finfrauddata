#Schedule AB
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch AB')

csv_ab_ria = ['IA_Schedule_A_B_20150316_20150930.csv', 'IA_Schedule_A_B_20170701_20170930.csv',
'IA_Schedule_A_B_20151001_20151231.csv', 'IA_Schedule_A_B_20171001_20171231.csv',
'IA_Schedule_A_B_20160101_20160331.csv', 'IA_Schedule_A_B_20180101_20180331.csv',
'IA_Schedule_A_B_20160401_20160630.csv', 'IA_Schedule_A_B_20180401_20180630.csv',
'IA_Schedule_A_B_20160701_20160930.csv', 'IA_Schedule_A_B_20180701_20180930.csv',
'IA_Schedule_A_B_20161001_20161231.csv', 'IA_Schedule_A_B_20181001_20181231.csv',
'IA_Schedule_A_B_20170101_20170331.csv', 'Schedule_A_B_20150315.csv',
'IA_Schedule_A_B_20170401_20170630.csv']

#collect files
df_temp = pd.read_csv('IA_Schedule_A_B_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_ab = pd.DataFrame(columns = df_temp.columns)

for file in csv_ab_ria:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_ab),(len(df_schedule_ab) + len(df_file)))
    df_schedule_ab = pd.concat([df_schedule_ab, df_file])
    print(file, ":\n")
    for col in df_schedule_ab.columns:
        print(col)
        
#era
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/ERA/schedule AB')

csv_ab_era = ['ERA_Schedule_A_B_20150316_20150930.csv', 'ERA_Schedule_A_B_20170701_20170930.csv',
'ERA_Schedule_A_B_20151001_20151231.csv', 'ERA_Schedule_A_B_20171001_20171231.csv',
'ERA_Schedule_A_B_20160101_20160331.csv', 'ERA_Schedule_A_B_20180101_20180331.csv',
'ERA_Schedule_A_B_20160401_20160630.csv', 'ERA_Schedule_A_B_20180401_20180630.csv',
'ERA_Schedule_A_B_20160701_20160930.csv', 'ERA_Schedule_A_B_20180701_20180930.csv',
'ERA_Schedule_A_B_20161001_20161231.csv', 'ERA_Schedule_A_B_20181001_20181231.csv',
'ERA_Schedule_A_B_20170101_20170331.csv', 'ERA_Schedule_A_B_20150315.csv',
'ERA_Schedule_A_B_20170401_20170630.csv', 'ERA_Schedule_A_B_20190101_20190331.csv']

df_temp = pd.read_csv('ERA_Schedule_A_B_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_ab_era = pd.DataFrame(columns = df_temp.columns)

for file in csv_ab_era:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_ab_era),(len(df_schedule_ab_era) + len(df_file)))
    df_schedule_ab_era = pd.concat([df_schedule_ab_era, df_file])
    print(file, ":\n")
    for col in df_schedule_ab_era.columns:
        print(col)
        
df_schedule_ab_merged = df_schedule_ab.append(df_schedule_ab_era, ignore_index=True)

df_schedule_ab_merged = df_schedule_ab_merged[['FilingID',
 'Full Legal Name',
 'DE/FE/I',
 'Entity in Which',
 'Title or Status',
 'Status Acquired',
 'Ownership Code',
 'Control Person',
 'PR',
 'OwnerID']]

is_individual = df_schedule_ab_merged['DE/FE/I']=='I'
df_owners_orgs = df_schedule_ab_merged[(df_schedule_ab_merged['DE/FE/I']=='DE') | (df_schedule_ab_merged['DE/FE/I']=='FE')]
df_owners_inds = df_schedule_ab_merged[is_individual]

df_owners_inds = df_owners_inds[['FilingID',
 'Full Legal Name',
 'Entity in Which',
 'Title or Status',
 'Status Acquired',
 'Ownership Code',
 'Control Person',
 'PR',
 'OwnerID']]
df_owners_inds.columns = ['FILINGID', 'OWNERNAME_NAME_FULL', 'ENTITYOWNED','TITLE', 'DATEACQUIRED', 'OWNERSHIPCODE', 'CONTROLPERSON', 'PR', 'OWNER_ID']
df_owners_orgs.columns = ['FILINGID', 'OWNERNAME_NAME_ORG', 'ORGTYPE', 'ENTITYOWNED', 'TITLE', 'DATEACQUIRED', 'OWNERSHIPCODE', 'CONTROLPERSON', 'PR', 'OWNER_ID']

os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/Processed Data')

df_owners_inds.to_csv("SCHEDULE_AB_OWNERS_INDS.csv")
df_owners_orgs.to_csv("SCHEDULE_AB_OWNERS_ORGS.csv")
