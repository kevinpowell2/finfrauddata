# -*- coding: utf-8 -*-
"""
Created on Thu Jul 25 11:37:56 2019

@author: CodyGraham
"""

import os
import re
import json
import csv
import time
import numpy as np
from neo4j import GraphDatabase

uri = "bolt://54.208.4.212:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "password"))

"""takes current and a counter c and creates a json file with the given current
    params:
        current: the current line to write
        c: the number of the json file
    return: none
    author:
        Josephine Douglas
    date:
        7/23/2019
"""
def write_to_file(current, c): 
    os.chdir('C:\\Users\\CodyGraham\\Desktop\\JsonObjects\\JsonFiles')
    with open(str(c) + 'toJSON.json', 'wb') as f:
        f.write(current.encode('utf-8'))
        
"""splits the given file and creates json files out of it
    params: none
    returns: none
    author:
        Josephine Douglas
    date:
        7/23/2019
"""
def create_json_files():
    counter = 0
    with open('C:\\Users\\CodyGraham\\Desktop\\JsonObjects\\fullrun_0_x.json', encoding='utf-8') as json_file:
        lines = json_file.readlines()
        for line in lines:
            write_to_file(line,counter)
            counter+=1

"""takes a list or set and returns it in a more formatted way as a string
    params:
        current_list: the list you want formatted
    returns:
        a string formatted in the way specified
    author:
        Cody Graham
    date:
        7/23/2019
"""
def print_list(current_list):
    output = ""
    for i in current_list:
        output += str(i) + ", "
    return output

"""uses regex to sort strings
    author:
        Cody Graham
    date:
        7/23/2019
"""
def atoi(text):
    return int(text) if text.isdigit() else text

"""uses regex to sort strings
    author:
        Cody Graham
    date:
        7/23/2019
"""
def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

"""gets and returns the entityid of the current json
    params: none
    returns:
        the entityid
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_entityid():
    entityid = set([])
    try:
        entityid.add(data['RESOLVED_ENTITY']['ENTITY_ID'])
    except KeyError:
        pass
    return entityid

"""gets and returns the addresses of the current json
    params: none
    returns:
        the addresses
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_addresses():
    output = set([])
    for i in range(len(data['RESOLVED_ENTITY']['RECORDS'])):
        try:  
            for l in range(len(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['ADDRESSES'])):
                record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['ADDRESSES'][l]
                address = ''
                for key, value in record_data.items():
                    if value == "CCO":
                        continue
    #                 if not value == "":
    #                     print(value+"___")
    #             print(" ")
                    address += str(value)
                    address += ' '
                output.add(address)
        except KeyError:
            pass
    return sorted(output)

"""gets and returns the filingid of the current json
    params: none
    returns:
        the filingid
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_filingid():
    output = set([])
    counter = 1
    for i in range(len(data['RESOLVED_ENTITY']['RECORDS'])):
        try:  
            for l in range(len(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['FILINGID'])):
                record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['FILINGID'][l]
                filingid = ''
                for key, value in record_data.items():
                    filingid = ''
                    if value == "CCO":
                        continue
                    filingid += str(counter) + ": "
                    counter += 1
                    filingid += str(value)
                    output.add(filingid)
        except KeyError:
            pass
    return sorted(output,key=natural_keys)

"""gets and returns the phone numbers of the current json
    params: none
    returns:
        the phone numbers
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_phonenums():
    phone_nums = set([])
    for i in range(len(data['RESOLVED_ENTITY']['RECORDS'])):
        try:
            for l in range(len(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['PHONENUMS'])):
                record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['PHONENUMS'][l]['PHONE_NUMBER']
                output = ''
                output += record_data
                phone_nums.add(output)
        except KeyError:
            pass
    return sorted(phone_nums)

"""gets and returns the recordid of the current json
    params: none
    returns:
        the recordid
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_recordid():
    recordid = set([])
    for i in range(len(data['RESOLVED_ENTITY']['RECORDS'])):
        record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['RECORD_ID']
        output = str(record_data)
        recordid.add(output)
    return sorted(recordid)

"""gets and returns the specified cco_attribute
    works with ccoempein and ccoemp_add
    params:
        title: either ccoempein or ccoemp_add
    returns: A set of all of the ccoempein or
                ccoemp_add values
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_cco_attribute(title):
    output = set([])
    counter = 1
    for i in range(len(data['RESOLVED_ENTITY']['RECORDS'])):
        if data['RESOLVED_ENTITY']['RECORDS'][i]['DATA_SOURCE'] == 'CCO':
            for l in range(len(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA'][str(title).upper()])):
                record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA'][str(title).upper()][l]
                cco = ''
                for key, value in record_data.items():
                    cco = ''
                    if value == "CCO":
                        continue
                    cco += str(counter) + ": "
                    counter += 1
                    cco += str(value)
                    output.add(cco)
    return sorted(output,key=natural_keys)

"""gets and returns the values of the senzing data if they are in a list or just a string
    works with employer, email_address, cik_number, crd_number, lei_number, sec_number, name_full, load_id,
    data_source, entity_type, jobtitles, phone_number, record_id
    params:
        title: one of the above listed that it works with
    returns:
        a set of the all the values of the specified type
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_list_or_string(title):
    output = set([])
    counter = 1
    for i in range(len(data['RESOLVED_ENTITY']['RECORDS'])):
        try:
            if type(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA'][str(title).upper()]) == list: 
                for l in range(len(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA'][str(title).upper()])):
                    record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA'][str(title).upper()][l]
                    result = ''
                    for key, value in record_data.items():
                        result = ''
                        result += str(counter) + ": "
                        counter += 1
                        result += str(value)
                        output.add(result)
            elif type(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA'][str(title).upper()]) == str:
                record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA'][str(title).upper()]
                result = record_data
                output.add(result)
        except KeyError:
            counter+=1
            pass
    return sorted(output,key=natural_keys)

"""gets and returns the matchkey of the related entities.
    works with matchkey and entity_id
    params:
        title: one of the above specified
    returns:
        a list of all of the specified data
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_related_attributes(title):
    output = list()
    for i in range(len(data['RELATED_ENTITIES'])):
        related_data = data['RELATED_ENTITIES'][i][str(title).upper()]
        result = ''
        result += str(related_data)
        output.append(result)
    return output

"""takes the relatedid set and outputs it as a set
    params: none
    returns:
        a set of individual values
    author:
        Cody Graham
    date:
        7/23/2019
"""
def individual_relatedid():
    output = set([])
    for i in get_related_attributes('entity_id'):
            output.add(str(i))
    return output

"""gets and returns all of the org names in key, value pairs as a set
    params: none
    returns:
        a set of the org names with key, value pairs
    author:
        Cody Graham
    date:
        7/23/2019
"""
def get_orgnames():
    names = set([])
    for i in range(len(data['RESOLVED_ENTITY']['RECORDS'])):
        try:
            for l in range(len(data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['NAMES'])):
                record_data = data['RESOLVED_ENTITY']['RECORDS'][i]['JSON_DATA']['NAMES'][l]
                output = ''
                counter=1
                for key, value in record_data.items():
                    if not counter % 2 == 0:
                        output += str(value)
                    counter+=1
                names.add(output)
        except KeyError:
            pass
    return sorted(names)

"""formats the data in order to get it into neo4j
    params: see methods
    returns: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def format_person(tx, entityid, entityid2, matchkey, name, address, ccoempein, ccoemp_add, data_source, email_address, employer, entity_type, filingid, jobtitles, loadid, phonenums, recordid, controlper, owned_code, owned_since, ownerid, parent_org, pubrep, title, descript, effectdate, termdate, disclose_type, register, control_per, data_acqr, own_code, own_emp, public_rep, crd_number, relyad_nme):
    e1=int(entityid.replace(",","").replace(" ",""))
    e2=int(entityid2.replace(",","").replace(" ",""))
    if e1 < e2:
        tx.run("MERGE (a:PEOPLE {entityid: $entityid, name: $name, address: $address, ccoempein: $ccoempein, ccoemp_add: $ccoemp_add, data_source: $data_source, email_address: $email_address, employer: $employer, entity_type: $entity_type, filingid: $filingid, jobtitles: $jobtitles, loadid: $loadid, phonenums: $phonenums, recordid: $recordid, controlper: $controlper, owned_code: $owned_code, owned_since: $owned_since, ownerid: $ownerid, parent_org: $parent_org, pubrep: $pubrep, title: $title, descript: $descript, effectdate: $effectdate, termdate: $termdate, disclose_type: $disclose_type, register: $register, control_per: $control_per, data_acqr: $data_acqr, own_code: $own_code, own_emp: $own_emp, public_rep: $public_rep, crd_number: $crd_number, relyad_nme: $relyad_nme})",
                entityid=entityid.replace(",","").replace(" ",""), entityid2=entityid2.replace(", ", ""), matchkey=matchkey.replace(",",""), name=name, address=address, ccoempein=ccoempein, ccoemp_add=ccoemp_add, data_source=data_source, email_address=email_address, employer=employer, entity_type=entity_type, filingid=filingid, jobtitles=jobtitles, loadid=loadid, phonenums=phonenums, recordid=recordid, controlper=controlper, owned_code=owned_code, owned_since=owned_since, ownerid=ownerid, parent_org=parent_org, pubrep=pubrep, title=title, descript=descript, effectdate=effectdate, termdate=termdate, disclose_type=disclose_type, register=register, control_per=control_per, data_acqr=data_acqr, own_code=own_code, own_emp=own_emp, public_rep=public_rep, crd_number=crd_number, relyad_nme=relyad_nme)
    else:
        try:
            tx.run("MATCH (a) WHERE a.entityid = '{}' MATCH (b) WHERE b.entityid = '{}' MERGE (a)<-[:{}]-(b)".format(str(entityid2.replace(",","").replace(" ","")), str(entityid.replace(",","").replace(" ","")), matchkey.replace("+","__").replace(" ","").replace(",","").replace("-","AND")),
                      entityid=entityid.replace(",","").replace(" ",""), entityid2=entityid2.replace(", ", ""), matchkey=matchkey.replace(",",""), name=name, address=address, ccoempein=ccoempein, ccoemp_add=ccoemp_add, data_source=data_source, email_address=email_address, employer=employer, entity_type=entity_type, filingid=filingid, jobtitles=jobtitles, loadid=loadid, phonenums=phonenums, recordid=recordid, controlper=controlper, owned_code=owned_code, owned_since=owned_since, ownerid=ownerid, parent_org=parent_org, pubrep=pubrep, title=title, descript=descript, effectdate=effectdate, termdate=termdate, disclose_type=disclose_type, register=register, control_per=control_per, data_acqr=data_acqr, own_code=own_code, own_emp=own_emp, public_rep=public_rep, crd_number=crd_number, relyad_nme=relyad_nme)
        except:
            tx.run("MATCH (a) WHERE a.entityid = $entityid2 MERGE (a)<-[:{}]-(b:PEOPLE {entityid: $entityid, name: $name, address: $address, ccoempein: $ccoempein, ccoemp_add: $ccoemp_add, data_source: $data_source, email_address: $email_address, employer: $employer, entity_type: $entity_type, filingid: $filingid, jobtitles: $jobtitles, loadid: $loadid, phonenums: $phonenums, recordid: $recordid, controlper: $controlper, owned_code: $owned_code, owned_since: $owned_since, ownerid: $ownerid, parent_org: $parent_org, pubrep: $pubrep, title: $title, descript: $descript, effectdate: $effectdate, termdate: $termdate, disclose_type: $disclose_type, register: $register, control_per: $control_per, data_acqr: $data_acqr, own_code: $own_code, own_emp: $own_emp, public_rep: $public_rep, crd_number: $crd_number, relyad_nme: $relyad_nme})".format(matchkey.replace("+","__").replace(" ","").replace(",","").replace("-","AND")),
                      entityid=entityid.replace(",","").replace(" ",""), entityid2=entityid2.replace(", ", ""), matchkey=matchkey.replace(",",""), name=name, address=address, ccoempein=ccoempein, ccoemp_add=ccoemp_add, data_source=data_source, email_address=email_address, employer=employer, entity_type=entity_type, filingid=filingid, jobtitles=jobtitles, loadid=loadid, phonenums=phonenums, recordid=recordid, controlper=controlper, owned_code=owned_code, owned_since=owned_since, ownerid=ownerid, parent_org=parent_org, pubrep=pubrep, title=title, descript=descript, effectdate=effectdate, termdate=termdate, disclose_type=disclose_type, register=register, control_per=control_per, data_acqr=data_acqr, own_code=own_code, own_emp=own_emp, public_rep=public_rep, crd_number=crd_number, relyad_nme=relyad_nme)
            
"""formats the data in order to get it into neo4j
    params: see methods
    returns: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def format_org(tx, entityid, entityid2, matchkey, name, address, data_source, entity_type, filingid, loadid, phonenums, recordid, onebillionassets, accountingexamdate, amtclientfundcustody, businessactivities, busnamechange, ciknumber, clients, conflictofinterest, crdnumber, custody, disclosures, discretionaryassets, executiondate, executiontype, exemptionreasons, legnmechng, leinumber, managesecurities, newnme, nondiscretionaryassets, numberofoffices, numclientcustody, numclientsia, numdiscretionaryaccts, numemployeesbrokers, numemployeesia, numnondiscretionaryaccts, otherbusactivity, otherclients, percentclientnonus, publiccompany, regreasons, relatedclientfundscustody, relatednumclientscustody, secnumber, successiondate, umbrellaregistration, bus_actv, foreign_reg, legal_nme, share_location, share_person, controlorg, forgien_dom, owned_code, owned_since, ownerid, parent_org, pubrep, descript, effectdate, termdate, control_per, date_acqr, jobtitle, own_id, pub_rep, relyad_cik, relyad_nme, managed_org, other_busnme, reg_assets, rel_person, bus_nme, exmpt_app_number, exmpt_date, jurisdiction, other_jurisdiction, other_nme, reg_reasons):
    e1=int(entityid.replace(",","").replace(" ",""))
    e2=int(entityid2.replace(",","").replace(" ",""))
    if e1 < e2:
        tx.run("MERGE (a:ORG {entityid: $entityid, name: $name, address: $address, data_source: $data_source, entity_type: $entity_type, filingid: $filingid, loadid: $loadid, phonenums: $phonenums, recordid: $recordid, onebillionassets: $onebillionassets, accountingexamdate: $accountingexamdate, amtclientfundcustody: $amtclientfundcustody, businessactivities: $businessactivities, busnamechange: $busnamechange, ciknumber: $ciknumber, clients: $clients, conflictofinterest: $conflictofinterest, crdnumber: $crdnumber, custody: $custody, disclosures: $disclosures, discretionaryassets: $discretionaryassets, executiondate: $executiondate, executiontype: $executiontype, exemptionreasons: $exemptionreasons, legnmechng: $legnmechng, leinumber: $leinumber, managesecurities: $managesecurities, newnme: $newnme, nondiscretionaryassets: $nondiscretionaryassets, numberofoffices: $numberofoffices, numclientcustody: $numclientcustody, numclientsia: $numclientsia, numdiscretionaryaccts: $numdiscretionaryaccts, numemployeesbrokers: $numemployeesbrokers, numemployeesia: $numemployeesia, numnondiscretionaryaccts: $numnondiscretionaryaccts, ohterbusactivity: $otherbusactivity, otherclients: $otherclients, percentclientsnonus: $percentclientnonus, publiccompany: $publiccompany, regreasons: $regreasons, relatedclientfundscustody: $relatedclientfundscustody, relatednumclientscustody: $relatednumclientscustody, secnumber: $secnumber, successiondate: $successiondate, umbrellaregistration: $umbrellaregistration, bus_actv: $bus_actv, foreign_reg: $foreign_reg, legal_nme: $legal_nme, share_location: $share_location, share_person: $share_person, controlorg: $controlorg, forgien_dom: $forgien_dom, owned_code: $owned_code, owned_since: $owned_since, ownerid: $ownerid, parent_org: $parent_org, pubrep: $pubrep, descript: $descript, effectdate: $effectdate, termdate: $termdate, control_per: $control_per, date_acqr: $date_acqr, jobtitle: $jobtitle, own_id: $own_id, pub_rep: $pub_rep, relyad_cik: $relyad_cik, relyad_nme: $relyad_nme, managed_org: $managed_org, other_busnme: $other_busnme, reg_assets: $reg_assets, rel_person: $rel_person, bus_nme: $bus_nme, exmpt_app_number: $exmpt_app_number, exmpt_date: $exmpt_date, jurisdiction: $jurisdiction, other_jurisdiction: $other_jurisdiction, other_nme: $other_nme, reg_reasons: $reg_reasons})",
                 entityid=entityid.replace(",","").replace(" ",""), entityid2=entityid2.replace(",","").replace(" ",""), matchkey=matchkey.replace(",",""), name=name, address=address, data_source=data_source, entity_type=entity_type, filingid=filingid, loadid=loadid, phonenums=phonenums, recordid=recordid, onebillionassets=onebillionassets, accountingexamdate=accountingexamdate, amtclientfundcustody=amtclientfundcustody, businessactivities=businessactivities, busnamechange=busnamechange, ciknumber=ciknumber, clients=clients, conflictofinterest=conflictofinterest, crdnumber=crdnumber, custody=custody, disclosures=disclosures, discretionaryassets=discretionaryassets, executiondate=executiondate, executiontype=executiontype, exemptionreasons=exemptionreasons, legnmechng=legnmechng, leinumber=leinumber, managesecurities=managesecurities, newnme=newnme, nondiscretionaryassets=nondiscretionaryassets, numberofoffices=numberofoffices, numclientcustody=numclientcustody, numclientsia=numclientsia, numdiscretionaryaccts=numdiscretionaryaccts, numemployeesbrokers=numemployeesbrokers, numemployeesia=numemployeesia, numnondiscretionaryaccts=numnondiscretionaryaccts, otherbusactivity=otherbusactivity, otherclients=otherclients, percentclientnonus=percentclientnonus, publiccompany=publiccompany, regreasons=regreasons, relatedclientfundscustody=relatedclientfundscustody, relatednumclientscustody=relatednumclientscustody, secnumber=secnumber, successiondate=successiondate, umbrellaregistration=umbrellaregistration, bus_actv=bus_actv, foreign_reg=foreign_reg, legal_nme=legal_nme, share_location=share_location, share_person=share_person, controlorg=controlorg, forgien_dom=forgien_dom, owned_code=owned_code, owned_since=owned_since, ownerid=ownerid, parent_org=parent_org, pubrep=pubrep, descript=descript, effectdate=effectdate, termdate=termdate, control_per=control_per, date_acqr=date_acqr, jobtitle=jobtitle, own_id=own_id, pub_rep=pub_rep, relyad_cik=relyad_cik, relyad_nme=relyad_nme, managed_org=managed_org, other_busnme=other_busnme, reg_assets=reg_assets, rel_person=rel_person, bus_nme=bus_nme, exmpt_app_number=exmpt_app_number, exmpt_date=exmpt_date, jurisdiction=jurisdiction, other_jurisdiction=other_jurisdiction, other_nme=other_nme, reg_reasons=reg_reasons)
    else:
        try:
            tx.run("MATCH (a) WHERE a.entityid = '{}' MATCH (b) WHERE b.entityid = '{}' MERGE (a)<-[:{}]-(b)".format(str(entityid2.replace(",","").replace(" ","")), str(entityid.replace(",","").replace(" ","")), matchkey.replace("+","__").replace("-","AND")),
                      entityid=entityid.replace(",","").replace(" ",""), entityid2=entityid2.replace(",","").replace(" ",""), matchkey=matchkey.replace(",",""), name=name, address=address, data_source=data_source, entity_type=entity_type, filingid=filingid, loadid=loadid, phonenums=phonenums, recordid=recordid, onebillionassets=onebillionassets, accountingexamdate=accountingexamdate, amtclientfundcustody=amtclientfundcustody, businessactivities=businessactivities, busnamechange=busnamechange, ciknumber=ciknumber, clients=clients, conflictofinterest=conflictofinterest, crdnumber=crdnumber, custody=custody, disclosures=disclosures, discretionaryassets=discretionaryassets, executiondate=executiondate, executiontype=executiontype, exemptionreasons=exemptionreasons, legnmechng=legnmechng, leinumber=leinumber, managesecurities=managesecurities, newnme=newnme, nondiscretionaryassets=nondiscretionaryassets, numberofoffices=numberofoffices, numclientcustody=numclientcustody, numclientsia=numclientsia, numdiscretionaryaccts=numdiscretionaryaccts, numemployeesbrokers=numemployeesbrokers, numemployeesia=numemployeesia, numnondiscretionaryaccts=numnondiscretionaryaccts, otherbusactivity=otherbusactivity, otherclients=otherclients, percentclientnonus=percentclientnonus, publiccompany=publiccompany, regreasons=regreasons, relatedclientfundscustody=relatedclientfundscustody, relatednumclientscustody=relatednumclientscustody, secnumber=secnumber, successiondate=successiondate, umbrellaregistration=umbrellaregistration, bus_actv=bus_actv, foreign_reg=foreign_reg, legal_nme=legal_nme, share_location=share_location, share_person=share_person, controlorg=controlorg, forgien_dom=forgien_dom, owned_code=owned_code, owned_since=owned_since, ownerid=ownerid, parent_org=parent_org, pubrep=pubrep, descript=descript, effectdate=effectdate, termdate=termdate, control_per=control_per, date_acqr=date_acqr, jobtitle=jobtitle, own_id=own_id, pub_rep=pub_rep, relyad_cik=relyad_cik, relyad_nme=relyad_nme, managed_org=managed_org, other_busnme=other_busnme, reg_assets=reg_assets, rel_person=rel_person, bus_nme=bus_nme, exmpt_app_number=exmpt_app_number, exmpt_date=exmpt_date, jurisdiction=jurisdiction, other_jurisdiction=other_jurisdiction, other_nme=other_nme, reg_reasons=reg_reasons)
        except:
            tx.run("MATCH (a) WHERE a.entityid = $entityid2 MERGE (a)<-[:{}]-(b:ORG {entityid: $entityid, name: $name, address: $address, data_source: $data_source, entity_type: $entity_type, filingid: $filingid, loadid: $loadid, phonenums: $phonenums, recordid: $recordid, onebillionassets: $onebillionassets, accountingexamdate: $accountingexamdate, amtclientfundcustody: $amtclientfundcustody, businessactivities: $businessactivities, busnamechange: $busnamechange, ciknumber: $ciknumber, clients: $clients, conflictofinterest: $conflictofinterest, crdnumber: $crdnumber, custody: $custody, disclosures: $disclosures, discretionaryassets: $discretionaryassets, executiondate: $executiondate, executiontype: $executiontype, exemptionreasons: $exemptionreasons, legnmechng: $legnmechng, leinumber: $leinumber, managesecurities: $managesecurities, newnme: $newnme, nondiscretionaryassets: $nondiscretionaryassets, numberofoffices: $numberofoffices, numclientcustody: $numclientcustody, numclientsia: $numclientsia, numdiscretionaryaccts: $numdiscretionaryaccts, numemployeesbrokers: $numemployeesbrokers, numemployeesia: $numemployeesia, numnondiscretionaryaccts: $numnondiscretionaryaccts, ohterbusactivity: $otherbusactivity, otherclients: $otherclients, percentclientsnonus: $percentclientnonus, publiccompany: $publiccompany, regreasons: $regreasons, relatedclientfundscustody: $relatedclientfundscustody, relatednumclientscustody: $relatednumclientscustody, secnumber: $secnumber, successiondate: $successiondate, umbrellaregistration: $umbrellaregistration, bus_actv: $bus_actv, foreign_reg: $foreign_reg, legal_nme: $legal_nme, share_location: $share_location, share_person: $share_person, controlorg: $controlorg, forgien_dom: $forgien_dom, owned_code: $owned_code, owned_since: $owned_since, ownerid: $ownerid, parent_org: $parent_org, pubrep: $pubrep, descript: $descript, effectdate: $effectdate, termdate: $termdate, control_per: $control_per, date_acqr: $date_acqr, jobtitle: $jobtitle, own_id: $own_id, pub_rep: $pub_rep, relyad_cik: $relyad_cik, relyad_nme: $relyad_nme, managed_org: $managed_org, other_busnme: $other_busnme, reg_assets: $reg_assets, rel_person: $rel_person, bus_nme: $bus_nme, exmpt_app_number: $exmpt_app_number, exmpt_date: $exmpt_date, jurisdiction: $jurisdiction, other_jurisdiction: $other_jurisdiction, other_nme: $other_nme, reg_reasons: $reg_reasons})".format(matchkey.replace("+","__").replace("-","AND")),
                      entityid=entityid.replace(",","").replace(" ",""), entityid2=entityid2.replace(",","").replace(" ",""), matchkey=matchkey.replace(",",""), name=name, address=address, data_source=data_source, entity_type=entity_type, filingid=filingid, loadid=loadid, phonenums=phonenums, recordid=recordid, onebillionassets=onebillionassets, accountingexamdate=accountingexamdate, amtclientfundcustody=amtclientfundcustody, businessactivities=businessactivities, busnamechange=busnamechange, ciknumber=ciknumber, clients=clients, conflictofinterest=conflictofinterest, crdnumber=crdnumber, custody=custody, disclosures=disclosures, discretionaryassets=discretionaryassets, executiondate=executiondate, executiontype=executiontype, exemptionreasons=exemptionreasons, legnmechng=legnmechng, leinumber=leinumber, managesecurities=managesecurities, newnme=newnme, nondiscretionaryassets=nondiscretionaryassets, numberofoffices=numberofoffices, numclientcustody=numclientcustody, numclientsia=numclientsia, numdiscretionaryaccts=numdiscretionaryaccts, numemployeesbrokers=numemployeesbrokers, numemployeesia=numemployeesia, numnondiscretionaryaccts=numnondiscretionaryaccts, otherbusactivity=otherbusactivity, otherclients=otherclients, percentclientnonus=percentclientnonus, publiccompany=publiccompany, regreasons=regreasons, relatedclientfundscustody=relatedclientfundscustody, relatednumclientscustody=relatednumclientscustody, secnumber=secnumber, successiondate=successiondate, umbrellaregistration=umbrellaregistration, bus_actv=bus_actv, foreign_reg=foreign_reg, legal_nme=legal_nme, share_location=share_location, share_person=share_person, controlorg=controlorg, forgien_dom=forgien_dom, owned_code=owned_code, owned_since=owned_since, ownerid=ownerid, parent_org=parent_org, pubrep=pubrep, descript=descript, effectdate=effectdate, termdate=termdate, control_per=control_per, date_acqr=date_acqr, jobtitle=jobtitle, own_id=own_id, pub_rep=pub_rep, relyad_cik=relyad_cik, relyad_nme=relyad_nme, managed_org=managed_org, other_busnme=other_busnme, reg_assets=reg_assets, rel_person=rel_person, bus_nme=bus_nme, exmpt_app_number=exmpt_app_number, exmpt_date=exmpt_date, jurisdiction=jurisdiction, other_jurisdiction=other_jurisdiction, other_nme=other_nme, reg_reasons=reg_reasons)

"""formats the data in order to get it into neo4j
    params: see methods
    returns: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def format_fund(tx, entityid, entityid2, matchkey, name, data_source, entity_type, filingid, fundid, loadid, recordid, recordtype):
    e1=int(entityid.replace(",","").replace(" ",""))
    e2=int(entityid2.replace(",","").replace(" ",""))
    if e1 < e2:
        tx.run("MERGE (a:FUND {entityid: $entityid, name: $name, data_source: $data_source, entity_type: $entity_type, filingid: $filingid, fundid: $fundid, loadid: $loadid, recordid: $recordid, recordtype: $recordtype})",
               entityid=entityid, entityid2=entityid2, matchkey=matchkey.replace(",",""), name=name, data_source=data_source, entity_type=entity_type, filingid=filingid, fundid=fundid, loadid=loadid, recordid=recordid, recordtype=recordtype)
    else:
        try:
            tx.run("MATCH (a) WHERE a.entityid = '{}' MATCH (b) WHERE b.entityid = '{}' MERGE (a)<-[:{}]-(b)".format(str(entityid2.replace(",","").replace(" ","")), str(entityid.replace(",","").replace(" ","")), matchkey.replace("+","__").replace("-","AND")),
                  entityid=entityid, entityid2=entityid2, matchkey=matchkey.replace(",",""), name=name, data_source=data_source, entity_type=entity_type, filingid=filingid, fundid=fundid, loadid=loadid, recordid=recordid, recordtype=recordtype)
        except:
            tx.run("MATCH (a) WHERE a.entityid = $entityid2 MERGE (a)<-[:{}]-(a:FUND {entityid: $entityid, name: $name, data_source: $data_source, entity_type: $entity_type, filingid: $filingid, fundid: $fundid, loadid: $loadid, recordid: $recordid, recordtype: $recordtype})".format(matchkey.replace("+","__").replace("-","AND")),
                  entityid=entityid, entityid2=entityid2, matchkey=matchkey.replace(",",""), name=name, data_source=data_source, entity_type=entity_type, filingid=filingid, fundid=fundid, loadid=loadid, recordid=recordid, recordtype=recordtype)
            
"""creates a person node within neo4j
    params: none
    return: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def create_person():
    counter=len(individual_relatedid())
    if counter == 0:
        create_altperson()
    counter=counter-1
    for i in individual_relatedid():
        with driver.session() as session:
             session.write_transaction(format_person, print_list(get_entityid()), str(i), get_related_attributes('match_key')[counter], get_list_or_string('name_full'), get_addresses(), get_list_or_string('ccoempein'), get_cco_attribute('ccoemp_add'), get_list_or_string('data_source'), get_list_or_string('email_address'), get_list_or_string('employer'), get_list_or_string('entity_type'), get_filingid(), get_list_or_string('jobtitles'), get_list_or_string('load_id'), get_phonenums(), get_recordid(), get_list_or_string('controlper'), get_list_or_string('ownedcode'), get_list_or_string('ownedsince'), get_list_or_string('owner_id'), get_list_or_string('parent_org'), get_list_or_string('pubrep'), get_list_or_string('title'), get_list_or_string('descript'), get_list_or_string('effectdate'), get_list_or_string('termdate'), get_list_or_string('disclose_type'), get_list_or_string('register'), get_list_or_string('control_per'), get_list_or_string('data_acqr'), get_list_or_string('own_code'), get_list_or_string('own_emp'), get_list_or_string('public_rep'), get_list_or_string('crd_number'), get_list_or_string('relyad_nme'))
             
"""creates a person node within neo4j with no relationships
    params: none
    return: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def create_altperson():
    with driver.session() as session:
         session.write_transaction(format_person, print_list(get_entityid()), print_list(get_entityid()), ("NaN"), get_list_or_string('name_full'), get_addresses(), get_list_or_string('ccoempein'), get_cco_attribute('ccoemp_add'), get_list_or_string('data_source'), get_list_or_string('email_address'), get_list_or_string('employer'), get_list_or_string('entity_type'), get_filingid(), get_list_or_string('jobtitles'), get_list_or_string('load_id'), get_phonenums(), get_recordid(), get_list_or_string('controlper'), get_list_or_string('ownedcode'), get_list_or_string('ownedsince'), get_list_or_string('owner_id'), get_list_or_string('parent_org'), get_list_or_string('pubrep'), get_list_or_string('title'), get_list_or_string('descript'), get_list_or_string('effectdate'), get_list_or_string('termdate'), get_list_or_string('disclose_type'), get_list_or_string('register'), get_list_or_string('control_per'), get_list_or_string('data_acqr'), get_list_or_string('own_code'), get_list_or_string('own_emp'), get_list_or_string('public_rep'), get_list_or_string('crd_number'), get_list_or_string('relyad_nme'))
         
"""creates a org node within neo4j
    params: none
    return: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def create_org():
    counter=len(individual_relatedid())
    if counter == 0:
        create_altorg()
    counter=counter-1
    for i in individual_relatedid():
        with driver.session() as session:
             session.write_transaction(format_org, print_list(get_entityid()), str(i), get_related_attributes('match_key')[counter], get_orgnames(), get_addresses(), get_list_or_string('data_source'), get_list_or_string('entity_type'), get_filingid(), get_list_or_string('load_id'), get_phonenums(), get_recordid(), get_list_or_string('1billionassets'), get_list_or_string('accountingexamdate'), get_list_or_string('amtclientfundscustody'), get_list_or_string('businessactivities'), get_list_or_string('busnamechange'), get_list_or_string('cik_number'), get_list_or_string('clients'), get_list_or_string('conflictinterest'), get_list_or_string('crd_number'), get_list_or_string('custody'), get_list_or_string('disclosures'), get_list_or_string('discretionaryassets'), get_list_or_string('executiondate'), get_list_or_string('executiontype'), get_list_or_string('exemptionreasons'), get_list_or_string('legnmechng'), get_list_or_string('lei_number'), get_list_or_string('managesecurities'), get_list_or_string('new_nme'), get_list_or_string('nondiscretionaryassets'), get_list_or_string('numberoffices'), get_list_or_string('numclientscustody'), get_list_or_string('numclientsia'), get_list_or_string('numdiscretionaryaccts'), get_list_or_string('numemployeesbrokers'), get_list_or_string('numemployeesia'), get_list_or_string('numnondiscretionaryaccts'), get_list_or_string('otherbusactivity'), get_list_or_string('otherclients'), get_list_or_string('percentclientsnonus'), get_list_or_string('publiccompany'), get_list_or_string('regreasons'), get_list_or_string('relatedclientfundscustody'), get_list_or_string('relatednumclientscustody'), get_list_or_string('sec_number'), get_list_or_string('successiondate'), get_list_or_string('umbrellaregistration'), get_list_or_string('bus_actv'), get_list_or_string('foreign_reg'), get_list_or_string('legal_nme'), get_list_or_string('share_location'), get_list_or_string('share_person'), get_list_or_string('controlorg'), get_list_or_string('forgien/dom'), get_list_or_string('ownedcode'), get_list_or_string('ownedsince'), get_list_or_string('owner_id'), get_list_or_string('parent_org'), get_list_or_string('pubrep'), get_list_or_string('descript'), get_list_or_string('effectdate'), get_list_or_string('termdate'), get_list_or_string('control_per'), get_list_or_string('date_acqr'), get_list_or_string('jobtitle'), get_list_or_string('own_id'), get_list_or_string('pub_rep'), get_list_or_string('relyad_cik'), get_list_or_string('relyad_nme'), get_list_or_string('managed_org'), get_list_or_string('other_busnme'), get_list_or_string('reg_assets'), get_list_or_string('rel_person'), get_list_or_string('bus_nme'), get_list_or_string('exmpt_app_number'), get_list_or_string('exmpt_date'), get_list_or_string('jurisdiction'), get_list_or_string('other_jurisdiction'), get_list_or_string('other_nme'), get_list_or_string('reg_reasons'))
             
"""creates a org node within neo4j with no relationships
    params: none
    return: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def create_altorg():
    with driver.session() as session:
        session.write_transaction(format_org, print_list(get_entityid()), print_list(get_entityid()), ("NaN"), get_orgnames(), get_addresses(), get_list_or_string('data_source'), get_list_or_string('entity_type'), get_filingid(), get_list_or_string('load_id'), get_phonenums(), get_recordid(), get_list_or_string('1billionassets'), get_list_or_string('accountingexamdate'), get_list_or_string('amtclientfundscustody'), get_list_or_string('businessactivities'), get_list_or_string('busnamechange'), get_list_or_string('cik_number'), get_list_or_string('clients'), get_list_or_string('conflictinterest'), get_list_or_string('crd_number'), get_list_or_string('custody'), get_list_or_string('disclosures'), get_list_or_string('discretionaryassets'), get_list_or_string('executiondate'), get_list_or_string('executiontype'), get_list_or_string('exemptionreasons'), get_list_or_string('legnmechng'), get_list_or_string('lei_number'), get_list_or_string('managesecurities'), get_list_or_string('new_nme'), get_list_or_string('nondiscretionaryassets'), get_list_or_string('numberoffices'), get_list_or_string('numclientscustody'), get_list_or_string('numclientsia'), get_list_or_string('numdiscretionaryaccts'), get_list_or_string('numemployeesbrokers'), get_list_or_string('numemployeesia'), get_list_or_string('numnondiscretionaryaccts'), get_list_or_string('otherbusactivity'), get_list_or_string('otherclients'), get_list_or_string('percentclientsnonus'), get_list_or_string('publiccompany'), get_list_or_string('regreasons'), get_list_or_string('relatedclientfundscustody'), get_list_or_string('relatednumclientscustody'), get_list_or_string('sec_number'), get_list_or_string('successiondate'), get_list_or_string('umbrellaregistration'), get_list_or_string('bus_actv'), get_list_or_string('foreign_reg'), get_list_or_string('legal_nme'), get_list_or_string('share_location'), get_list_or_string('share_person'), get_list_or_string('controlorg'), get_list_or_string('forgien/dom'), get_list_or_string('ownedcode'), get_list_or_string('ownedsince'), get_list_or_string('owner_id'), get_list_or_string('parent_org'), get_list_or_string('pubrep'), get_list_or_string('descript'), get_list_or_string('effectdate'), get_list_or_string('termdate'), get_list_or_string('control_per'), get_list_or_string('date_acqr'), get_list_or_string('jobtitle'), get_list_or_string('own_id'), get_list_or_string('pub_rep'), get_list_or_string('relyad_cik'), get_list_or_string('relyad_nme'), get_list_or_string('managed_org'), get_list_or_string('other_busnme'), get_list_or_string('reg_assets'), get_list_or_string('rel_person'), get_list_or_string('bus_nme'), get_list_or_string('exmpt_app_number'), get_list_or_string('exmpt_date'), get_list_or_string('jurisdiction'), get_list_or_string('other_jurisdiction'), get_list_or_string('other_nme'), get_list_or_string('reg_reasons'))
        
"""creates a fund node within neo4j
    params: none
    return: none
    author:
        Cody Graham
    date:
        7/23/2019
        session.write_transactions(format_fund, print_list(get_entityid()), str(i), get_related_attributes('match_key')[counter], get_orgnames(), get_list_or_string('data_source'), get_list_or_string('entity_type'), get_filingid(), get_list_or_string('fund_id'), get_list_or_string('load_id'), get_list_or_string('record_id'), get_list_or_string('record_type'))
"""
def create_fund():
    counter=len(individual_relatedid())
    if counter == 0:
        create_altfund()
    counter=counter-1
    for i in individual_relatedid():
        with driver.session() as session:
            session.write_transaction(format_fund, print_list(get_entityid()), str(i), get_related_attributes('match_key')[counter], get_list_or_string('name_org'), get_list_or_string('data_source'), get_list_or_string('entity_type'), get_list_or_string("filingid"), get_list_or_string('fund_id'), get_list_or_string('load_id'), get_recordid(), get_list_or_string('record_type'))
            
"""creates a fund node within neo4j with no relationships
    params: none
    return: none
    author:
        Cody Graham
    date:
        7/23/2019
"""
def create_altfund():
    with driver.session() as session:
        session.write_transaction(format_fund, print_list(get_entityid()), print_list(get_entityid()), ("NaN"), get_list_or_string('name_org'), get_list_or_string('data_source'), get_list_or_string('entity_type'), get_list_or_string("filingid"), get_list_or_string('fund_id'), get_list_or_string('load_id'), get_recordid(), get_list_or_string('record_type'))
        
def merge_node(tx, node):
    tx.run("MATCH (n:{}) WITH n.name AS name, COLLECT(n) AS nodelist, COUNT(*) AS count WHERE count > 1 CALL apoc.refactor.mergeNodes(nodelist) YIELD node RETURN node".format(str(node)),
    node=node)
    
def merge(node):
    with driver.session() as session:
        session.write_transaction(merge_node, str(node))
        
#create_json_files()
ts = time.time()
people = ['CCO', 'REG', 'SIG', 'OWNI', 'CONPER', 'DRPI', 'RELYOWNP', 'RELYCONP']
org = ['ORG', 'RELPER', 'DRPO', 'OWNO', 'RELAD', 'CONORG', 'RELYOWNO', 'CUST', 'RELYAD', 'SECOPA', 'RELYCONO']
fund = ['SECFUND']
for i in range(330920):
    with open("Json_ingesting\\JsonFiles\\" + str(i) + "toJSON.json", 'r', encoding='utf-8') as f:
        data = json.load(f)
        if data['RESOLVED_ENTITY']['ENTITY_NAME'] == "":
            continue
        for l in range(len(data['RESOLVED_ENTITY']['RECORD_SUMMARY'])):
            if data['RESOLVED_ENTITY']['RECORD_SUMMARY'][l]['DATA_SOURCE'] in people:
                create_person()
                merge('PEOPLE')
            if data['RESOLVED_ENTITY']['RECORD_SUMMARY'][l]['DATA_SOURCE'] in org:
                create_org()
                merge('ORG')
            if data['RESOLVED_ENTITY']['RECORD_SUMMARY'][l]['DATA_SOURCE'] in fund:
                create_fund()
                merge("FUND")
        print(i)
        if i % 100 == 0:
            curr = time.time()
            avg = curr - ts
            print(avg)
            ts = curr
#     os.remove("C:\\Users\\CodyGraham\\Desktop\\JsonObjects\\" + str(i) + "toJSON.json")     