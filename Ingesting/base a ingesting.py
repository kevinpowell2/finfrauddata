import pandas as pd
import numpy as np
import os

#Base A
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/base a')

#pre-2017
csv_list1 =  ['ADV_Base_A_20150315.csv', 'IA_ADV_Base_A_20150316_20150930.csv', 'IA_ADV_Base_A_20151001_20151231.csv',
'IA_ADV_Base_A_20160101_20160331.csv','IA_ADV_Base_A_20160401_20160630.csv', 'IA_ADV_Base_A_20160701_20160930.csv',
'IA_ADV_Base_A_20161001_20161231.csv', 'IA_ADV_Base_A_20170101_20170331.csv','IA_ADV_Base_A_20170401_20170630.csv',
'IA_ADV_Base_A_20170701_20170930.csv']

df_temp1 = pd.read_csv('ADV_Base_A_20150315.csv', encoding='ISO-8859-1')
df_base_a1 = pd.DataFrame(columns = df_temp1.columns)


for file in csv_list1:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_base_a1),(len(df_base_a1) + len(df_file)))
    df_base_a1 = pd.concat([df_base_a1, df_file])
    print(file, ":\n")
    for col in df_base_a1.columns:
        print(col)

df_base_a1 = df_base_a1[['FilingID',
 '1A',
 '1B',
 '1C-Legal',
 '1C-Business',
 '1C-New Name',
 '1D',
 '1E',
 '1F1-Street 1',
 '1F1-Street 2',
 '1F1-City',
 '1F1-State',
 '1F1-Country',
 '1F1-Postal',
 '1F3',
 '1F4',
 '1G-Street 1',
 '1G-Street 2',
 '1G-City',
 '1G-State',
 '1G-Country',
 '1G-Postal',
 '1J-Name',
 '1J-Title',
 '1J-Phone',
 '1J-Fax',
 '1J-Street 1',
 '1J-Street 2',
 '1J-City',
 '1J-State',
 '1J-Country',
 '1J-Postal',
 '1J-Email',
 'Reg Contact-Name',
 'Reg Contact-Title',
 'Reg Contact-Phone',
 'Reg Contact-Fax',
 'Reg Contact-Street 1',
 'Reg Contact-Street 2',
 'Reg Contact-City',
 'Reg Contact-State',
 'Reg Contact-Country',
 'Reg Contact-Postal',
 'Reg Contact-Email',
 '1N',
 '1N-CIK',
 '1O',
 '1P',
 '4A',
 '4B',
 '5B1-Number',
 '5B2-Number',
 '5C-Number',
 '5C2',
 '5D1/5D1a',
 '5D2/5D1b',
 '5D3/5D1c',
 '5D4/5D1d',
 '5D1e',
 '5D6/5D1f',
 '5D5/5D1g',
 '5D7/5D1h',
 '5D8/5D1i',
 '5D9/5D1j',
 '5D1k',
 '5D1l',
 '5D10/5D1m',
 '5D10-Other/5D1m-Other',
 '5F1',
 '5F2a',
 '5F2b',
 '5F2d',
 '5F2e',
 '6A1',
 '6A2',
 '6A3',
 '6A4',
 '6A5',
 '6A6',
 '6A7',
 '6A8',
 '6A9',
 '6A10',
 '6A11',
 '6A12',
 '6A13',
 '6A14',
 '6A7/6A14-Other',
 '6B1',
 '6B2',
 '6B3',
 '8A1',
 '8A2',
 '8A3',
 '8B1',
 '8B2',
 '8B3',
 '8C1',
 '8C2',
 '8C3',
 '8C4',
 '8D',
 '8E',
 '8F',
 '8G1',
 '8G2',
 '8H',
 '8I',
 '9A1/9A1a',
 '9A2/9A1b',
 '9A2a',
 '9A2b',
 '9B1/9B1a',
 '9B2/9B1b',
 '9B2a',
 '9B2b',
 '9C1',
 '9C2',
 '9C3',
 '9C4',
 '9D1',
 '9D2',
 '9E',
 '9F',
 '11A1',
 '11A2',
 '11B1',
 '11B2',
 '11C1',
 '11C2',
 '11C3',
 '11C4',
 '11C5',
 '11D1',
 '11D2',
 '11D3',
 '11D4',
 '11D5',
 '11E1',
 '11E2',
 '11E3',
 '11E4',
 '11F',
 '11G',
 '11H1a',
 '11H1b',
 '11H1c',
 '11H2',
 '12A',
 '12B1',
 '12B2',
 '12C1',
 '12C2',
 'Execution Type',
 'Signatory',
 'Execution Date',
 'Title']]

#post-2017
csv_list2 = ['IA_ADV_Base_A_20171001_20171231.csv', 'IA_ADV_Base_A_20180101_20180331.csv',
'IA_ADV_Base_A_20180401_20180630.csv', 'IA_ADV_Base_A_20180701_20180930.csv', 'IA_ADV_Base_A_20181001_20181231.csv']

df_temp2 = pd.read_csv('IA_ADV_Base_A_20171001_20171231.csv', encoding='ISO-8859-1')
df_base_a2 = pd.DataFrame(columns = df_temp2.columns)

for file in csv_list2:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_base_a2),(len(df_base_a2) + len(df_file)))
    df_base_a2 = pd.concat([df_base_a2, df_file])
    print(file, ":\n")
    for col in df_base_a2.columns:
        print(col)
        
df_base_a2 = df_base_a2.rename(columns={"1E1": "1E"})
df_base_a2 = df_base_a2.rename(columns={"1B1": "1B"})

df_base_a2 = df_base_a2[['FilingID',
 '1A',
 '1B',
 '1B2',
 '1C-Legal',
 '1C-Business',
 '1C-New Name',
 '1D',
 '1E',
 '1F1-Street 1',
 '1F1-Street 2',
 '1F1-City',
 '1F1-State',
 '1F1-Country',
 '1F1-Postal',
 '1F3',
 '1F4',
 '1F5',
 '1G-Street 1',
 '1G-Street 2',
 '1G-City',
 '1G-State',
 '1G-Country',
 '1G-Postal',
 '1J-Name',
 '1J-Title',
 '1J-Phone',
 '1J-Fax',
 '1J-Street 1',
 '1J-Street 2',
 '1J-City',
 '1J-State',
 '1J-Country',
 '1J-Postal',
 '1J-Email',
 '1J2-Name',
 '1J2-EIN',
 'Reg Contact-Name',
 'Reg Contact-Title',
 'Reg Contact-Phone',
 'Reg Contact-Fax',
 'Reg Contact-Street 1',
 'Reg Contact-Street 2',
 'Reg Contact-City',
 'Reg Contact-State',
 'Reg Contact-Country',
 'Reg Contact-Postal',
 'Reg Contact-Email',
 '1N',
 '1N-CIK',
 '1O',
 '1P',
 '4A',
 '4B',
 '5B1',
 '5B2',
 '5C1',
 '5C2',
 '5D1a',
 '5D1b',
 '5D1c',
 '5D1d',
 '5D1e',
 '5D1f',
 '5D1g',
 '5D1h',
 '5D1i',
 '5D1j',
 '5D1k',
 '5D1l',
 '5D1m',
 '5D1n',
 '5D1n Other',
 '5F1',
 '5F2a',
 '5F2b',
 '5F2d',
 '5F2e',
 '6A1',
 '6A2',
 '6A3',
 '6A4',
 '6A5',
 '6A6',
 '6A7',
 '6A8',
 '6A9',
 '6A10',
 '6A11',
 '6A12',
 '6A13',
 '6A14',
 '6A14-Other',
 '6B1',
 '6B2',
 '6B3',
 '8A1',
 '8A2',
 '8A3',
 '8B1',
 '8B2',
 '8B3',
 '8C1',
 '8C2',
 '8C3',
 '8C4',
 '8D',
 '8E',
 '8F',
 '8G1',
 '8G2',
 '8H1',
 '8H2',
 '8I',
 '9A1a',
 '9A1b',
 '9A2a',
 '9A2b',
 '9B1a',
 '9B1b',
 '9B2a',
 '9B2b',
 '9C1',
 '9C2',
 '9C3',
 '9C4',
 '9D1',
 '9D2',
 '9E',
 '9F',
 '11A1',
 '11A2',
 '11B1',
 '11B2',
 '11C1',
 '11C2',
 '11C3',
 '11C4',
 '11C5',
 '11D1',
 '11D2',
 '11D3',
 '11D4',
 '11D5',
 '11E1',
 '11E2',
 '11E3',
 '11E4',
 '11F',
 '11G',
 '11H1a',
 '11H1b',
 '11H1c',
 '11H2',
 '12A',
 '12B1',
 '12B2',
 '12C1',
 '12C2',
 'Execution Type',
 'Signatory',
 'Execution Date',
 'Title']]

df_base_a2 = df_base_a2.rename(columns={"5D1n Other": "5D10-Other/5D1m-Other", "6A14-Other":"6A7/6A14-Other"})

#Changing Tables to Letters
df_pre2017 = df_base_a1
df_post2017 = df_base_a2
#Conflicts of Interest (8)
ConInt_List = np.empty(len(df_pre2017), dtype = 'object')

for x in  range(len(df_pre2017.loc[:,"8A1"])):
    str_list =''
    if df_pre2017.loc[x,"8A1"] == "Y":
        str_list = str_list + 'A'
    if df_pre2017.loc[x,"8A2"] == "Y":
        str_list = str_list + 'B'
    if df_pre2017.loc[x,"8A3"] == "Y":
        str_list = str_list + 'C'
    if df_pre2017.loc[x,"8B1"] == "Y":
        str_list = str_list + 'D'
    if df_pre2017.loc[x,"8B2"] == "Y":
        str_list = str_list + 'E'
    if df_pre2017.loc[x,"8C1"] == "Y":
        str_list = str_list + 'F'
    if df_pre2017.loc[x,"8C2"] == "Y":
        str_list = str_list + 'G'
    if df_pre2017.loc[x,"8C3"] == "Y":
        str_list = str_list + 'H'
    if df_pre2017.loc[x,"8C4"] == "Y":
        str_list = str_list + 'I'
    if df_pre2017.loc[x,"8D"] == "Y":
        str_list = str_list + 'J'
    if df_pre2017.loc[x,"8E"] == "Y":
        str_list = str_list + 'K'
    if df_pre2017.loc[x,"8F"] == "Y":
        str_list = str_list + 'L'
    if df_pre2017.loc[x,"8G1"] == "Y":
        str_list = str_list + 'M'
    if df_pre2017.loc[x,"8G2"] == "Y":
        str_list = str_list + 'N'
    if df_pre2017.loc[x,"8H"] == "Y":
        str_list = str_list + 'O'
    if df_pre2017.loc[x,"8I"] == "Y":
        str_list = str_list + 'P'
    ConInt_List[x] = str_list
        
df_pre2017.insert(157, "ConInt", ConInt_List, True )

#post2017
ConInt_List = np.empty(len(df_post2017), dtype = 'object')

for x in  range(len(df_post2017.loc[:,"8A1"])):
    str_list =''
    if df_post2017.loc[x,"8A1"] == "Y":
        str_list = str_list + 'A'
    if df_post2017.loc[x,"8A2"] == "Y":
        str_list = str_list + 'B'
    if df_post2017.loc[x,"8A3"] == "Y":
        str_list = str_list + 'C'
    if df_post2017.loc[x,"8B1"] == "Y":
        str_list = str_list + 'D'
    if df_post2017.loc[x,"8B2"] == "Y":
        str_list = str_list + 'E'
    if df_post2017.loc[x,"8C1"] == "Y":
        str_list = str_list + 'F'
    if df_post2017.loc[x,"8C2"] == "Y":
        str_list = str_list + 'G'
    if df_post2017.loc[x,"8C3"] == "Y":
        str_list = str_list + 'H'
    if df_post2017.loc[x,"8C4"] == "Y":
        str_list = str_list + 'I'
    if df_post2017.loc[x,"8D"] == "Y":
        str_list = str_list + 'J'
    if df_post2017.loc[x,"8E"] == "Y":
        str_list = str_list + 'K'
    if df_post2017.loc[x,"8F"] == "Y":
        str_list = str_list + 'L'
    if df_post2017.loc[x,"8G1"] == "Y":
        str_list = str_list + 'M'
    if df_post2017.loc[x,"8G2"] == "Y":
        str_list = str_list + 'N'
    if df_post2017.loc[x,"8H1"] == "Y" or df_post2017.loc[x,"8H2"] == "Y":
        str_list = str_list + 'O'
    if df_post2017.loc[x,"8I"] == "Y":
        str_list = str_list + 'P'
    ConInt_List[x] = str_list
        
df_post2017.insert(163, "ConInt", ConInt_List, True )


 #5D Clients Table Pre-Oct 2017 (df1-df10)
Clients_List = np.empty(len(df_pre2017), dtype = 'object')

for x in  range(len(df_pre2017.loc[:,"5D1/5D1a"])):
    str_list =''
    if "%" in df_pre2017.loc[x,"5D1/5D1a"]:
        str_list = str_list + 'A'
    if "%" in df_pre2017.loc[x,"5D2/5D1b"]:
        str_list = str_list + 'B'
    if "%" in df_pre2017.loc[x,"5D3/5D1c"]:
        str_list = str_list + 'C'
    if "%" in df_pre2017.loc[x,"5D4/5D1d"]:
        str_list = str_list + 'D'
    if "%" in str(df_pre2017.loc[x,"5D1e"]):
        str_list = str_list + 'E'
    if "%" in df_pre2017.loc[x,"5D6/5D1f"]:
        str_list = str_list + 'F'
    if "%" in df_pre2017.loc[x,"5D5/5D1g"]:
        str_list = str_list + 'G'
    if "%" in df_pre2017.loc[x,"5D7/5D1h"]:
        str_list= str_list + 'H'
    if "%" in df_pre2017.loc[x,"5D8/5D1i"]:
        str_list = str_list + 'I'
    if "%" in df_pre2017.loc[x,"5D9/5D1j"]:
        str_list = str_list + 'J'
    if "%" in str(df_pre2017.loc[x,"5D1k"]):
        str_list = str_list + 'K'
    if "%" in str(df_pre2017.loc[x,"5D1l"]):
        str_list = str_list + 'L'
    if "%" in str(df_pre2017.loc[x,"5D10/5D1m"]):
        str_list = str_list + 'M'

    Clients_List[x] = str_list
        
df_pre2017.insert(158, "Clients", Clients_List, True )
 
  #5D Clients Table Post-Oct 2017 (df11-15)
Clients_List = np.empty(len(df_post2017), dtype = 'object')

for x in  range(len(df_post2017.loc[:,"5D1a"])):
    str_list =''
    if df_post2017.loc[x,"5D1a"] > 0:
        str_list = str_list + 'A'
    if df_post2017.loc[x,"5D1b"] >0:
        str_list = str_list + 'B'
    if df_post2017.loc[x,"5D1c"] >0:
        str_list = str_list + 'C'
    if df_post2017.loc[x,"5D1d"] >0:
        str_list = str_list + 'D'
    if df_post2017.loc[x,"5D1e"] >0:
        str_list = str_list + 'E'
    if df_post2017.loc[x,"5D1f"] >0:
        str_list = str_list + 'F'
    if df_post2017.loc[x,"5D1g"] >0:
        str_list = str_list + 'G'
    if df_post2017.loc[x,"5D1h"] >0:
        str_list= str_list + 'H'
    if df_post2017.loc[x,"5D1i"] >0:
        str_list = str_list + 'I'
    if df_post2017.loc[x,"5D1j"] >0:
        str_list = str_list + 'J'
    if df_post2017.loc[x,"5D1k"] >0:
        str_list = str_list + 'K'
    if df_post2017.loc[x,"5D1l"] >0:
        str_list = str_list + 'L'
    if df_post2017.loc[x,"5D1m"] >0:
        str_list = str_list + 'M'

    Clients_List[x] = str_list
        
 df_post2017.insert(151, "Clients", Clients_List, True )


#9 Custody Pre-2017

Custody_List = np.empty(len(df_pre2017), dtype = 'object')

for x in  range(len(df_pre2017.loc[:,"9A1/9A1a"])):
    str_list =''
    if df_pre2017.loc[x,"9A1/9A1a"] =="Y":
        str_list = str_list + 'A'
    if df_pre2017.loc[x,"9A2/9A1b"] =="Y":
        str_list = str_list + 'B'
    if df_pre2017.loc[x,"9B1/9B1a"] =="Y":
        str_list = str_list + 'C'
    if df_pre2017.loc[x,"9B2/9B1b"] =="Y":
        str_list = str_list + 'D'
    if df_pre2017.loc[x,"9C1"] =="Y":
        str_list = str_list + 'E'
    if df_pre2017.loc[x,"9C2"] =="Y":
        str_list= str_list + 'F'
    if df_pre2017.loc[x,"9C3"] =="Y":
        str_list = str_list + 'G'
    if df_pre2017.loc[x,"9C4"] =="Y":
        str_list = str_list + 'H'
    if df_pre2017.loc[x,"9D1"] =="Y":
        str_list = str_list + 'I'
    if df_pre2017.loc[x,"9D2"] =="Y":
        str_list = str_list + 'J'

    Custody_List[x] = str_list
        
 df_pre2017.insert(159, "Custody", Custody_List, True )
 
 
#9 Custody Post-2017
Custody_List = np.empty(len(df_post2017), dtype = 'object')

for x in  range(len(df_post2017.loc[:,"9A1a"])):
    str_list =''
    if df_post2017.loc[x,"9A1a"] =="Y":
        str_list = str_list + 'A'
    if df_post2017.loc[x,"9A1b"] =="Y":
        str_list = str_list + 'B'
    if df_post2017.loc[x,"9B1a"] =="Y":
        str_list = str_list + 'C'
    if df_post2017.loc[x,"9B1b"] =="Y":
        str_list = str_list + 'D'
    if df_post2017.loc[x,"9C1"] =="Y":
        str_list = str_list + 'E'
    if df_post2017.loc[x,"9C2"] =="Y":
        str_list= str_list + 'F'
    if df_post2017.loc[x,"9C3"] =="Y":
        str_list = str_list + 'G'
    if df_post2017.loc[x,"9C4"] =="Y":
        str_list = str_list + 'H'
    if df_post2017.loc[x,"9D1"] =="Y":
        str_list = str_list + 'I'
    if df_post2017.loc[x,"9D2"] =="Y":
        str_list = str_list + 'J'

    Custody_List[x] = str_list
        
 df_post2017.insert(152, "Custody", Custody_List, True )

#Cleaning 11 - Disclosure Information
Disclosure_List = np.empty(len(df_post2017), dtype = 'object')

for x in  range(len(df_post2017.loc[:,"11A1"])):
    str_list =''
    if df_post2017.loc[x,"11A1"] == "Y":
        str_list = str_list + 'A'
    if df_post2017.loc[x,"11A2"] == "Y":
        str_list = str_list + 'B'
    if df_post2017.loc[x,"11B1"] == "Y":
        str_list = str_list + 'C'
    if df_post2017.loc[x,"11B2"] == "Y":
        str_list = str_list + 'D'
    if df_post2017.loc[x,"11C1"] == "Y":
        str_list = str_list + 'E'
    if df_post2017.loc[x,"11C2"] == "Y":
        str_list = str_list + 'F'
    if df_post2017.loc[x,"11C3"] == "Y":
        str_list = str_list + 'G'
    if df_post2017.loc[x,"11C4"] == "Y":
        str_list = str_list + 'H'
    if df_post2017.loc[x,"11C5"] == "Y":
        str_list = str_list + 'I'
    if df_post2017.loc[x,"11D1"] == "Y":
        str_list = str_list + 'J'
    if df_post2017.loc[x,"11D2"] == "Y":
        str_list = str_list + 'K'
    if df_post2017.loc[x,"11D3"] == "Y":
        str_list = str_list + 'L'
    if df_post2017.loc[x,"11D4"] == "Y":
        str_list = str_list + 'M'
    if df_post2017.loc[x,"11D5"] == "Y":
        str_list = str_list + 'N'
    if df_post2017.loc[x,"11E1"] == "Y":
        str_list = str_list + 'O'
    if df_post2017.loc[x,"11E2"] == "Y":
        str_list = str_list + 'P'
    if df_post2017.loc[x,"11E3"] == "Y":
        str_list = str_list + 'Q'
    if df_post2017.loc[x,"11E4"] == "Y":
        str_list = str_list + 'R'
    if df_post2017.loc[x,"11F"] == "Y":
        str_list = str_list + 'S'
    if df_post2017.loc[x,"11G"] == "Y":
        str_list = str_list + 'T'
    if df_post2017.loc[x,"11H1a"] == "Y":
        str_list = str_list + 'U'
    if df_post2017.loc[x,"11H1b"] == "Y":
        str_list = str_list + 'V'
    if df_post2017.loc[x,"11H1c"] == "Y":
        str_list = str_list + 'W'
    if df_post2017.loc[x,"11H2"]== "Y":
        str_list = str_list + 'X'
    Disclosure_List[x] = str_list
        
 df_post2017.insert(166, "Disclosure_List", Disclosure_List, True )  



#Cleaning 6
Business_Activities = np.empty(len(df_post2017), dtype = 'object')

for x in  range(len(df_post2017.loc[:,"6A1"])):
    str_list =''
    if df_post2017.loc[x,"6A1"] == "Y":
        str_list = str_list + 'A'
    if df_post2017.loc[x,"6A2"] == "Y":
        str_list = str_list + 'B'
    if df_post2017.loc[x,"6A3"] == "Y":
        str_list = str_list + 'C'
    if df_post2017.loc[x,"6A4"] == "Y":
        str_list = str_list + 'D'
    if df_post2017.loc[x,"6A5"] == "Y":
        str_list = str_list + 'E'
    if df_post2017.loc[x,"6A6"] == "Y":
        str_list = str_list + 'F'
    if df_post2017.loc[x,"6A7"] == "Y":
        str_list = str_list + 'G'
    if df_post2017.loc[x,"6A8"] == "Y":
        str_list = str_list + 'H'
    if df_post2017.loc[x,"6A9"] == "Y":
        str_list = str_list + 'I'
    if df_post2017.loc[x,"6A10"] == "Y":
        str_list = str_list + 'J'
    if df_post2017.loc[x,"6A11"] == "Y":
        str_list = str_list + 'K'
    if df_post2017.loc[x,"6A12"] == "Y":
        str_list = str_list + 'L'
    if df_post2017.loc[x,"6A13"] == "Y":
        str_list = str_list + 'M'
    if df_post2017.loc[x,"6A14"] == "Y":
        str_list = str_list + 'N'
    Business_Activities[x] = str_list
        
 df_post2017.insert(167, "Business_Activities", Business_Activities, True ) 

#Merging columns pre-2017 and post-2017
dfnew_pre2017 = df_pre2017[['FilingID',
 '1A',
 '1B',
 '1C-Legal',
 '1C-Business',
 '1C-New Name',
 '1D',
 '1E',
 '1F1-Street 1',
 '1F1-Street 2',
 '1F1-City',
 '1F1-State',
 '1F1-Country',
 '1F1-Postal',
 '1F3',
 '1F4',
 '1G-Street 1',
 '1G-Street 2',
 '1G-City',
 '1G-State',
 '1G-Country',
 '1G-Postal',
 '1J-Name',
 '1J-Title',
 '1J-Phone',
 '1J-Fax',
 '1J-Street 1',
 '1J-Street 2',
 '1J-City',
 '1J-State',
 '1J-Country',
 '1J-Postal',
 '1J-Email',
 'Reg Contact-Name',
 'Reg Contact-Title',
 'Reg Contact-Phone',
 'Reg Contact-Fax',
 'Reg Contact-Street 1',
 'Reg Contact-Street 2',
 'Reg Contact-City',
 'Reg Contact-State',
 'Reg Contact-Country',
 'Reg Contact-Postal',
 'Reg Contact-Email',
 '1N',
 '1N-CIK',
 '1O',
 '1P',
 '4A',
 '4B',
 '5B1-Number',
 '5B2-Number',
 '5C-Number',
 '5C2',
 '5D10-Other/5D1m-Other',
 '5F1',
 '5F2a',
 '5F2b',
 '5F2d',
 '5F2e',
 '6A7/6A14-Other',
 '9A2a',
 '9A2b',
 '9B2a',
 '9B2b',
 '9E',
 'Execution Type',
 'Signatory',
 'Execution Date',
 'Title',
 'ConInt',
 'Clients',
 'Custody',
 'Disclosure_List',
 'Business_Activities']]

dfnew_post2017 = df_post2017[['FilingID',
 '1A',
 '1B',
 '1B2',
 '1C-Legal',
 '1C-Business',
 '1C-New Name',
 '1D',
 '1E',
 '1F1-Street 1',
 '1F1-Street 2',
 '1F1-City',
 '1F1-State',
 '1F1-Country',
 '1F1-Postal',
 '1F3',
 '1F4',
 '1F5',
 '1G-Street 1',
 '1G-Street 2',
 '1G-City',
 '1G-State',
 '1G-Country',
 '1G-Postal',
 '1J-Name',
 '1J-Title',
 '1J-Phone',
 '1J-Fax',
 '1J-Street 1',
 '1J-Street 2',
 '1J-City',
 '1J-State',
 '1J-Country',
 '1J-Postal',
 '1J-Email',
 '1J2-Name',
 '1J2-EIN',
 'Reg Contact-Name',
 'Reg Contact-Title',
 'Reg Contact-Phone',
 'Reg Contact-Fax',
 'Reg Contact-Street 1',
 'Reg Contact-Street 2',
 'Reg Contact-City',
 'Reg Contact-State',
 'Reg Contact-Country',
 'Reg Contact-Postal',
 'Reg Contact-Email',
 '1N',
 '1N-CIK',
 '1O',
 '1P',
 '4A',
 '4B',
  '5B1',
 '5B2',
 '5C1',
 '5C2',
 '5D10-Other/5D1m-Other',
 '5F1',
 '5F2a',
 '5F2b',
 '5F2d',
 '5F2e',
 '6A7/6A14-Other',
 '9A2a',
 '9A2b',
 '9B2a',
 '9B2b',
 '9E',
 'Clients',
 'Custody',
 'Execution Type',
 'Signatory',
 'Execution Date',
 'Title',
 'ConInt',
 'Disclosure_List',
 'Business_Activities']]


dfnew_pre2017 = dfnew_pre2017.rename(columns={"5B1-Number":"5B1", "5B2-Number":"5B2", "5C-Number":"5C1"})


df_final = dfnew_pre2017.append(dfnew_post2017, ignore_index=True)

#Make all Upper case
temp = pd.DataFrame()
for col in list(df_final.columns):
    df_final[col] = df_final[col].str.upper()


df_base_ab = pd.merge(df_final, df_keep_bb, on='FilingID')

