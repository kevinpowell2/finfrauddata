#Schedule D 1B
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 1B')

csv_list_schedule_d_1b = ['IA_Schedule_D_1B_20150316_20150930.csv',  'IA_Schedule_D_1B_20170701_20170930.csv',
'IA_Schedule_D_1B_20151001_20151231.csv', 'IA_Schedule_D_1B_20171001_20171231.csv',
'IA_Schedule_D_1B_20160101_20160331.csv', 'IA_Schedule_D_1B_20180101_20180331.csv',
'IA_Schedule_D_1B_20160401_20160630.csv', 'IA_Schedule_D_1B_20180401_20180630.csv',
'IA_Schedule_D_1B_20160701_20160930.csv', 'IA_Schedule_D_1B_20180701_20180930.csv',
'IA_Schedule_D_1B_20161001_20161231.csv', 'IA_Schedule_D_1B_20181001_20181231.csv',
'IA_Schedule_D_1B_20170101_20170331.csv', 'Schedule_D_1B_20150315.csv',
'IA_Schedule_D_1B_20170401_20170630.csv']

#collect files
df_temp = pd.read_csv('IA_Schedule_D_1B_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1b = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_d_1b:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1b),(len(df_schedule_d_1b) + len(df_file)))
    df_schedule_d_1b = pd.concat([df_schedule_d_1b, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1b.columns:
        print(col)
        
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/ERA/schedule D/Schedule D 1B')

csv_list_d_1b_era = ['ERA_Schedule_D_1B_20150316_20150930.csv',  'ERA_Schedule_D_1B_20170701_20170930.csv',
'ERA_Schedule_D_1B_20151001_20151231.csv', 'ERA_Schedule_D_1B_20171001_20171231.csv',
'ERA_Schedule_D_1B_20160101_20160331.csv', 'ERA_Schedule_D_1B_20180101_20180331.csv',
'ERA_Schedule_D_1B_20160401_20160630.csv', 'ERA_Schedule_D_1B_20180401_20180630.csv',
'ERA_Schedule_D_1B_20160701_20160930.csv', 'ERA_Schedule_D_1B_20180701_20180930.csv',
'ERA_Schedule_D_1B_20161001_20161231.csv', 'ERA_Schedule_D_1B_20181001_20181231.csv',
'ERA_Schedule_D_1B_20170101_20170331.csv', 'Schedule_D_1B_20150315.csv',
'ERA_Schedule_D_1B_20170401_20170630.csv', 'ERA_Schedule_D_1B_20190101_20190331.csv',]
        

df_temp = pd.read_csv('ERA_Schedule_D_1B_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1b_era = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_d_1b_era:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1b_era),(len(df_schedule_d_1b_era) + len(df_file)))
    df_schedule_d_1b_era = pd.concat([df_schedule_d_1b_era, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1b_era.columns:
        print(col)
        
df_schedule_d1b_merged = df_schedule_d_1b.append(df_schedule_d_1b_era, ignore_index=True)


#select columns
df_keep_d_1b = df_schedule_d1b_merged[['FilingID','Other Business Name', 'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'OTHER']]
df_keep_d_1b.columns = ['FILINGID', 'OTHERBUSINESSNAMES_NAME_ORG',  'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'OTHER']

Jurisdictions = np.empty(len(df_keep_d_1b), dtype = 'object')

for x in  range(len(df_keep_d_1b.loc[:,"AL"])):
    str_list =''
    if df_keep_d_1b.loc[x,"AL"] =="Y":
        str_list = str_list + 'AL '
    if df_keep_d_1b.loc[x,"AK"] =="Y":
        str_list = str_list + 'AK '    
    if df_keep_d_1b.loc[x,"AZ"] =="Y":
        str_list = str_list + 'AZ '
    if df_keep_d_1b.loc[x,"AR"] =="Y":
        str_list = str_list + 'AR '
    if df_keep_d_1b.loc[x,"CA"] =="Y":
        str_list = str_list + 'CA '
    if df_keep_d_1b.loc[x,"CO"] =="Y":
        str_list = str_list + 'CO '
    if df_keep_d_1b.loc[x,"CT"] =="Y":
        str_list = str_list + 'CT '
    if df_keep_d_1b.loc[x,"DE"] =="Y":
        str_list = str_list + 'DE '
    if df_keep_d_1b.loc[x,"DC"] =="Y":
        str_list = str_list + 'DC '
    if df_keep_d_1b.loc[x,"FL"] =="Y":
        str_list = str_list + 'FL '
    if df_keep_d_1b.loc[x,"GA"] =="Y":
        str_list = str_list + 'GA '
    if df_keep_d_1b.loc[x,"GU"] =="Y":
        str_list = str_list + 'GU '
    if df_keep_d_1b.loc[x,"HI"] =="Y":
        str_list = str_list + 'HI '
    if df_keep_d_1b.loc[x,"ID"] =="Y":
        str_list = str_list + 'ID '
    if df_keep_d_1b.loc[x,"IL"] =="Y":
        str_list = str_list + 'IL '
    if df_keep_d_1b.loc[x,"IN"] =="Y":
        str_list = str_list + 'IN '
    if df_keep_d_1b.loc[x,"IA"] =="Y":
        str_list = str_list + 'IA '
    if df_keep_d_1b.loc[x,"KS"] =="Y":
        str_list = str_list + 'KS '
    if df_keep_d_1b.loc[x,"KY"] =="Y":
        str_list = str_list + 'KY '
    if df_keep_d_1b.loc[x,"LA"] =="Y":
        str_list = str_list + 'LA '
    if df_keep_d_1b.loc[x,"ME"] =="Y":
        str_list = str_list + 'ME '
    if df_keep_d_1b.loc[x,"MD"] =="Y":
        str_list = str_list + 'MD '
    if df_keep_d_1b.loc[x,"MA"] =="Y":
        str_list = str_list + 'MA '
    if df_keep_d_1b.loc[x,"MI"] =="Y":
        str_list = str_list + 'MI '
    if df_keep_d_1b.loc[x,"MN"] =="Y":
        str_list = str_list + 'MN '
    if df_keep_d_1b.loc[x,"MS"] =="Y":
        str_list = str_list + 'MS '
    if df_keep_d_1b.loc[x,"MO"] =="Y":
        str_list = str_list + 'MO '
    if df_keep_d_1b.loc[x,"MT"] =="Y":
        str_list = str_list + 'MT '
    if df_keep_d_1b.loc[x,"NE"] =="Y":
        str_list = str_list + 'NE '
    if df_keep_d_1b.loc[x,"NV"] =="Y":
        str_list = str_list + 'NV '
    if df_keep_d_1b.loc[x,"NC"] =="Y":
        str_list = str_list + 'NC '
    if df_keep_d_1b.loc[x,"ND"] =="Y":
        str_list = str_list + 'ND '
    if df_keep_d_1b.loc[x,"OH"] =="Y":
        str_list = str_list + 'OH '
    if df_keep_d_1b.loc[x,"OK"] =="Y":
        str_list = str_list + 'OK '
    if df_keep_d_1b.loc[x,"OR"] =="Y":
        str_list = str_list + 'OR '
    if df_keep_d_1b.loc[x,"PA"] =="Y":
        str_list = str_list + 'PA '
    if df_keep_d_1b.loc[x,"PR"] =="Y":
        str_list = str_list + 'PR '
    if df_keep_d_1b.loc[x,"RI"] =="Y":
        str_list = str_list + 'RI '
    if df_keep_d_1b.loc[x,"SC"] =="Y":
        str_list = str_list + 'SC '
    if df_keep_d_1b.loc[x,"SD"] =="Y":
        str_list = str_list + 'SD '
    if df_keep_d_1b.loc[x,"TN"] =="Y":
        str_list = str_list + 'TN '
    if df_keep_d_1b.loc[x,"TX"] =="Y":
        str_list = str_list + 'TX '
    if df_keep_d_1b.loc[x,"UT"] =="Y":
        str_list = str_list + 'UT '
    if df_keep_d_1b.loc[x,"VT"] =="Y":
        str_list = str_list + 'VT '
    if df_keep_d_1b.loc[x,"VI"] =="Y":
        str_list = str_list + 'VI '
    if df_keep_d_1b.loc[x,"VA"] =="Y":
        str_list = str_list + 'VA '
    if df_keep_d_1b.loc[x,"WA"] =="Y":
        str_list = str_list + 'WA '
    if df_keep_d_1b.loc[x,"WV"] =="Y":
        str_list = str_list + 'WV '
    if df_keep_d_1b.loc[x,"WI"] =="Y":
        str_list = str_list + 'WI '
    Jurisdictions[x] = str_list
        
 df_keep_d_1b.insert(2, "JURISDICTIONS", Jurisdictions, True )

df_final_D1B = df_keep_d_1b[['FILINGID', 'OTHERBUSINESSNAMES_NAME_ORG','JURISDICTIONS','OTHER']]
df_final_D1B = df_final_D1B.rename(columns={"OTHER":"OTHERJURISDICTIONS"})
#write file
df_final_D1B.to_csv('SCHEDULE_D_1B_ORG.csv')


#Schedule D 1F
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 1F')

csv_list_ria_d1f_pre2017 = ['IA_Schedule_D_1F_20150316_20150930.csv', 'IA_Schedule_D_1F_20170701_20170930.csv',
'IA_Schedule_D_1F_20151001_20151231.csv', 
'IA_Schedule_D_1F_20160101_20160331.csv', 
'IA_Schedule_D_1F_20160401_20160630.csv',
'IA_Schedule_D_1F_20160701_20160930.csv', 
'IA_Schedule_D_1F_20161001_20161231.csv',
'IA_Schedule_D_1F_20170101_20170331.csv', 'Schedule_D_1F_20150315.csv',
'IA_Schedule_D_1F_20170401_20170630.csv',]

df_temp = pd.read_csv('IA_Schedule_D_1F_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_ria_d1f_pre2017 = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_ria_d1f_pre2017:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_ria_d1f_pre2017),(len(df_schedule_ria_d1f_pre2017) + len(df_file)))
    df_schedule_ria_d1f_pre2017 = pd.concat([df_schedule_ria_d1f_pre2017, df_file])
    print(file, ":\n")
    for col in df_schedule_ria_d1f_pre2017.columns:
        print(col)

os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/ERA/schedule D/Schedule D 1F')

#era pre 2017
csv_list_era_d1f_pre2017 = ['ERA_Schedule_D_1F_20150316_20150930.csv', 'ERA_Schedule_D_1F_20170701_20170930.csv',
'ERA_Schedule_D_1F_20151001_20151231.csv', 
'ERA_Schedule_D_1F_20160101_20160331.csv', 
'ERA_Schedule_D_1F_20160401_20160630.csv',
'ERA_Schedule_D_1F_20160701_20160930.csv', 
'ERA_Schedule_D_1F_20161001_20161231.csv',
'ERA_Schedule_D_1F_20170101_20170331.csv', 'Schedule_D_1F_20150315.csv',
'ERA_Schedule_D_1F_20170401_20170630.csv',]

df_temp = pd.read_csv('ERA_Schedule_D_1F_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_era_d1f_pre2017 = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_era_d1f_pre2017:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_era_d1f_pre2017),(len(df_schedule_era_d1f_pre2017) + len(df_file)))
    df_schedule_era_d1f_pre2017 = pd.concat([df_schedule_era_d1f_pre2017, df_file])
    print(file, ":\n")
    for col in df_schedule_era_d1f_pre2017.columns:
        print(col)
        
df_schedule_d1f_pre2017_merged = df_schedule_ria_d1f_pre2017.append(df_schedule_era_d1f_pre2017, ignore_index=True)

#post 2017
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 1F')

csv_list_ria_d1f_post2017 = ['IA_Schedule_D_1F_20171001_20171231.csv',
'IA_Schedule_D_1F_20180101_20180331.csv',
'IA_Schedule_D_1F_20180401_20180630.csv',
'IA_Schedule_D_1F_20180701_20180930.csv',
'IA_Schedule_D_1F_20181001_20181231.csv']

df_temp = pd.read_csv('IA_Schedule_D_1F_20171001_20171231.csv', encoding='ISO-8859-1')
df_schedule_ria_d1f_post2017 = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_ria_d1f_post2017:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_ria_d1f_post2017),(len(df_schedule_ria_d1f_post2017) + len(df_file)))
    df_schedule_ria_d1f_post2017 = pd.concat([df_schedule_ria_d1f_post2017, df_file])
    print(file, ":\n")
    for col in df_schedule_ria_d1f_post2017.columns:
        print(col)
    
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/ERA/schedule D/Schedule D 1F')

csv_list_era_d1f_post2017 = ['ERA_Schedule_D_1F_20171001_20171231.csv',
'ERA_Schedule_D_1F_20180101_20180331.csv',
'ERA_Schedule_D_1F_20180401_20180630.csv',
'ERA_Schedule_D_1F_20180701_20180930.csv',
'ERA_Schedule_D_1F_20181001_20181231.csv', 'ERA_Schedule_D_1F_20190101_20190331.csv']

df_temp = pd.read_csv('ERA_Schedule_D_1F_20171001_20171231.csv', encoding='ISO-8859-1')
df_schedule_era_d1f_post2017 = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_era_d1f_post2017:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_era_d1f_post2017),(len(df_schedule_era_d1f_post2017) + len(df_file)))
    df_schedule_era_d1f_post2017 = pd.concat([df_schedule_era_d1f_post2017, df_file])
    print(file, ":\n")
    for col in df_schedule_era_d1f_post2017.columns:
        print(col)

df_d1f_post2017_merged = df_schedule_ria_d1f_post2017.append(df_schedule_era_d1f_post2017, ignore_index=True)

df_d1f_post2017_merged = df_d1f_post2017_merged[['FilingID',
 'Street 1',
 'Street 2',
 'City',
 'State',
 'Country',
 'Postal Code',
 'Private Residence',
 'Telephone Number',
 'Facsimile Number',
 'Branch Number',
 'Employees']]

df_d1f_final = df_schedule_d1f_pre2017_merged.append(df_d1f_post2017_merged, ignore_index=True)
df_d1f_final = df_d1f_final[['FilingID',
 'Street 1',
 'Street 2',
 'City',
 'State',
 'Country',
 'Postal Code',
 'Telephone Number',
 'Facsimile Number',
 'Branch Number',
 'Employees']]

df_d1f_final.columns = ['FILINGID', 'OFFICESTREET1_ADDR_LINE1',
                        'OFFICESTREET2_ADDR_LINE2',
                        'OFFICECITY_ADDR_CITY',
                        'OFFICESTATE_ADDR_STATE',
                        'OFFICECOUNTRY_ADDR_COUNTRY',
                        'OFFICEZIP_ADDR_POSTAL',
                        'OFFICEPHONE_PHONE_NUMBER',
                        'OFFICEFAX_PHONE_NUMBER',
                        'BRANCHNUMBER',
                        'IAEMPLOYEES']

df_d1f_final.to_csv("SCHEDULE_D_1F_OFFICES.csv")

#schedule d 1I
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 1I')

csv_list_schedule_d_1I_ria = ['IA_Schedule_D_1I_20150316_20150930.csv', 'IA_Schedule_D_1I_20170701_20170930.csv',
'IA_Schedule_D_1I_20151001_20151231.csv', 'IA_Schedule_D_1I_20171001_20171231.csv',
'IA_Schedule_D_1I_20160101_20160331.csv', 'IA_Schedule_D_1I_20180101_20180331.csv',
'IA_Schedule_D_1I_20160401_20160630.csv', 'IA_Schedule_D_1I_20180401_20180630.csv',
'IA_Schedule_D_1I_20160701_20160930.csv', 'IA_Schedule_D_1I_20180701_20180930.csv',
'IA_Schedule_D_1I_20161001_20161231.csv', 'IA_Schedule_D_1I_20181001_20181231.csv',
'IA_Schedule_D_1I_20170101_20170331.csv', 'Schedule_D_1I_20150315.csv',
'IA_Schedule_D_1I_20170401_20170630.csv']

df_temp = pd.read_csv('IA_Schedule_D_1I_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1i_ria = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_1I_ria:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1i_ria),(len(df_schedule_d_1i_ria) + len(df_file)))
    df_schedule_d_1i_ria = pd.concat([df_schedule_d_1i_ria, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1i_ria.columns:
        print(col)
df_schedule_d_1i_ria["WEBSITE"] = df_schedule_d_1i_ria["WEB_ADRS_NM"].fillna('') + df_schedule_d_1i_ria["Website"].fillna('')


os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/ERA/schedule D/Schedule D 1I')

csv_list_schedule_d_1I_era = ['ERA_Schedule_D_1I_20150316_20150930.csv', 'ERA_Schedule_D_1I_20170701_20170930.csv',
'ERA_Schedule_D_1I_20151001_20151231.csv', 'ERA_Schedule_D_1I_20171001_20171231.csv',
'ERA_Schedule_D_1I_20160101_20160331.csv', 'ERA_Schedule_D_1I_20180101_20180331.csv',
'ERA_Schedule_D_1I_20160401_20160630.csv', 'ERA_Schedule_D_1I_20180401_20180630.csv',
'ERA_Schedule_D_1I_20160701_20160930.csv', 'ERA_Schedule_D_1I_20180701_20180930.csv',
'ERA_Schedule_D_1I_20161001_20161231.csv', 'ERA_Schedule_D_1I_20181001_20181231.csv',
'ERA_Schedule_D_1I_20170101_20170331.csv', 'Schedule_D_1I_20150315.csv',
'ERA_Schedule_D_1I_20170401_20170630.csv', 'ERA_Schedule_D_1I_20190101_20190331.csv']

df_temp = pd.read_csv('ERA_Schedule_D_1I_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1i_era = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_1I_era:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1i_era),(len(df_schedule_d_1i_era) + len(df_file)))
    df_schedule_d_1i_era = pd.concat([df_schedule_d_1i_era, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1i_era.columns:
        print(col)
df_schedule_d_1i_era["WEBSITE"] = df_schedule_d_1i_era["WEB_ADRS_NM"].fillna('') + df_schedule_d_1i_era["Website"].fillna('')

df_schedule_d_1i = df_schedule_d_1i_ria.append(df_schedule_d_1i_era,ignore_index=True)
df_schedule_d_1i = df_schedule_d_1i[['FilingID', 'WEBSITE']]
df_schedule_d_1i = df_schedule_d_1i.rename(columns={'FilingID':'FILINGID'})

os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/Processed Data')

df_schedule_d_1i.to_csv("SCHEDULE_D_1I_WEBSITES.csv")


#schedule D 1L - Books & Records
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 1L')

csv_list_schedule_d_Books_and_Records = ['IA_Schedule_D_Books_and_Records_20150315_20150316_20150930.csv',
'IA_Schedule_D_Books_and_Records_20150315_20151001_20151231.csv',
'IA_Schedule_D_Books_and_Records_20160101_20160331.csv',
'IA_Schedule_D_Books_and_Records_20160401_20160630.csv',
'IA_Schedule_D_Books_and_Records_20160701_20160930.csv',
'IA_Schedule_D_Books_and_Records_20161001_20161231.csv',
'IA_Schedule_D_Books_and_Records_20170101_20170331.csv',
'IA_Schedule_D_Books_and_Records_20170401_20170630.csv',
'IA_Schedule_D_Books_and_Records_20170701_20170930.csv',
'IA_Schedule_D_Books_and_Records_20171001_20171231.csv',
'IA_Schedule_D_Books_and_Records_20180101_20180331.csv',
'IA_Schedule_D_Books_and_Records_20180401_20180630.csv',
'IA_Schedule_D_Books_and_Records_20180701_20180930.csv',
'IA_Schedule_D_Books_and_Records_20181001_20181231.csv',
'Schedule_D_Books_and_Records_20150315.csv']

df_temp = pd.read_csv('IA_Schedule_D_Books_and_Records_20150315_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1l_ria = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_Books_and_Records:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1l_ria),(len(df_schedule_d_1l_ria) + len(df_file)))
    df_schedule_d_1l_ria = pd.concat([df_schedule_d_1l_ria, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1l_ria.columns:
        print(col)
        
#have to do era books and records and 2A

#schedule D 4
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 4')

csv_list_schedule_d_4 = ['IA_Schedule_D_4_20150316_20150930.csv',  'IA_Schedule_D_4_20170701_20170930.csv',
'IA_Schedule_D_4_20151001_20151231.csv', 'IA_Schedule_D_4_20171001_20171231.csv',
'IA_Schedule_D_4_20160101_20160331.csv', 'IA_Schedule_D_4_20180101_20180331.csv',
'IA_Schedule_D_4_20160401_20160630.csv', 'IA_Schedule_D_4_20180401_20180630.csv',
'IA_Schedule_D_4_20160701_20160930.csv', 'IA_Schedule_D_4_20180701_20180930.csv',
'IA_Schedule_D_4_20161001_20161231.csv', 'IA_Schedule_D_4_20181001_20181231.csv',
'IA_Schedule_D_4_20170101_20170331.csv', 'Schedule_D_4_20150315.csv',
'IA_Schedule_D_4_20170401_20170630.csv']

df_temp = pd.read_csv('IA_Schedule_D_4_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_4 = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_4:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_4),(len(df_schedule_d_4) + len(df_file)))
    df_schedule_d_4 = pd.concat([df_schedule_d_4, df_file])
    print(file, ":\n")
    for col in df_schedule_d_4.columns:
        print(col)

df_schedule_d_4.to_csv("SCHEDULE_D_4_ACQUIREDFIRMS.csv")

#5G3
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 5G3')

csv_list_schedule_d_5G3 = ['IA_Schedule_D_5G3_20150316_20150930.csv',
'IA_Schedule_D_5G3_20151001_20151231.csv',
'IA_Schedule_D_5G3_20160101_20160331.csv',
'IA_Schedule_D_5G3_20160401_20160630.csv',
'IA_Schedule_D_5G3_20160701_20160930.csv',
'IA_Schedule_D_5G3_20161001_20161231.csv',
'IA_Schedule_D_5G3_20170101_20170331.csv',
'IA_Schedule_D_5G3_20170401_20170630.csv',
'IA_Schedule_D_5G3_20170701_20170930.csv',
'IA_Schedule_D_5G3_20171001_20171231.csv',
'IA_Schedule_D_5G3_20180101_20180331.csv',
'IA_Schedule_D_5G3_20180401_20180630.csv',
'IA_Schedule_D_5G3_20180701_20180930.csv',
'IA_Schedule_D_5G3_20181001_20181231.csv',
'Schedule_D_5G3_20150315.csv']

df_temp = pd.read_csv('IA_Schedule_D_5G3_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_5g3 = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_5G3:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_5g3),(len(df_schedule_d_5g3) + len(df_file)))
    df_schedule_d_5g3 = pd.concat([df_schedule_d_5g3, df_file])
    print(file, ":\n")
    for col in df_schedule_d_5g3.columns:
        print(col)

df_schedule_d_5g3 = df_schedule_d_5g3.iloc[2:]
df_schedule_d_5g3= df_schedule_d_5g3[['FilingID', 'SEC File Number', 'Series ID', 'RAUM']]
df_schedule_d_5g3 = df_schedule_d_5g3.rename(columns = {"SEC File Number": "SEC_NUMBER", "FilingID": "Investment Advisor FilingID"})

os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/Processed Data')
#df_base = pd.read_csv("BASE_ORG_HASH.csv")

#df_schedule_d_5g3 = df_schedule_d_5g3.merge(df_base[["SEC_NUMBER","BUSINESSNAME_NAME_ORG"]], on="SEC_NUMBER")
#df_schedule_d_5g3 = df_schedule_d_5g3.rename(columns = {"SEC_NUMBER":"Advisee SEC Number"})
df_schedule_d_5g3.to_csv("SCHEDULE_D_5G3_ADVISEES.csv", index=False)

#5I2
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 5I2')

csv_list_schedule_d_5I2 = ['IA_Schedule_D_5I2_20150316_20150930.csv',
'IA_Schedule_D_5I2_20151001_20151231.csv',
'IA_Schedule_D_5I2_20160101_20160331.csv',
'IA_Schedule_D_5I2_20160401_20160630.csv',
'IA_Schedule_D_5I2_20160701_20160930.csv',
'IA_Schedule_D_5I2_20161001_20161231.csv',
'IA_Schedule_D_5I2_20170101_20170331.csv',
'IA_Schedule_D_5I2_20170401_20170630.csv',
'IA_Schedule_D_5I2_20170701_20170930.csv',
'IA_Schedule_D_5I2_20171001_20171231.csv',
'IA_Schedule_D_5I2_20180101_20180331.csv',
'IA_Schedule_D_5I2_20180401_20180630.csv',
'IA_Schedule_D_5I2_20180701_20180930.csv',
'IA_Schedule_D_5I2_20181001_20181231.csv',
'Schedule_D_5I2_20150315.csv']

df_temp = pd.read_csv('IA_Schedule_D_5I2_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_5i2 = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_5I2:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_5i2),(len(df_schedule_d_5i2) + len(df_file)))
    df_schedule_d_5i2 = pd.concat([df_schedule_d_5i2, df_file])
    print(file, ":\n")
    for col in df_schedule_d_5i2.columns:
        print(col)

df_schedule_d_5i2 = df_schedule_d_5i2[[
 'FilingID',
 'Sponsor',
 'Wrap Fee Program',
 'Sponsor SEC File Number',
 'Sponsor CRD Number']]

df_schedule_d_5i2 = df_schedule_d_5i2.rename(columns = {'Sponsor':'Wrap Fee Sponsor'})
df_schedule_d_5i2 = df_schedule_d_5i2.iloc[3:]

df_schedule_d_5i2.to_csv("SCHEDULE_D_5I2_WRAPFEE.csv")

#5k3 - Custodians
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 5K3')

csv_list_schedule_d_5K3 = ['IA_Schedule_D_5K3_20171001_20171231.csv',
'IA_Schedule_D_5K3_20180101_20180331.csv',
'IA_Schedule_D_5K3_20180401_20180630.csv',
'IA_Schedule_D_5K3_20180701_20180930.csv',
'IA_Schedule_D_5K3_20181001_20181231.csv']

df_temp = pd.read_csv('IA_Schedule_D_5K3_20171001_20171231.csv', encoding='ISO-8859-1')
df_schedule_d_5k3 = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_5K3:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_5k3),(len(df_schedule_d_5k3) + len(df_file)))
    df_schedule_d_5k3 = pd.concat([df_schedule_d_5k3, df_file], sort=False)
    print(file, ":\n")
    for col in df_schedule_d_5k3.columns:
        print(col)

df_schedule_d_5k3 = df_schedule_d_5k3[[
 '5K(3)(a)',
 '5K(3)(b)',
 '5K(3)(c) City',
 '5K(3)(c) Country',
 '5K(3)(c) State',
 '5K(3)(d)',
 '5K(3)(e)',
 '5K(3)(f)',
 '5K(3)(g)',
 'Filing ID']]

df_schedule_d_5k3 = df_schedule_d_5k3.rename(columns={
 '5K(3)(a)':'Custodian Legal Name',
 '5K(3)(b)': 'Custodian Business Name',
 '5K(3)(c) City': "Custodian's Office City",
 '5K(3)(c) Country': "Custodian's Office Country",
 '5K(3)(c) State': "Custodian's Office State",
 '5K(3)(d)': "Custodian Related Person?",
 '5K(3)(e)': "Custodian SEC Number",
 '5K(3)(f)': "Custodian LEI Number",
 '5K(3)(g)': "Regulatory Assets Held",
 'Filing ID': 'FILINGID'})

  df_schedule_d_5k3=df_schedule_d_5k3.iloc[10:] 
  
 df_schedule_d_5k3 = df_schedule_d_5k3.merge(df_base[["FILINGID","BUSINESSNAME_NAME_ORG"]], on="FILINGID")
  df_schedule_d_5k3 = df_schedule_d_5k3[[
 'Custodian Legal Name',
 'Custodian Business Name',
 "Custodian's Office City",
 "Custodian's Office Country",
 "Custodian's Office State",
 'Custodian Related Person?',
 'Custodian SEC Number',
 'Custodian LEI Number',
 'Regulatory Assets Held',
 'FILINGID',
 'BUSINESSNAME_NAME_ORG']]
  os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/Processed Data')

  df_schedule_d_5k3.to_csv("SCHEDULE_D_5K3_CUSTODIANS.csv", index=False)
  
  #6A 
  os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/sch D/Schedule D 6A')
csv_list_schedule_d_6A_ria = ['IA_Schedule_D_6A_20150316_20150930.csv', 'IA_Schedule_D_6A_20170701_20170930.csv',
'IA_Schedule_D_6A_20151001_20151231.csv', 'IA_Schedule_D_6A_20171001_20171231.csv',
'IA_Schedule_D_6A_20160101_20160331.csv', 'IA_Schedule_D_6A_20180101_20180331.csv',
'IA_Schedule_D_6A_20160401_20160630.csv', 'IA_Schedule_D_6A_20180401_20180630.csv',
'IA_Schedule_D_6A_20160701_20160930.csv', 'IA_Schedule_D_6A_20180701_20180930.csv',
'IA_Schedule_D_6A_20161001_20161231.csv', 'IA_Schedule_D_6A_20181001_20181231.csv',
'IA_Schedule_D_6A_20170101_20170331.csv', 'Schedule_D_6A_20150315.csv',
'IA_Schedule_D_6A_20170401_20170630.csv']

df_temp = pd.read_csv('IA_Schedule_D_6A_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_6A_ria = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_6A_ria:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_6A_ria),(len(df_schedule_d_6A_ria) + len(df_file)))
    df_schedule_d_6A_ria = pd.concat([df_schedule_d_6A_ria, df_file], sort=False)
    print(file, ":\n")
    for col in df_schedule_d_6A_ria.columns:
        print(col)
        
#era
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/ERA/schedule D/Schedule D 6A')
csv_list_schedule_d_6A_era = ['ERA_Schedule_D_6A_20150316_20150930.csv', 'ERA_Schedule_D_6A_20170701_20170930.csv',
'ERA_Schedule_D_6A_20151001_20151231.csv', 'ERA_Schedule_D_6A_20171001_20171231.csv',
'ERA_Schedule_D_6A_20160101_20160331.csv', 'ERA_Schedule_D_6A_20180101_20180331.csv',
'ERA_Schedule_D_6A_20160401_20160630.csv', 'ERA_Schedule_D_6A_20180401_20180630.csv',
'ERA_Schedule_D_6A_20160701_20160930.csv', 'ERA_Schedule_D_6A_20180701_20180930.csv',
'ERA_Schedule_D_6A_20161001_20161231.csv', 'ERA_Schedule_D_6A_20181001_20181231.csv',
'ERA_Schedule_D_6A_20170101_20170331.csv', 'Schedule_D_6A_20150315.csv',
'ERA_Schedule_D_6A_20170401_20170630.csv', 'ERA_Schedule_D_6A_20190101_20190331.csv']

df_temp = pd.read_csv('ERA_Schedule_D_6A_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_6A_era = pd.DataFrame(df_temp.columns)

for file in csv_list_schedule_d_6A_era:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_6A_era),(len(df_schedule_d_6A_era) + len(df_file)))
    df_schedule_d_6A_era = pd.concat([df_schedule_d_6A_era, df_file], sort=False)
    print(file, ":\n")
    for col in df_schedule_d_6A_era.columns:
        print(col)

df_schedule_d_6A = df_schedule_d_6A_ria.append(df_schedule_d_6A_era,ignore_index=True)
df_schedule_d_6A = df_schedule_d_6A[[ 'Accountant',
 'BD',
 'Bank',
 'FCM',
 'FilingID',
 'Insurance',
 'Lawyer',
 'Municipal',
 'Other',
 'Other Description',
 'Other Name',
 'Pool',
 'RR',
 'Real Estate',
 'Swap Dealer',
 'Swap Participant',
 'Trust']]

 df_schedule_d_6A = df_schedule_d_6A.iloc[17:]
 
#encoding
Business_Activities = np.empty(len(df_schedule_d_6A), dtype = 'object')

for x in  range(len(df_schedule_d_6A.loc[:,"BD"])):
    str_list =''
    if df_schedule_d_6A.loc[x,"BD"] == "Y":
        str_list = str_list + 'A'
    if df_schedule_d_6A.loc[x,"RR"] == "Y":
        str_list = str_list + 'B'
    if df_schedule_d_6A.loc[x,"Pool"] == "Y":
        str_list = str_list + 'C'
    if df_schedule_d_6A.loc[x,"FCM"] == "Y":
        str_list = str_list + 'D'
    if df_schedule_d_6A.loc[x,"Real Estate"] == "Y":
        str_list = str_list + 'E'
    if df_schedule_d_6A.loc[x,"Insurance"] == "Y":
        str_list = str_list + 'F'
    if df_schedule_d_6A.loc[x,"Bank"] == "Y":
        str_list = str_list + 'G'
    if df_schedule_d_6A.loc[x,"Trust"] == "Y":
        str_list = str_list + 'H'
    if df_schedule_d_6A.loc[x,"Municipal"] == "Y":
        str_list = str_list + 'I'
    if df_schedule_d_6A.loc[x,"Swap Dealer"] == "Y":
        str_list = str_list + 'J'
    if df_schedule_d_6A.loc[x,"Swap Participant"] == "Y":
        str_list = str_list + 'K'
    if df_schedule_d_6A.loc[x,"Accountant"] == "Y":
        str_list = str_list + 'L'
    if df_schedule_d_6A.loc[x,"Lawyer"] == "Y":
        str_list = str_list + 'M'
    if df_schedule_d_6A.loc[x,"Other"] == "Y":
        str_list = str_list + 'N'
    Business_Activities[x] = str_list
        
 df_schedule_d_6A.insert(17, "Business_Activities", Business_Activities, True )  
 
 df_schedule_d_6A = df_schedule_d_6A[[
 'FilingID',
 'Other Name',
 'Business_Activities',
  'Other Description']]
 
 df_schedule_d_6A.to_csv("SCHEDULE_D_6A_OTHERBUSINESSES.csv")
 
 