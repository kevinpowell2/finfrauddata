# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 09:10:20 2019
#Writes the CCO dictionary
@author: KevinPowell
"""

import os
os.chdir('C:/Users/KevinPowell/Documents/Intern2019/FincFraudData/finfrauddata/Data/FINRA data/Processed Data')
import pandas as pd
import numpy as np
import json

#read in files
df_cco = pd.read_csv('BASE_CCO_HASH.csv')
hash_list_cco = pd.read_csv('CCO_HASH_LIST.csv')

cnt = 0
cco_dict = {'ENTITIES': {}}
for key in hash_list_cco['0']:
    df_line = df_cco[df_cco['CCOHASHKEY']==key]
    cco_dict['ENTITIES'].update(  {str(cnt): {
    "RECORD_ID"                : key,
    "NAME_FULL"                : df_line.head(1).CCO_NAME_FULL.item(),
    'ADDRESSES'                : [{'ADDR_TYPE'       : 'CCO',
                                  'ADDR_CITY'       : df_line.head(1).OFFICECITY_ADDR_CITY.item(),
                                  'ADDR_COUNTRY'    : df_line.head(1).OFFICECOUNTRY_ADDR_COUNTRY.item(),
                                  'ADDR_POSTAL_CODE': df_line.head(1).OFFICEZIP_ADDR_POSTAL.item(),
                                  'ADDR_STATE'      : df_line.head(1).OFFICESTATE_ADDR_STATE.item(),
                                  'ADDR_LINE1'      : df_line.head(1).OFFICESTREET1_ADDR_LINE1.item(),
                                  'ADDR_LINE2'      : df_line.head(1).OFFICESTREET2_ADDR_LINE2.item()
                                 },
    'BUSNAMECHANGE'            : df_line.BUSNAMECHANGE.tolist(),
    'LEGNMECHNG'               : df_line.LEGALNAMECHANGE.tolist(),
    'NEW_NME'                  : df_line.NEWNAME_NAME_ORG.tolist(),
    'SEC_NUMBER'               : df_line.head(1).SEC_NUMBER.item(),
    'CRD_NUMBER'               : df_line.head(1).CRD_NUMBER.item(),