# -*- coding: utf-8 -*-
"""
Processing Raw SEC Form ADV data and store processed csv foir ingetsion into Senzing
6/27/19
Kevin Powell
"""
import pandas as pd
import numpy as np
#Base A

#csv_list =  ['ADV_Base_A_20150315.csv', 'IA_ADV_Base_A_20150316_20150930.csv', 'IA_ADV_Base_A_20151001_20151231.csv',
#'IA_ADV_Base_A_20160101_20160331.csv','IA_ADV_Base_A_20160401_20160630.csv', 'IA_ADV_Base_A_20160701_20160930.csv',
#'IA_ADV_Base_A_20161001_20161231.csv', 'IA_ADV_Base_A_20170101_20170331.csv','IA_ADV_Base_A_20170401_20170630.csv',
#'IA_ADV_Base_A_20170701_20170930.csv','IA_ADV_Base_A_20171001_20171231.csv', 'IA_ADV_Base_A_20180101_20180331.csv',
#'IA_ADV_Base_A_20180401_20180630.csv', 'IA_ADV_Base_A_20180701_20180930.csv', 'IA_ADV_Base_A_20181001_20181231.csv']

#colums to keep
#colName =  [...]

#new column names 
#new_colName = [...]

#readfile1
#df1 = read_csv(csv_list[0])

#rename
#df1.rename(['A' :'a', ... ])

#add row
#df1.index = df1.index + 1
#df1[0]= ['GES'...]
#df1.sort_index()

#Make all Upper case

#Other cleaning tasks

#next file in list
#dfNext = pd.read_csv(csv_list[1])

#.....
#Base B
csv_list_base_b = ['ADV_Base_B_20150315.csv', 'IA_ADV_Base_B_20170401_20170630.csv',
'IA_ADV_Base_B_20150316_20150930.csv', 'IA_ADV_Base_B_20170701_20170930.csv',
'IA_ADV_Base_B_20151001_20151231.csv',
'IA_ADV_Base_B_20160101_20160331.csv',
'IA_ADV_Base_B_20160401_20160630.csv',
'IA_ADV_Base_B_20160701_20160930.csv',
'IA_ADV_Base_B_20161001_20161231.csv',
'IA_ADV_Base_B_20170101_20170331.csv']

#Schedule AB
csv_list_schedule_ab = ['IA_Schedule_A_B_20150316_20150930.csv', 'IA_Schedule_A_B_20170701_20170930.csv',
'IA_Schedule_A_B_20151001_20151231.csv', 'IA_Schedule_A_B_20160101_20160331.csv',
'IA_Schedule_A_B_20160401_20160630.csv', 'IA_Schedule_A_B_20160701_20160930.csv',
'IA_Schedule_A_B_20161001_20161231.csv', 'IA_Schedule_A_B_20170101_20170331.csv',
'Schedule_A_B_20150315.csv', 'IA_Schedule_A_B_20170401_20170630.csv',
'ERA_Schedule_A_B_20150315.csv', 'ERA_Schedule_A_B_20170401_20170630.csv',
'ERA_Schedule_A_B_20150316_20150930.csv', 'ERA_Schedule_A_B_20170701_20170930.csv',
'ERA_Schedule_A_B_20151001_20151231.csv', 'ERA_Schedule_A_B_20160101_20160331.csv',
'ERA_Schedule_A_B_20160401_20160630.csv', 'ERA_Schedule_A_B_20160701_20160930.csv',
'ERA_Schedule_A_B_20161001_20161231.csv', 'ERA_Schedule_A_B_20170101_20170331.csv']

csv_list_schedule_ab_Oct_2017 = ['IA_Schedule_A_B_20171001_20171231.csv', 'IA_Schedule_A_B_20180101_20180331.csv',
'IA_Schedule_A_B_20180401_20180630.csv',
'IA_Schedule_A_B_20180701_20180930.csv',
'IA_Schedule_A_B_20181001_20181231.csv',
'ERA_Schedule_A_B_20171001_20171231.csv',
'ERA_Schedule_A_B_20180101_20180331.csv',
'ERA_Schedule_A_B_20180401_20180630.csv',
'ERA_Schedule_A_B_20180701_20180930.csv',
'ERA_Schedule_A_B_20181001_20181231.csv',
'ERA_Schedule_A_B_20190101_20190331.csv']

#Schedule D 1B
csv_list_schedule_d_1b = ['IA_Schedule_D_1B_20150316_20150930.csv',  'IA_Schedule_D_1B_20170701_20170930.csv',
'IA_Schedule_D_1B_20151001_20151231.csv',
'IA_Schedule_D_1B_20160101_20160331.csv',
'IA_Schedule_D_1B_20160401_20160630.csv',
'IA_Schedule_D_1B_20160701_20160930.csv',
'IA_Schedule_D_1B_20161001_20161231.csv',
'IA_Schedule_D_1B_20170101_20170331.csv', 'Schedule_D_1B_20150315.csv',
'IA_Schedule_D_1B_20170401_20170630.csv', 'ERA_Schedule_D_1B_20150316_20150930.csv',
'ERA_Schedule_D_1B_20151001_20151231.csv',
'ERA_Schedule_D_1B_20160101_20160331.csv',
'ERA_Schedule_D_1B_20160401_20160630.csv',
'ERA_Schedule_D_1B_20160701_20160930.csv',
'ERA_Schedule_D_1B_20161001_20161231.csv',
'ERA_Schedule_D_1B_20170101_20170331.csv',
'ERA_Schedule_D_1B_20170401_20170630.csv',
'ERA_Schedule_D_1B_20170701_20170930.csv',
'ERA_Schedule_D_1B_20150315.csv']

csv_list_schedule_d_1b_Oct_2017 = ['IA_Schedule_D_1B_20171001_20171231.csv',
'IA_Schedule_D_1B_20180101_20180331.csv',
'IA_Schedule_D_1B_20180401_20180630.csv',
'IA_Schedule_D_1B_20180701_20180930.csv',
'IA_Schedule_D_1B_20181001_20181231.csv',
'ERA_Schedule_D_1B_20171001_20171231.csv',
'ERA_Schedule_D_1B_20180101_20180331.csv',
'ERA_Schedule_D_1B_20180401_20180630.csv',
'ERA_Schedule_D_1B_20180701_20180930.csv',
'ERA_Schedule_D_1B_20181001_20181231.csv',
'ERA_Schedule_D_1B_20190101_20190331.csv',
'ERA_Schedule_D_1B_20150315.csv']

#Schedule D 1F
csv_list_schedule_d_1f = ['IA_Schedule_D_1F_20150316_20150930.csv',
'IA_Schedule_D_1F_20170701_20170930.csv',
'IA_Schedule_D_1F_20151001_20151231.csv',
'IA_Schedule_D_1F_20160101_20160331.csv',
'IA_Schedule_D_1F_20160401_20160630.csv',
'IA_Schedule_D_1F_20160701_20160930.csv',
'IA_Schedule_D_1F_20161001_20161231.csv',
'IA_Schedule_D_1F_20170101_20170331.csv',
'Schedule_D_1F_20150315.csv',
'IA_Schedule_D_1F_20170401_20170630.csv',
'ERA_Schedule_D_1F_20150316_20150930.csv',
'ERA_Schedule_D_1F_20151001_20151231.csv',
'ERA_Schedule_D_1F_20160101_20160331.csv',
'ERA_Schedule_D_1F_20160401_20160630.csv',
'ERA_Schedule_D_1F_20160701_20160930.csv',
'ERA_Schedule_D_1F_20161001_20161231.csv',
'ERA_Schedule_D_1F_20170101_20170331.csv',
'ERA_Schedule_D_1F_20170401_20170630.csv',
'ERA_Schedule_D_1F_20170701_20170930.csv',
'ERA_Schedule_D_1F_20150315.csv']

csv_list_schedule_d_1f_Oct_2017 = ['IA_Schedule_D_1F_20171001_20171231.csv',
'IA_Schedule_D_1F_20180101_20180331.csv',
'IA_Schedule_D_1F_20180401_20180630.csv',
'IA_Schedule_D_1F_20180701_20180930.csv',
'IA_Schedule_D_1F_20181001_20181231.csv',
'ERA_Schedule_D_1F_20180101_20180331.csv',
'ERA_Schedule_D_1F_20180401_20180630.csv',
'ERA_Schedule_D_1F_20180701_20180930.csv',
'ERA_Schedule_D_1F_20181001_20181231.csv',
'ERA_Schedule_D_1F_20190101_20190331.csv']

#Schedule D 1I
csv_list_schedule_d_1I = ['IA_Schedule_D_1I_20150316_20150930.csv', 'IA_Schedule_D_1I_20170701_20170930.csv',
'IA_Schedule_D_1I_20151001_20151231.csv', 'IA_Schedule_D_1I_20160101_20160331.csv',
'IA_Schedule_D_1I_20160401_20160630.csv', 'IA_Schedule_D_1I_20160701_20160930.csv',
'IA_Schedule_D_1I_20161001_20161231.csv', 'IA_Schedule_D_1I_20170101_20170331.csv',
'Schedule_D_1I_20150315.csv', 'IA_Schedule_D_1I_20170401_20170630.csv',
'ERA_Schedule_D_1I_20150316_20150930.csv', 'ERA_Schedule_D_1I_20151001_20151231.csv',
'ERA_Schedule_D_1I_20160101_20160331.csv', 'ERA_Schedule_D_1I_20160401_20160630.csv',
'ERA_Schedule_D_1I_20160701_20160930.csv', 'ERA_Schedule_D_1I_20161001_20161231.csv',
'ERA_Schedule_D_1I_20170101_20170331.csv', 'ERA_Schedule_D_1I_20170401_20170630.csv',
'ERA_Schedule_D_1I_20170701_20170930.csv',
'Schedule_D_1I_20150315.csv']

csv_list_schedule_d_1I_Oct_2017 = ['IA_Schedule_D_1I_20171001_20171231.csv', 'IA_Schedule_D_1I_20180101_20180331.csv',
'IA_Schedule_D_1I_20180401_20180630.csv', 'IA_Schedule_D_1I_20180701_20180930.csv',
'IA_Schedule_D_1I_20181001_20181231.csv', 'ERA_Schedule_D_1I_20171001_20171231.csv',
'ERA_Schedule_D_1I_20180101_20180331.csv', 'ERA_Schedule_D_1I_20180401_20180630.csv',
'ERA_Schedule_D_1I_20180701_20180930.csv', 'ERA_Schedule_D_1I_20181001_20181231.csv',
'ERA_Schedule_D_1I_20190101_20190331.csv']

csv_list_era_1M = ['ERA_Schedule_D_1M_20150316_20150930.csv',
'ERA_Schedule_D_1M_20151001_20151231.csv',
'ERA_Schedule_D_1M_20160101_20160331.csv',
'ERA_Schedule_D_1M_20160401_20160630.csv',
'Schedule_D_1M_20150315.csv']

#Schedule D 2A
csv_list_schedule_d_2A = ['IA_Schedule_D_2A_Related_20150316_20150930.csv',
'IA_Schedule_D_2A_Related_20151001_20151231.csv',
'IA_Schedule_D_2A_Related_20160101_20160331.csv',
'IA_Schedule_D_2A_Related_20160401_20160630.csv',
'IA_Schedule_D_2A_Related_20160701_20160930.csv',
'IA_Schedule_D_2A_Related_20161001_20161231.csv',
'IA_Schedule_D_2A_Related_20170101_20170331.csv',
'IA_Schedule_D_2A_Related_20170401_20170630.csv',
'IA_Schedule_D_2A_Related_20170701_20170930.csv',
'Schedule_D_2A_Exemptive_Order_20150315.csv',
'Schedule_D_2A_Multistate_20150315.csv',
'Schedule_D_2A_Newly_Formed_20150315.csv',
'Schedule_D_2A_Related_20150315.csv']

csv_list_schedule_d_2A_Oct_2017 = ['IA_Schedule_D_2A_Related_20171001_20171231.csv',
'IA_Schedule_D_2A_Related_20180101_20180331.csv',
'IA_Schedule_D_2A_Related_20180401_20180630.csv',
'IA_Schedule_D_2A_Related_20180701_20180930.csv',
'IA_Schedule_D_2A_Related_20181001_20181231.csv']

#Schedule D 4
csv_list_schedule_d_4 = ['IA_Schedule_D_4_20150316_20150930.csv',  'IA_Schedule_D_4_20170701_20170930.csv',
'IA_Schedule_D_4_20151001_20151231.csv',
'IA_Schedule_D_4_20160101_20160331.csv',
'IA_Schedule_D_4_20160401_20160630.csv',
'IA_Schedule_D_4_20160701_20160930.csv',
'IA_Schedule_D_4_20161001_20161231.csv',
'IA_Schedule_D_4_20170101_20170331.csv', 'Schedule_D_4_20150315.csv',
'IA_Schedule_D_4_20170401_20170630.csv']

#Schedule D 5G3
csv_list_schedule_d_5G3 = ['IA_Schedule_D_5G3_20150316_20150930.csv',
'IA_Schedule_D_5G3_20151001_20151231.csv',
'IA_Schedule_D_5G3_20160101_20160331.csv',
'IA_Schedule_D_5G3_20160401_20160630.csv',
'IA_Schedule_D_5G3_20160701_20160930.csv',
'IA_Schedule_D_5G3_20161001_20161231.csv',
'IA_Schedule_D_5G3_20170101_20170331.csv',
'IA_Schedule_D_5G3_20170401_20170630.csv',
'Schedule_D_5G3_20150315.csv']

csv_list_schedule_d_5G3_Oct_2017 = ['IA_Schedule_D_5G3_20171001_20171231.csv',
'IA_Schedule_D_5G3_20180101_20180331.csv',
'IA_Schedule_D_5G3_20180401_20180630.csv',
'IA_Schedule_D_5G3_20180701_20180930.csv',
'IA_Schedule_D_5G3_20181001_20181231.csv',]

#Schedule D 5I2
csv_list_schedule_d_5I2 = ['IA_Schedule_D_5I2_20150316_20150930.csv',
'IA_Schedule_D_5I2_20151001_20151231.csv',
'IA_Schedule_D_5I2_20160101_20160331.csv',
'IA_Schedule_D_5I2_20160401_20160630.csv',
'IA_Schedule_D_5I2_20160701_20160930.csv',
'IA_Schedule_D_5I2_20161001_20161231.csv',
'IA_Schedule_D_5I2_20170101_20170331.csv',
'IA_Schedule_D_5I2_20170401_20170630.csv',
'IA_Schedule_D_5I2_20170701_20170930.csv',
'Schedule_D_5I2_20150315.csv']

csv_list_schedule_d_5I2_Oct_2017 = ['IA_Schedule_D_5I2_20171001_20171231.csv',
'IA_Schedule_D_5I2_20180101_20180331.csv',
'IA_Schedule_D_5I2_20180401_20180630.csv',
'IA_Schedule_D_5I2_20180701_20180930.csv',
'IA_Schedule_D_5I2_20181001_20181231.csv']

#Schedule D 5K1
csv_list_schedule_d_5K1 = ['IA_Schedule_D_5K1_20171001_20171231.csv',
'IA_Schedule_D_5K1_20180101_20180331.csv',
'IA_Schedule_D_5K1_20180401_20180630.csv',
'IA_Schedule_D_5K1_20180701_20180930.csv',
'IA_Schedule_D_5K1_20181001_20181231.csv']

csv_list_schedule_d_5K2 = ['IA_Schedule_D_5K2_20171001_20171231.csv',
'IA_Schedule_D_5K2_20180101_20180331.csv',
'IA_Schedule_D_5K2_20180401_20180630.csv',
'IA_Schedule_D_5K2_20180701_20180930.csv',
'IA_Schedule_D_5K2_20181001_20181231.csv']

df_temp = pd.read_csv('IA_Schedule_D_5G3_20181001_20181231.csv', encoding='ISO-8859-1')
df_schedule_d_5k2 = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_d_5K2:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_5k2),(len(df_schedule_d_5k2) + len(df_file)))
    df_schedule_d_5k2 = pd.concat([df_schedule_d_5k2, df_file])
    print(file, ":\n")
    for col in df_schedule_d_5k2.columns:
        print(col)
        
        
#selct columns
df_schedule_d_5k2.head()
df_keep_d_5k2 = df_schedule_d_5k2[['Filing ID', '5K(2)(a)(i)(1)10', '5K(2)(a)(i)(2)10', '5K(2)(a)(i)(3)(a)10', '5K(2)(a)(i)(3)(b)10', '5K(2)(a)(i)(3)(c)10', '5K(2)(a)(i)(3)(d)10', '5K(2)(a)(i)(3)(e)10', '5K(2)(a)(i)(3)(f)10', '5K(2)(a)(i)(1)10-149', '5K(2)(a)(i)(2)10-149', '5K(2)(a)(i)(3)(a)10-149', '5K(2)(a)(i)(3)(b)10-149', '5K(2)(a)(i)(3)(c)10-149', '5K(2)(a)(i)(3)(d)10-149', '5K(2)(a)(i)(3)(e)10-149', '5K(2)(a)(i)(3)(f)10-149', '5K(2)(a)(i)(1)150', '5K(2)(a)(i)(2)150', '5K(2)(a)(i)(3)(a)150', '5K(2)(a)(i)(3)(b)150', '5K(2)(a)(i)(3)(c)150', '5K(2)(a)(i)(3)(d)150', '5K(2)(a)(i)(3)(e)150', '5K(2)(a)(i)(3)(f)150', '5K(2)(a)(i)Desc', '5K(2)(a)(ii)(1)10', '5K(2)(a)(ii)(2)10', '5K(2)(a)(ii)(3)(a)10', '5K(2)(a)(ii)(3)(b)10', '5K(2)(a)(ii)(3)(c)10', '5K(2)(a)(ii)(3)(d)10', '5K(2)(a)(ii)(3)(e)10', '5K(2)(a)(ii)(3)(f)10', '5K(2)(a)(ii)(1)10-149', '5K(2)(a)(ii)(2)10-149', '5K(2)(a)(ii)(3)(a)10-149', '5K(2)(a)(ii)(3)(b)10-149', '5K(2)(a)(ii)(3)(c)10-149', '5K(2)(a)(ii)(3)(d)10-149', '5K(2)(a)(ii)(3)(e)10-149', '5K(2)a)(ii)(3)(f)10-149', '5K(2)(a)(ii)(1)150', '5K(2)(a)(ii)(2)150', '5K(2)(a)(ii)(3)(a)150', '5K(2)(a)(ii)(3)(b)150', '5K(2)(a)(ii)(3)(c)150', '5K(2)(a)(ii)(3)(d)150', '5K(2)(a)(ii)(3)(e)150', '5K(2)(a)(ii)(3)(f)150', '5K(2)(a)(ii)Desc', '5K(2)(b)(1)105K(2)(b)(2)10', '5K(2)(b)(1)10-149', '5K(2)(b)(2)10-149', '5K(2)(b)(1)150', '5K(2)(b)(2)150', '5K(2)(b)Desc']]

#add GES names
#row_ges = pd.DataFrame([['ACCT_NUM','WEBSITE']], columns = df_keep_d_5k2.columns)
#df_keep_d_5k2 = pd.concat([row_ges, df_keep_d_5k2]).reset_index(drop=True)
#write file
df_keep_d_5k2.to_csv('soname.csv')

csv_list_schedule_d_5K3 = ['IA_Schedule_D_5K3_20171001_20171231.csv',
'IA_Schedule_D_5K3_20180101_20180331.csv',
'IA_Schedule_D_5K3_20180401_20180630.csv',
'IA_Schedule_D_5K3_20180701_20180930.csv',
'IA_Schedule_D_5K3_20181001_20181231.csv']

csv_list_schedule_d_6A = ['IA_Schedule_D_6A_20150316_20150930.csv',
'IA_Schedule_D_6A_20170701_20170930.csv',
'IA_Schedule_D_6A_20151001_20151231.csv',
'IA_Schedule_D_6A_20160101_20160331.csv',
'IA_Schedule_D_6A_20160401_20160630.csv',
'IA_Schedule_D_6A_20160701_20160930.csv',
'IA_Schedule_D_6A_20161001_20161231.csv',
'IA_Schedule_D_6A_20170101_20170331.csv',
'Schedule_D_6A_20150315.csv',
'IA_Schedule_D_6A_20170401_20170630.csv',
'ERA_Schedule_D_6A_20150316_20150930.csv',
'ERA_Schedule_D_6A_20151001_20151231.csv',
'ERA_Schedule_D_6A_20160101_20160331.csv',
'ERA_Schedule_D_6A_20160401_20160630.csv',
'ERA_Schedule_D_6A_20160701_20160930.csv',
'ERA_Schedule_D_6A_20161001_20161231.csv',
'ERA_Schedule_D_6A_20170101_20170331.csv',
'ERA_Schedule_D_6A_20170401_20170630.csv',
'ERA_Schedule_D_6A_20170701_20170930.csv',
'Schedule_D_6A_20150315.csv']

csv_list_schedule_d_6A_Oct_2017 = ['IA_Schedule_D_6A_20171001_20171231.csv',
 'IA_Schedule_D_6A_20180101_20180331.csv',
 'IA_Schedule_D_6A_20180401_20180630.csv'
 'IA_Schedule_D_6A_20180701_20180930.csv',
 'IA_Schedule_D_6A_20181001_20181231.csv',
 'IA_Schedule_D_6A_20170401_20170630.csv',
 'ERA_Schedule_D_6A_20171001_20171231.csv',
'ERA_Schedule_D_6A_20180101_20180331.csv', 
'ERA_Schedule_D_6A_20180401_20180630.csv',
'ERA_Schedule_D_6A_20180701_20180930.csv',
'ERA_Schedule_D_6A_20181001_20181231.csv', 
'ERA_Schedule_D_6A_20190101_20190331.csv']

csv_list_schedule_d_6B2 = ['IA_Schedule_D_6B2_20150316_20150930.csv',
'IA_Schedule_D_6B2_20151001_20151231.csv',
'IA_Schedule_D_6B2_20160101_20160331.csv',
'IA_Schedule_D_6B2_20160401_20160630.csv',
'IA_Schedule_D_6B2_20160701_20160930.csv',
'IA_Schedule_D_6B2_20161001_20161231.csv',
'IA_Schedule_D_6B2_20170101_20170331.csv',
'IA_Schedule_D_6B2_20170401_20170630.csv',
'IA_Schedule_D_6B2_20170701_20170930.csv',
'Schedule_D_6B2_20150315.csv',
'ERA_Schedule_D_6B2_20150316_20150930.csv',
'ERA_Schedule_D_6B2_20151001_20151231.csv',
'ERA_Schedule_D_6B2_20160101_20160331.csv',
'ERA_Schedule_D_6B2_20160401_20160630.csv',
'ERA_Schedule_D_6B2_20160701_20160930.csv',
'ERA_Schedule_D_6B2_20161001_20161231.csv',
'ERA_Schedule_D_6B2_20170101_20170331.csv',
'ERA_Schedule_D_6B2_20170401_20170630.csv',
'ERA_Schedule_D_6B2_20170701_20170930.csv',
'Schedule_D_6B2_20150315.csv']

csv_list_schedule_d_6B2_Oct_2017= ['IA_Schedule_D_6B2_20171001_20171231.csv',
'IA_Schedule_D_6B2_20180101_20180331.csv',
'IA_Schedule_D_6B2_20180401_20180630.csv',
'IA_Schedule_D_6B2_20180701_20180930.csv',
'IA_Schedule_D_6B2_20181001_20181231.csv',
'ERA_Schedule_D_6B2_20171001_20171231.csv',
'ERA_Schedule_D_6B2_20180101_20180331.csv',
'ERA_Schedule_D_6B2_20180401_20180630.csv',
'ERA_Schedule_D_6B2_20180701_20180930.csv',
'ERA_Schedule_D_6B2_20181001_20181231.csv',
'ERA_Schedule_D_6B2_20190101_20190331.csv']

csv_list_schedule_d_6B3 = ['IA_Schedule_D_6B3_20150316_20150930.csv',
'IA_Schedule_D_6B3_20151001_20151231.csv',
'IA_Schedule_D_6B3_20160101_20160331.csv',
'IA_Schedule_D_6B3_20160401_20160630.csv',
'IA_Schedule_D_6B3_20160701_20160930.csv',
'IA_Schedule_D_6B3_20161001_20161231.csv',
'IA_Schedule_D_6B3_20170101_20170331.csv',
'IA_Schedule_D_6B3_20170401_20170630.csv',
'IA_Schedule_D_6B3_20170701_20170930.csv',
'Schedule_D_6B3_20150315.csv',
'ERA_Schedule_D_6B3_20150316_20150930.csv',
'ERA_Schedule_D_6B3_20151001_20151231.csv',
'ERA_Schedule_D_6B3_20160101_20160331.csv',
'ERA_Schedule_D_6B3_20160401_20160630.csv',
'ERA_Schedule_D_6B3_20160701_20160930.csv',
'ERA_Schedule_D_6B3_20161001_20161231.csv',
'ERA_Schedule_D_6B3_20170101_20170331.csv',
'ERA_Schedule_D_6B3_20170401_20170630.csv',
'ERA_Schedule_D_6B3_20170701_20170930.csv',
'Schedule_D_6B3_20150315.csv']

csv_list_schedule_d_6B3_Oct_2017 = ['IA_Schedule_D_6B3_20171001_20171231.csv',
'IA_Schedule_D_6B3_20180101_20180331.csv',
'IA_Schedule_D_6B3_20180401_20180630.csv',
'IA_Schedule_D_6B3_20180701_20180930.csv',
'IA_Schedule_D_6B3_20181001_20181231.csv',
'ERA_Schedule_D_6B3_20171001_20171231.csv',
'ERA_Schedule_D_6B3_20180101_20180331.csv',
'ERA_Schedule_D_6B3_20180401_20180630.csv',
'ERA_Schedule_D_6B3_20180701_20180930.csv',
'ERA_Schedule_D_6B3_20181001_20181231.csv',
'ERA_Schedule_D_6B3_20190101_20190331.csv']

csv_list_schedule_d_7A =['IA_Schedule_D_7A_20150316_20150930.csv', 'IA_Schedule_D_7A_20170701_20170930.csv',
'IA_Schedule_D_7A_20151001_20151231.csv',
'IA_Schedule_D_7A_20160101_20160331.csv',
'IA_Schedule_D_7A_20160401_20160630.csv',
'IA_Schedule_D_7A_20160701_20160930.csv',
'IA_Schedule_D_7A_20161001_20161231.csv',
'IA_Schedule_D_7A_20170101_20170331.csv',
'Schedule_D_7A_20150315.csv',
'IA_Schedule_D_7A_20170401_20170630.csv',
'ERA_Schedule_D_7A_20150316_20150930.csv',
'ERA_Schedule_D_7A_20151001_20151231.csv',
'ERA_Schedule_D_7A_20160101_20160331.csv',
'ERA_Schedule_D_7A_20160401_20160630.csv',
'ERA_Schedule_D_7A_20160701_20160930.csv',
'ERA_Schedule_D_7A_20161001_20161231.csv',
'ERA_Schedule_D_7A_20170101_20170331.csv',
'ERA_Schedule_D_7A_20170401_20170630.csv',
'ERA_Schedule_D_7A_20170701_20170930.csv',
'Schedule_D_7A_20150315.csv']

csv_list_schedule_d_7A_Oct_2017 = ['IA_Schedule_D_7A_20171001_20171231.csv',
'IA_Schedule_D_7A_20180101_20180331.csv',
'IA_Schedule_D_7A_20180401_20180630.csv',
'IA_Schedule_D_7A_20180701_20180930.csv',
'IA_Schedule_D_7A_20181001_20181231.csv',
'Schedule_D_7A_20150315.csv',
'ERA_Schedule_D_7A_20171001_20171231.csv',
'ERA_Schedule_D_7A_20180101_20180331.csv',
'ERA_Schedule_D_7A_20180401_20180630.csv',
'ERA_Schedule_D_7A_20180701_20180930.csv',
'ERA_Schedule_D_7A_20181001_20181231.csv',
'ERA_Schedule_D_7A_20190101_20190331.csv']

csv_list_schedule_d_7A_CIK =['IA_Schedule_D_7A_CIK_20171001_20171231.csv',
'IA_Schedule_D_7A_CIK_20180101_20180331.csv',
'IA_Schedule_D_7A_CIK_20180401_20180630.csv',
'IA_Schedule_D_7A_CIK_20180701_20180930.csv',
'IA_Schedule_D_7A_CIK_20181001_20181231.csv', 'ERA_Schedule_D_7A_CIK_20171001_20171231.csv',
'ERA_Schedule_D_7A_CIK_20180101_20180331.csv',
'ERA_Schedule_D_7A_CIK_20180401_20180630.csv',
'ERA_Schedule_D_7A_CIK_20180701_20180930.csv',
'ERA_Schedule_D_7A_CIK_20181001_20181231.csv',
'ERA_Schedule_D_7A_CIK_20190101_20190331.csv']

csv_list_schedule_d_7A10B = ['IA_Schedule_D_7A10b_20150316_20150930.csv',
'IA_Schedule_D_7A10b_20151001_20151231.csv',
'IA_Schedule_D_7A10b_20160101_20160331.csv',
'IA_Schedule_D_7A10b_20160401_20160630.csv',
'IA_Schedule_D_7A10b_20160701_20160930.csv',
'IA_Schedule_D_7A10b_20161001_20161231.csv',
'IA_Schedule_D_7A10b_20170101_20170331.csv',
'IA_Schedule_D_7A10b_20170401_20170630.csv',
'IA_Schedule_D_7A10b_20170701_20170930.csv',
'Schedule_D_7A10B_20150315.csv', 'ERA_Schedule_D_7A10b_20150316_20150930.csv',
'ERA_Schedule_D_7A10b_20151001_20151231.csv',
'ERA_Schedule_D_7A10b_20160101_20160331.csv',
'ERA_Schedule_D_7A10b_20160401_20160630.csv',
'ERA_Schedule_D_7A10b_20160701_20160930.csv',
'ERA_Schedule_D_7A10b_20161001_20161231.csv',
'ERA_Schedule_D_7A10b_20170101_20170331.csv',
'ERA_Schedule_D_7A10b_20170401_20170630.csv',
'ERA_Schedule_D_7A10b_20170701_20170930.csv',
'Schedule_D_7A10b_20150315.csv']

csv_list_schedule_d_7A10B_Oct_2017 = ['IA_Schedule_D_7A10b_20171001_20171231.csv',
'IA_Schedule_D_7A10b_20180101_20180331.csv',
'IA_Schedule_D_7A10b_20180401_20180630.csv',
'IA_Schedule_D_7A10b_20180701_20180930.csv',
'IA_Schedule_D_7A10b_20181001_20181231.csv',
'ERA_Schedule_D_7A10b_20171001_20171231.csv',
'ERA_Schedule_D_7A10b_20180101_20180331.csv',
'ERA_Schedule_D_7A10b_20180401_20180630.csv',
'ERA_Schedule_D_7A10b_20180701_20180930.csv',
'ERA_Schedule_D_7A10b_20181001_20181231.csv',
'ERA_Schedule_D_7A10b_20190101_20190331.csv']

csv_list_schedule_d_7B1 = ['IA_Schedule_D_7B1_20150316_20150930.csv',
'IA_Schedule_D_7B1_20151001_20151231.csv',
'IA_Schedule_D_7B1_20160101_20160331.csv',
'IA_Schedule_D_7B1_20160401_20160630.csv',
'IA_Schedule_D_7B1_20160701_20160930.csv',
'IA_Schedule_D_7B1_20161001_20161231.csv',
'IA_Schedule_D_7B1_20170101_20170331.csv',
'IA_Schedule_D_7B1_20170401_20170630.csv',
'IA_Schedule_D_7B1_20170701_20170930.csv',
'Schedule_D_7B_20150315.csv',
'Schedule_D_7B1_20150315.csv', 'ERA_Schedule_D_7B1_20150316_20150930.csv',
'ERA_Schedule_D_7B1_20151001_20151231.csv',
'ERA_Schedule_D_7B1_20160101_20160331.csv',
'ERA_Schedule_D_7B1_20160401_20160630.csv',
'ERA_Schedule_D_7B1_20160701_20160930.csv',
'ERA_Schedule_D_7B1_20161001_20161231.csv',
'ERA_Schedule_D_7B1_20170101_20170331.csv',
'ERA_Schedule_D_7B1_20170401_20170630.csv',
'ERA_Schedule_D_7B1_20170701_20170930.csv',
'Schedule_D_7B1_20150315.csv']

csv_list_schedule_d_7B1_Oct_2017 = [
'IA_Schedule_D_7B1_20171001_20171231.csv',
'IA_Schedule_D_7B1_20180101_20180331.csv',
'IA_Schedule_D_7B1_20180401_20180630.csv',
'IA_Schedule_D_7B1_20180701_20180930.csv',
'IA_Schedule_D_7B1_20181001_20181231.csv',
'ERA_Schedule_D_7B1_20171001_20171231.csv',
'ERA_Schedule_D_7B1_20180101_20180331.csv',
'ERA_Schedule_D_7B1_20180401_20180630.csv',
'ERA_Schedule_D_7B1_20180701_20180930.csv',
'ERA_Schedule_D_7B1_20181001_20181231.csv',
'ERA_Schedule_D_7B1_20190101_20190331.csv',]

cvs_list_era_schedule_d_7B1Ab1 = ['ERA_Schedule_D_7B1A1b_20150316_20150930.csv',
'ERA_Schedule_D_7B1A1b_20151001_20151231.csv']

csv_list_schedule_d_7B1A3 = ['IA_Schedule_D_7B1A3_20150316_20150930.csv',
'IA_Schedule_D_7B1A3_20151001_20151231.csv',
'IA_Schedule_D_7B1A3_20160101_20160331.csv',
'IA_Schedule_D_7B1A3_20160401_20160630.csv',
'IA_Schedule_D_7B1A3_20160701_20160930.csv',
'IA_Schedule_D_7B1A3_20161001_20161231.csv',
'IA_Schedule_D_7B1A3_20170101_20170331.csv',
'IA_Schedule_D_7B1A3_20170401_20170630.csv',
'IA_Schedule_D_7B1A3_20170701_20170930.csv',
'Schedule_D_7B1A3_20150315.csv', 'ERA_Schedule_D_7B1A3_20150316_20150930.csv',
'ERA_Schedule_D_7B1A3_20151001_20151231.csv',
'ERA_Schedule_D_7B1A3_20160101_20160331.csv',
'ERA_Schedule_D_7B1A3_20160401_20160630.csv',
'ERA_Schedule_D_7B1A3_20160701_20160930.csv',
'ERA_Schedule_D_7B1A3_20161001_20161231.csv',
'ERA_Schedule_D_7B1A3_20170101_20170331.csv',
'ERA_Schedule_D_7B1A3_20170401_20170630.csv',
'ERA_Schedule_D_7B1A3_20170701_20170930.csv',
'Schedule_D_7B1A3_20150315.csv']

csv_list_schedule_d_7B1A3a = ['IA_Schedule_D_7B1A3a_20171001_20171231.csv',
'IA_Schedule_D_7B1A3a_20180101_20180331.csv',
'IA_Schedule_D_7B1A3a_20180401_20180630.csv',
'IA_Schedule_D_7B1A3a_20180701_20180930.csv',
'IA_Schedule_D_7B1A3a_20181001_20181231.csv',
'IA_Schedule_D_7B1A3b_20171001_20171231.csv',
'IA_Schedule_D_7B1A3b_20180101_20180331.csv',
'IA_Schedule_D_7B1A3b_20180401_20180630.csv',
'IA_Schedule_D_7B1A3b_20180701_20180930.csv',
'IA_Schedule_D_7B1A3b_20181001_20181231.csv',
'ERA_Schedule_D_7B1A3a_20171001_20171231.csv',
'ERA_Schedule_D_7B1A3a_20180101_20180331.csv',
'ERA_Schedule_D_7B1A3a_20180401_20180630.csv',
'ERA_Schedule_D_7B1A3a_20180701_20180930.csv',
'ERA_Schedule_D_7B1A3a_20181001_20181231.csv',
'ERA_Schedule_D_7B1A3a_20190101_20190331.csv']

csv_list_schedule_d_7B1A5 = ['IA_Schedule_D_7B1A5_20150316_20150930.csv',
'IA_Schedule_D_7B1A5_20151001_20151231.csv',
'IA_Schedule_D_7B1A5_20160101_20160331.csv',
'IA_Schedule_D_7B1A5_20160401_20160630.csv',
'IA_Schedule_D_7B1A5_20160701_20160930.csv',
'IA_Schedule_D_7B1A5_20161001_20161231.csv',
'IA_Schedule_D_7B1A5_20170101_20170331.csv',
'IA_Schedule_D_7B1A5_20170401_20170630.csv',
'IA_Schedule_D_7B1A5_20170701_20170930.csv',
'Schedule_D_7B1A5_20150315.csv',
'ERA_Schedule_D_7B1A5_20150316_20150930.csv',
'ERA_Schedule_D_7B1A5_20151001_20151231.csv',
'ERA_Schedule_D_7B1A5_20160101_20160331.csv',
'ERA_Schedule_D_7B1A5_20160401_20160630.csv',
'ERA_Schedule_D_7B1A5_20160701_20160930.csv',
'ERA_Schedule_D_7B1A5_20161001_20161231.csv',
'ERA_Schedule_D_7B1A5_20170101_20170331.csv',
'ERA_Schedule_D_7B1A5_20170401_20170630.csv',
'ERA_Schedule_D_7B1A5_20170701_20170930.csv',
'Schedule_D_7B1A5_20150315.csv']

csv_list_schedule_d_7B1A5_Oct_2017 = ['IA_Schedule_D_7B1A5_20171001_20171231.csv',
'IA_Schedule_D_7B1A5_20180101_20180331.csv',
'IA_Schedule_D_7B1A5_20180401_20180630.csv',
'IA_Schedule_D_7B1A5_20180701_20180930.csv',
'IA_Schedule_D_7B1A5_20181001_20181231.csv',
'ERA_Schedule_D_7B1A5_20171001_20171231.csv',
'ERA_Schedule_D_7B1A5_20180101_20180331.csv',
'ERA_Schedule_D_7B1A5_20180401_20180630.csv',
'ERA_Schedule_D_7B1A5_20180701_20180930.csv',
'ERA_Schedule_D_7B1A5_20181001_20181231.csv',
'ERA_Schedule_D_7B1A5_20190101_20190331.csv']

csv_list_schedule_d_7B1A6b = ['IA_Schedule_D_7B1A6b_20150316_20150930.csv',
'IA_Schedule_D_7B1A6b_20151001_20151231.csv',
'IA_Schedule_D_7B1A6b_20160101_20160331.csv',
'IA_Schedule_D_7B1A6b_20160401_20160630.csv',
'IA_Schedule_D_7B1A6b_20160701_20160930.csv',
'IA_Schedule_D_7B1A6b_20161001_20161231.csv',
'IA_Schedule_D_7B1A6b_20170101_20170331.csv',
'IA_Schedule_D_7B1A6b_20170401_20170630.csv',
'IA_Schedule_D_7B1A6b_20170701_20170930.csv',
'Schedule_D_7B1A6B_20150315.csv', 'ERA_Schedule_D_7B1A6b_20150316_20150930.csv',
'ERA_Schedule_D_7B1A6b_20151001_20151231.csv',
'ERA_Schedule_D_7B1A6b_20160101_20160331.csv',
'ERA_Schedule_D_7B1A6b_20160401_20160630.csv',
'ERA_Schedule_D_7B1A6b_20160701_20160930.csv',
'ERA_Schedule_D_7B1A6b_20161001_20161231.csv',
'ERA_Schedule_D_7B1A6b_20170101_20170331.csv',
'ERA_Schedule_D_7B1A6b_20170401_20170630.csv',
'ERA_Schedule_D_7B1A6b_20170701_20170930.csv',
'Schedule_D_7B1A6b_20150315.csv']

csv_list_schedule_d_7B1A6b_Oct_2017 = ['IA_Schedule_D_7B1A6b_20171001_20171231.csv',
'IA_Schedule_D_7B1A6b_20180101_20180331.csv',
'IA_Schedule_D_7B1A6b_20180401_20180630.csv',
'IA_Schedule_D_7B1A6b_20180701_20180930.csv',
'IA_Schedule_D_7B1A6b_20181001_20181231.csv',
'ERA_Schedule_D_7B1A6b_20171001_20171231.csv',
'ERA_Schedule_D_7B1A6b_20180101_20180331.csv',
'ERA_Schedule_D_7B1A6b_20180401_20180630.csv',
'ERA_Schedule_D_7B1A6b_20180701_20180930.csv',
'ERA_Schedule_D_7B1A6b_20181001_20181231.csv',
'ERA_Schedule_D_7B1A6b_20190101_20190331.csv']

csv_list_schedule_d_7B1A7 = ['IA_Schedule_D_7B1A7_20150316_20150930.csv',
'IA_Schedule_D_7B1A7_20151001_20151231.csv',
'IA_Schedule_D_7B1A7_20160101_20160331.csv',
'IA_Schedule_D_7B1A7_20160401_20160630.csv',
'IA_Schedule_D_7B1A7_20160701_20160930.csv',
'IA_Schedule_D_7B1A7_20161001_20161231.csv',
'IA_Schedule_D_7B1A7_20170101_20170331.csv',
'IA_Schedule_D_7B1A7_20170401_20170630.csv',
'IA_Schedule_D_7B1A7_20170701_20170930.csv',
'Schedule_D_7B1A7_20150315.csv', 'ERA_Schedule_D_7B1A7_20150316_20150930.csv',
'ERA_Schedule_D_7B1A7_20151001_20151231.csv',
'ERA_Schedule_D_7B1A7_20160101_20160331.csv',
'ERA_Schedule_D_7B1A7_20160401_20160630.csv',
'ERA_Schedule_D_7B1A7_20160701_20160930.csv',
'ERA_Schedule_D_7B1A7_20161001_20161231.csv',
'ERA_Schedule_D_7B1A7_20170101_20170331.csv',
'ERA_Schedule_D_7B1A7_20170401_20170630.csv',
'ERA_Schedule_D_7B1A7_20170701_20170930.csv',
'Schedule_D_7B1A7_20150315.csv']

csv_list_schedule_d_7B1A7_Oct_2017 = ['IA_Schedule_D_7B1A7_20171001_20171231.csv',
'IA_Schedule_D_7B1A7_20180101_20180331.csv',
'IA_Schedule_D_7B1A7_20180401_20180630.csv',
'IA_Schedule_D_7B1A7_20180701_20180930.csv',
'IA_Schedule_D_7B1A7_20181001_20181231.csv',
'ERA_Schedule_D_7B1A7_20171001_20171231.csv',
'ERA_Schedule_D_7B1A7_20180101_20180331.csv',
'ERA_Schedule_D_7B1A7_20180401_20180630.csv',
'ERA_Schedule_D_7B1A7_20180701_20180930.csv',
'ERA_Schedule_D_7B1A7_20181001_20181231.csv',
'ERA_Schedule_D_7B1A7_20190101_20190331.csv']


csv_list_schedule_d_7B1A7d = ['IA_Schedule_D_7B1A7d_20150316_20150930.csv',
'IA_Schedule_D_7B1A7d_20151001_20151231.csv',
'IA_Schedule_D_7B1A7d_20160101_20160331.csv',
'IA_Schedule_D_7B1A7d_20160401_20160630.csv',
'IA_Schedule_D_7B1A7d_20160701_20160930.csv',
'IA_Schedule_D_7B1A7d_20161001_20161231.csv',
'IA_Schedule_D_7B1A7d_20170101_20170331.csv',
'IA_Schedule_D_7B1A7d_20170401_20170630.csv',
'IA_Schedule_D_7B1A7d_20170701_20170930.csv',
'Schedule_D_7B1A7D_20150315.csv', 'ERA_Schedule_D_7B1A7d_20150316_20150930.csv',
'ERA_Schedule_D_7B1A7d_20151001_20151231.csv',
'ERA_Schedule_D_7B1A7d_20160101_20160331.csv',
'ERA_Schedule_D_7B1A7d_20160401_20160630.csv',
'ERA_Schedule_D_7B1A7d_20160701_20160930.csv',
'ERA_Schedule_D_7B1A7d_20161001_20161231.csv',
'ERA_Schedule_D_7B1A7d_20170101_20170331.csv',
'ERA_Schedule_D_7B1A7d_20170401_20170630.csv',
'ERA_Schedule_D_7B1A7d_20170701_20170930.csv']

csv_list_schedule_d_7B1A7d_Oct_2017 = ['IA_Schedule_D_7B1A7d_20161001_20161231.csv',
'IA_Schedule_D_7B1A7d_20170101_20170331.csv',
'IA_Schedule_D_7B1A7d_20170401_20170630.csv',
'IA_Schedule_D_7B1A7d_20170701_20170930.csv',
'ERA_Schedule_D_7B1A7d1_20171001_20171231.csv',
'ERA_Schedule_D_7B1A7d1_20180101_20180331.csv',
'ERA_Schedule_D_7B1A7d1_20180401_20180630.csv',
'ERA_Schedule_D_7B1A7d1_20180701_20180930.csv',
'ERA_Schedule_D_7B1A7d1_20181001_20181231.csv',
'ERA_Schedule_D_7B1A7d1_20190101_20190331.csv']

csv_list_schedule_d_7B1A7d1 = ['ERA_Schedule_D_7B1A7d_20150316_20150930.csv',
'ERA_Schedule_D_7B1A7d_20160101_20160331.csv',
'ERA_Schedule_D_7B1A7d_20160401_20160630.csv',
'ERA_Schedule_D_7B1A7d_20160701_20160930.csv',
'ERA_Schedule_D_7B1A7d_20161001_20161231.csv',
'ERA_Schedule_D_7B1A7d_20170101_20170331.csv',
'ERA_Schedule_D_7B1A7d_20170401_20170630.csv',
'ERA_Schedule_D_7B1A7d_20170701_20170930.csv',
'Schedule_D_7B1A7d_20150315.csv']

csv_list_schedule_d_7B1A7d1_Oct_2017 = ['IA_Schedule_D_7B1A7d1_20171001_20171231.csv',
'IA_Schedule_D_7B1A7d1_20180101_20180331.csv',
'IA_Schedule_D_7B1A7d1_20180401_20180630.csv',
'IA_Schedule_D_7B1A7d1_20180701_20180930.csv',
'IA_Schedule_D_7B1A7d1_20181001_20181231.csv',
'ERA_Schedule_D_7B1A7d1_20171001_20171231.csv',
'ERA_Schedule_D_7B1A7d1_20180101_20180331.csv',
'ERA_Schedule_D_7B1A7d1_20180401_20180630.csv',
'ERA_Schedule_D_7B1A7d1_20180701_20180930.csv',
'ERA_Schedule_D_7B1A7d1_20181001_20181231.csv',
'ERA_Schedule_D_7B1A7d1_20190101_20190331.csv']

csv_list_schedule_d_7B1A7d2 = ['IA_Schedule_D_7B1A7d2_20171001_20171231.csv',
'IA_Schedule_D_7B1A7d2_20180101_20180331.csv',
'IA_Schedule_D_7B1A7d2_20180401_20180630.csv',
'IA_Schedule_D_7B1A7d2_20180701_20180930.csv',
'IA_Schedule_D_7B1A7d2_20181001_20181231.csv']

csv_list_schedule_d_7B1A7f = ['IA_Schedule_D_7B1A7f_20150316_20150930.csv',
'IA_Schedule_D_7B1A7f_20151001_20151231.csv',
'IA_Schedule_D_7B1A7f_20160101_20160331.csv',
'IA_Schedule_D_7B1A7f_20160401_20160630.csv',
'IA_Schedule_D_7B1A7f_20160701_20160930.csv',
'IA_Schedule_D_7B1A7f_20161001_20161231.csv',
'IA_Schedule_D_7B1A7f_20170101_20170331.csv',
'IA_Schedule_D_7B1A7f_20170401_20170630.csv',
'IA_Schedule_D_7B1A7f_20170701_20170930.csv',
'Schedule_D_7B1A7F_20150315.csv', 'ERA_Schedule_D_7B1A7f_20150316_20150930.csv',
'ERA_Schedule_D_7B1A7f_20151001_20151231.csv',
'ERA_Schedule_D_7B1A7f_20160101_20160331.csv',
'ERA_Schedule_D_7B1A7f_20160401_20160630.csv',
'ERA_Schedule_D_7B1A7f_20160701_20160930.csv',
'ERA_Schedule_D_7B1A7f_20161001_20161231.csv',
'ERA_Schedule_D_7B1A7f_20170101_20170331.csv',
'ERA_Schedule_D_7B1A7f_20170401_20170630.csv',
'ERA_Schedule_D_7B1A7f_20170701_20170930.csv',
'Schedule_D_7B1A7f_20150315.csv']

csv_list_schedule_d_7B1A7f_Oct_2017 = ['IA_Schedule_D_7B1A7f_20171001_20171231.csv',
'IA_Schedule_D_7B1A7f_20180101_20180331.csv',
'IA_Schedule_D_7B1A7f_20180401_20180630.csv',
'IA_Schedule_D_7B1A7f_20180701_20180930.csv',
'IA_Schedule_D_7B1A7f_20181001_20181231.csv',

'ERA_Schedule_D_7B1A7f_20171001_20171231.csv',
'ERA_Schedule_D_7B1A7f_20180101_20180331.csv',
'ERA_Schedule_D_7B1A7f_20180401_20180630.csv',
'ERA_Schedule_D_7B1A7f_20180701_20180930.csv',
'ERA_Schedule_D_7B1A7f_20181001_20181231.csv',
'ERA_Schedule_D_7B1A7f_20190101_20190331.csv']

csv_list_schedule_d_7B1A17b = ['IA_Schedule_D_7B1A17b_20150316_20150930.csv',
'IA_Schedule_D_7B1A17b_20151001_20151231.csv',
'IA_Schedule_D_7B1A17b_20160101_20160331.csv',
'IA_Schedule_D_7B1A17b_20160401_20160630.csv',
'IA_Schedule_D_7B1A17b_20160701_20160930.csv',
'IA_Schedule_D_7B1A17b_20161001_20161231.csv',
'IA_Schedule_D_7B1A17b_20170101_20170331.csv',
'IA_Schedule_D_7B1A17b_20170401_20170630.csv',
'IA_Schedule_D_7B1A17b_20170701_20170930.csv',
'Schedule_D_7B1A17B_20150315.csv', 'ERA_Schedule_D_7B1A17b_20150316_20150930.csv',
'ERA_Schedule_D_7B1A17b_20151001_20151231.csv',
'ERA_Schedule_D_7B1A17b_20160101_20160331.csv',
'ERA_Schedule_D_7B1A17b_20160401_20160630.csv',
'ERA_Schedule_D_7B1A17b_20160701_20160930.csv',
'ERA_Schedule_D_7B1A17b_20161001_20161231.csv',
'ERA_Schedule_D_7B1A17b_20170101_20170331.csv',
'ERA_Schedule_D_7B1A17b_20170401_20170630.csv',
'ERA_Schedule_D_7B1A17b_20170701_20170930.csv',
'Schedule_D_7B1A17b_20150315.csv']

csv_list_schedule_d_7B1A17b_Oct_2017 = ['IA_Schedule_D_7B1A17b_20171001_20171231.csv',
'IA_Schedule_D_7B1A17b_20180101_20180331.csv',
'IA_Schedule_D_7B1A17b_20180401_20180630.csv',
'IA_Schedule_D_7B1A17b_20180701_20180930.csv',
'IA_Schedule_D_7B1A17b_20181001_20181231.csv',
'ERA_Schedule_D_7B1A17b_20171001_20171231.csv',
'ERA_Schedule_D_7B1A17b_20180101_20180331.csv',
'ERA_Schedule_D_7B1A17b_20180401_20180630.csv',
'ERA_Schedule_D_7B1A17b_20180701_20180930.csv',
'ERA_Schedule_D_7B1A17b_20181001_20181231.csv',
'ERA_Schedule_D_7B1A17b_20190101_20190331.csv']

csv_list_schedule_d_7B1A18b = ['IA_Schedule_D_7B1A18b_20150316_20150930.csv',
'IA_Schedule_D_7B1A18b_20151001_20151231.csv',
'IA_Schedule_D_7B1A18b_20160101_20160331.csv',
'IA_Schedule_D_7B1A18b_20160401_20160630.csv',
'IA_Schedule_D_7B1A18b_20160701_20160930.csv',
'IA_Schedule_D_7B1A18b_20161001_20161231.csv',
'IA_Schedule_D_7B1A18b_20170101_20170331.csv',
'IA_Schedule_D_7B1A18b_20170401_20170630.csv',
'IA_Schedule_D_7B1A18b_20170701_20170930.csv',
'Schedule_D_7B1A18B_20150315.csv', 'ERA_Schedule_D_7B1A18b_20160101_20160331.csv',
'ERA_Schedule_D_7B1A18b_20160401_20160630.csv',
'ERA_Schedule_D_7B1A18b_20160701_20160930.csv',
'ERA_Schedule_D_7B1A18b_20161001_20161231.csv',
'ERA_Schedule_D_7B1A18b_20170101_20170331.csv',
'ERA_Schedule_D_7B1A18b_20170401_20170630.csv',
'ERA_Schedule_D_7B1A18b_20170701_20170930.csv']

csv_list_schedule_d_7B1A18b_Oct_2017 = ['IA_Schedule_D_7B1A18b_20171001_20171231.csv',
'IA_Schedule_D_7B1A18b_20180101_20180331.csv',
'IA_Schedule_D_7B1A18b_20180401_20180630.csv',
'IA_Schedule_D_7B1A18b_20180701_20180930.csv',
'IA_Schedule_D_7B1A18b_20181001_20181231.csv',
'ERA_Schedule_D_7B1A18b_20171001_20171231.csv',
'ERA_Schedule_D_7B1A18b_20180101_20180331.csv',
'ERA_Schedule_D_7B1A18b_20180401_20180630.csv',
'ERA_Schedule_D_7B1A18b_20180701_20180930.csv',
'ERA_Schedule_D_7B1A18b_20181001_20181231.csv',
'ERA_Schedule_D_7B1A18b_20190101_20190331.csv']

csv_list_schedule_d_7B1A22 = ['IA_Schedule_D_7B1A22_20150316_20150930.csv',
'IA_Schedule_D_7B1A22_20151001_20151231.csv',
'IA_Schedule_D_7B1A22_20160101_20160331.csv',
'IA_Schedule_D_7B1A22_20160401_20160630.csv',
'IA_Schedule_D_7B1A22_20160701_20160930.csv',
'IA_Schedule_D_7B1A22_20161001_20161231.csv',
'IA_Schedule_D_7B1A22_20170101_20170331.csv',
'IA_Schedule_D_7B1A22_20170401_20170630.csv',
'IA_Schedule_D_7B1A22_20170701_20170930.csv',
'Schedule_D_7B1A22_20150315.csv', 'ERA_Schedule_D_7B1A22_20150316_20150930.csv',
'ERA_Schedule_D_7B1A22_20151001_20151231.csv',
'ERA_Schedule_D_7B1A22_20160101_20160331.csv',
'ERA_Schedule_D_7B1A22_20160401_20160630.csv',
'ERA_Schedule_D_7B1A22_20160701_20160930.csv',
'ERA_Schedule_D_7B1A22_20161001_20161231.csv',
'ERA_Schedule_D_7B1A22_20170101_20170331.csv',
'ERA_Schedule_D_7B1A22_20170401_20170630.csv',
'ERA_Schedule_D_7B1A22_20170701_20170930.csv',
'Schedule_D_7B1A22_20150315.csv']

csv_list_schedule_d_7B1A22_Oct_2017 = ['IA_Schedule_D_7B1A22_20171001_20171231.csv',
'IA_Schedule_D_7B1A22_20180101_20180331.csv',
'IA_Schedule_D_7B1A22_20180401_20180630.csv',
'IA_Schedule_D_7B1A22_20180701_20180930.csv',
'IA_Schedule_D_7B1A22_20181001_20181231.csv',
'ERA_Schedule_D_7B1A22_20171001_20171231.csv',
'ERA_Schedule_D_7B1A22_20180101_20180331.csv',
'ERA_Schedule_D_7B1A22_20180401_20180630.csv',
'ERA_Schedule_D_7B1A22_20180701_20180930.csv',
'ERA_Schedule_D_7B1A22_20181001_20181231.csv',
'ERA_Schedule_D_7B1A22_20190101_20190331.csv']

csv_list_schedule_d_7B1A23 = ['IA_Schedule_D_7B1A23_20150316_20150930.csv',
'IA_Schedule_D_7B1A23_20151001_20151231.csv',
'IA_Schedule_D_7B1A23_20160101_20160331.csv',
'IA_Schedule_D_7B1A23_20160401_20160630.csv',
'IA_Schedule_D_7B1A23_20160701_20160930.csv',
'IA_Schedule_D_7B1A23_20161001_20161231.csv',
'IA_Schedule_D_7B1A23_20170101_20170331.csv',
'IA_Schedule_D_7B1A23_20170401_20170630.csv',
'IA_Schedule_D_7B1A23_20170701_20170930.csv',
'Schedule_D_7B1A23_20150315.csv', 'ERA_Schedule_D_7B1A23_20160701_20160930.csv',
'ERA_Schedule_D_7B1A23_20161001_20161231.csv',
'ERA_Schedule_D_7B1A23_20170101_20170331.csv',
'ERA_Schedule_D_7B1A23_20170401_20170630.csv',
'ERA_Schedule_D_7B1A23_20170701_20170930.csv']

csv_list_schedule_d_7B1A23_Oct_2017 = ['IA_Schedule_D_7B1A23_20171001_20171231.csv',
'IA_Schedule_D_7B1A23_20180101_20180331.csv',
'IA_Schedule_D_7B1A23_20180401_20180630.csv',
'IA_Schedule_D_7B1A23_20180701_20180930.csv',
'IA_Schedule_D_7B1A23_20181001_20181231.csv',
'ERA_Schedule_D_7B1A23_20171001_20171231.csv',
'ERA_Schedule_D_7B1A23_20180101_20180331.csv',
'ERA_Schedule_D_7B1A23_20180401_20180630.csv',
'ERA_Schedule_D_7B1A23_20180701_20180930.csv',
'ERA_Schedule_D_7B1A23_20181001_20181231.csv',
'ERA_Schedule_D_7B1A23_20190101_20190331.csv']

csv_list_schedule_d_7B1A24 = ['IA_Schedule_D_7B1A24_20150316_20150930.csv',
'IA_Schedule_D_7B1A24_20151001_20151231.csv',
'IA_Schedule_D_7B1A24_20160101_20160331.csv',
'IA_Schedule_D_7B1A24_20160401_20160630.csv',
'IA_Schedule_D_7B1A24_20160701_20160930.csv',
'IA_Schedule_D_7B1A24_20161001_20161231.csv',
'IA_Schedule_D_7B1A24_20170101_20170331.csv',
'IA_Schedule_D_7B1A24_20170401_20170630.csv',
'IA_Schedule_D_7B1A24_20170701_20170930.csv',
'Schedule_D_7B1A24_20150315.csv', 'ERA_Schedule_D_7B1A24_20160701_20160930.csv',
'ERA_Schedule_D_7B1A24_20161001_20161231.csv',
'ERA_Schedule_D_7B1A24_20170101_20170331.csv',
'ERA_Schedule_D_7B1A24_20170401_20170630.csv',
'ERA_Schedule_D_7B1A24_20170701_20170930.csv']

csv_list_schedule_d_7B1A24_Oct_2017 = ['IA_Schedule_D_7B1A24_20171001_20171231.csv',
'IA_Schedule_D_7B1A24_20180101_20180331.csv',
'IA_Schedule_D_7B1A24_20180401_20180630.csv',
'IA_Schedule_D_7B1A24_20180701_20180930.csv',
'IA_Schedule_D_7B1A24_20181001_20181231.csv',
'ERA_Schedule_D_7B1A24_20171001_20171231.csv',
'ERA_Schedule_D_7B1A24_20180101_20180331.csv',
'ERA_Schedule_D_7B1A24_20180401_20180630.csv',
'ERA_Schedule_D_7B1A24_20180701_20180930.csv',
'ERA_Schedule_D_7B1A24_20181001_20181231.csv',
'ERA_Schedule_D_7B1A24_20190101_20190331.csv']

csv_list_schedule_d_7B1A25 = ['IA_Schedule_D_7B1A25_20150316_20150930.csv',
'IA_Schedule_D_7B1A25_20151001_20151231.csv',
'IA_Schedule_D_7B1A25_20160101_20160331.csv',
'IA_Schedule_D_7B1A25_20160401_20160630.csv',
'IA_Schedule_D_7B1A25_20160701_20160930.csv',
'IA_Schedule_D_7B1A25_20161001_20161231.csv',
'IA_Schedule_D_7B1A25_20170101_20170331.csv',
'IA_Schedule_D_7B1A25_20170401_20170630.csv',
'IA_Schedule_D_7B1A25_20170701_20170930.csv',
'Schedule_D_7B1A25_20150315.csv',
'ERA_Schedule_D_7B1A25_20160701_20160930.csv',
'ERA_Schedule_D_7B1A25_20161001_20161231.csv',
'ERA_Schedule_D_7B1A25_20170101_20170331.csv',
'ERA_Schedule_D_7B1A25_20170401_20170630.csv',
'ERA_Schedule_D_7B1A25_20170701_20170930.csv']

csv_list_schedule_d_7B1A25_Oct_2017 = ['IA_Schedule_D_7B1A25_20171001_20171231.csv',
'IA_Schedule_D_7B1A25_20180101_20180331.csv',
'IA_Schedule_D_7B1A25_20180401_20180630.csv',
'IA_Schedule_D_7B1A25_20180701_20180930.csv',
'IA_Schedule_D_7B1A25_20181001_20181231.csv',
'ERA_Schedule_D_7B1A25_20171001_20171231.csv',
'ERA_Schedule_D_7B1A25_20180101_20180331.csv',
'ERA_Schedule_D_7B1A25_20180401_20180630.csv',
'ERA_Schedule_D_7B1A25_20180701_20180930.csv',
'ERA_Schedule_D_7B1A25_20181001_20181231.csv',
'ERA_Schedule_D_7B1A25_20190101_20190331.csv']

csv_list_schedule_d_7B1A26 = ['IA_Schedule_D_7B1A26_20150316_20150930.csv',
'IA_Schedule_D_7B1A26_20151001_20151231.csv',
'IA_Schedule_D_7B1A26_20160101_20160331.csv',
'IA_Schedule_D_7B1A26_20160401_20160630.csv',
'IA_Schedule_D_7B1A26_20160701_20160930.csv',
'IA_Schedule_D_7B1A26_20161001_20161231.csv',
'IA_Schedule_D_7B1A26_20170101_20170331.csv',
'IA_Schedule_D_7B1A26_20170401_20170630.csv',
'IA_Schedule_D_7B1A26_20170701_20170930.csv',
'Schedule_D_7B1A26_20150315.csv',
'ERA_Schedule_D_7B1A26_20160701_20160930.csv',
'ERA_Schedule_D_7B1A26_20161001_20161231.csv',
'ERA_Schedule_D_7B1A26_20170101_20170331.csv',
'ERA_Schedule_D_7B1A26_20170401_20170630.csv',
'ERA_Schedule_D_7B1A26_20170701_20170930.csv']

csv_list_schedule_d_7B1A26_Oct_2017 = ['IA_Schedule_D_7B1A26_20171001_20171231.csv',
'IA_Schedule_D_7B1A26_20180101_20180331.csv',
'IA_Schedule_D_7B1A26_20180401_20180630.csv',
'IA_Schedule_D_7B1A26_20180701_20180930.csv',
'IA_Schedule_D_7B1A26_20181001_20181231.csv',
'ERA_Schedule_D_7B1A26_20171001_20171231.csv',
'ERA_Schedule_D_7B1A26_20180101_20180331.csv',
'ERA_Schedule_D_7B1A26_20180401_20180630.csv',
'ERA_Schedule_D_7B1A26_20180701_20180930.csv',
'ERA_Schedule_D_7B1A26_20181001_20181231.csv',
'ERA_Schedule_D_7B1A26_20190101_20190331.csv']

csv_list_schedule_d_7B1A28 = ['IA_Schedule_D_7B1A28_20150316_20150930.csv',
'IA_Schedule_D_7B1A28_20151001_20151231.csv',
'IA_Schedule_D_7B1A28_20160101_20160331.csv',
'IA_Schedule_D_7B1A28_20160401_20160630.csv',
'IA_Schedule_D_7B1A28_20160701_20160930.csv',
'IA_Schedule_D_7B1A28_20161001_20161231.csv',
'IA_Schedule_D_7B1A28_20170101_20170331.csv',
'IA_Schedule_D_7B1A28_20170401_20170630.csv',
'IA_Schedule_D_7B1A28_20170701_20170930.csv',
'Schedule_D_7B1A28_20150315.csv'
'ERA_Schedule_D_7B1A28_20160701_20160930.csv',
'ERA_Schedule_D_7B1A28_20161001_20161231.csv',
'ERA_Schedule_D_7B1A28_20170101_20170331.csv',
'ERA_Schedule_D_7B1A28_20170401_20170630.csv',
'ERA_Schedule_D_7B1A28_20170701_20170930.csv']

csv_list_schedule_d_7B1A28_Oct_2017 = ['IA_Schedule_D_7B1A28_20171001_20171231.csv',
'IA_Schedule_D_7B1A28_20180101_20180331.csv',
'IA_Schedule_D_7B1A28_20180401_20180630.csv',
'IA_Schedule_D_7B1A28_20180701_20180930.csv',
'IA_Schedule_D_7B1A28_20181001_20181231.csv',
'ERA_Schedule_D_7B1A28_20171001_20171231.csv',
'ERA_Schedule_D_7B1A28_20180101_20180331.csv',
'ERA_Schedule_D_7B1A28_20180401_20180630.csv',
'ERA_Schedule_D_7B1A28_20180701_20180930.csv',
'ERA_Schedule_D_7B1A28_20181001_20181231.csv',
'ERA_Schedule_D_7B1A28_20190101_20190331.csv']

csv_list_schedule_d_7B1A28_Websites = ['IA_Schedule_D_7B1A28_websites_20150316_20150930.csv',
'IA_Schedule_D_7B1A28_websites_20151001_20151231.csv',
'IA_Schedule_D_7B1A28_websites_20160101_20160331.csv',
'IA_Schedule_D_7B1A28_websites_20160401_20160630.csv',
'IA_Schedule_D_7B1A28_websites_20160701_20160930.csv',
'IA_Schedule_D_7B1A28_websites_20161001_20161231.csv',
'IA_Schedule_D_7B1A28_websites_20170101_20170331.csv',
'IA_Schedule_D_7B1A28_websites_20170401_20170630.csv',
'IA_Schedule_D_7B1A28_websites_20170701_20170930.csv',
'Schedule_D_7B1A28Websites_20150315.csv',
'ERA_Schedule_D_7B1A28_websites_20160701_20160930.csv',
'ERA_Schedule_D_7B1A28_websites_20161001_20161231.csv',
'ERA_Schedule_D_7B1A28_websites_20170101_20170331.csv',
'ERA_Schedule_D_7B1A28_websites_20170401_20170630.csv',
'ERA_Schedule_D_7B1A28_websites_20170701_20170930.csv']

csv_list_schedule_d_7B1A28_Websites_Oct_2017 = ['IA_Schedule_D_7B1A28_websites_20171001_20171231.csv',
'IA_Schedule_D_7B1A28_websites_20180101_20180331.csv',
'IA_Schedule_D_7B1A28_websites_20180401_20180630.csv',
'IA_Schedule_D_7B1A28_websites_20180701_20180930.csv',
'IA_Schedule_D_7B1A28_websites_20181001_20181231.csv',
'ERA_Schedule_D_7B1A28_websites_20171001_20171231.csv',
'ERA_Schedule_D_7B1A28_websites_20180101_20180331.csv',
'ERA_Schedule_D_7B1A28_websites_20180401_20180630.csv',
'ERA_Schedule_D_7B1A28_websites_20180701_20180930.csv',
'ERA_Schedule_D_7B1A28_websites_20181001_20181231.csv',
'ERA_Schedule_D_7B1A28_websites_20190101_20190331.csv']

csv_list_schedule_d_7B2 = ['IA_Schedule_D_7B2_20150316_20150930.csv',
'IA_Schedule_D_7B2_20151001_20151231.csv',
'IA_Schedule_D_7B2_20160101_20160331.csv',
'IA_Schedule_D_7B2_20160401_20160630.csv',
'IA_Schedule_D_7B2_20160701_20160930.csv',
'IA_Schedule_D_7B2_20161001_20161231.csv',
'IA_Schedule_D_7B2_20170101_20170331.csv',
'IA_Schedule_D_7B2_20170401_20170630.csv',
'IA_Schedule_D_7B2_20170701_20170930.csv',
'Schedule_D_7B2_20150315.csv', 'ERA_Schedule_D_7B2_20150316_20150930.csv',
'ERA_Schedule_D_7B2_20151001_20151231.csv',
'ERA_Schedule_D_7B2_20160101_20160331.csv',
'ERA_Schedule_D_7B2_20160401_20160630.csv',
'ERA_Schedule_D_7B2_20160701_20160930.csv',
'ERA_Schedule_D_7B2_20161001_20161231.csv',
'ERA_Schedule_D_7B2_20170101_20170331.csv',
'ERA_Schedule_D_7B2_20170401_20170630.csv',
'ERA_Schedule_D_7B2_20170701_20170930.csv',
'Schedule_D_7B2_20150315.csv']

csv_list_schedule_d_7B2_Oct_2017 = ['IA_Schedule_D_7B2_20171001_20171231.csv',
'IA_Schedule_D_7B2_20180101_20180331.csv',
'IA_Schedule_D_7B2_20180401_20180630.csv',
'IA_Schedule_D_7B2_20180701_20180930.csv',
'IA_Schedule_D_7B2_20181001_20181231.csv',
'ERA_Schedule_D_7B2_20171001_20171231.csv',
'ERA_Schedule_D_7B2_20180101_20180331.csv',
'ERA_Schedule_D_7B2_20180401_20180630.csv',
'ERA_Schedule_D_7B2_20180701_20180930.csv',
'ERA_Schedule_D_7B2_20181001_20181231.csv',
'ERA_Schedule_D_7B2_20190101_20190331.csv']

csv_list_schedule_d_10A = ['IA_Schedule_D_10A_20150316_20150930.csv',
'IA_Schedule_D_10A_20151001_20151231.csv',
'IA_Schedule_D_10A_20160101_20160331.csv',
'IA_Schedule_D_10A_20160401_20160630.csv',
'IA_Schedule_D_10A_20160701_20160930.csv',
'IA_Schedule_D_10A_20161001_20161231.csv',
'IA_Schedule_D_10A_20170101_20170331.csv',
'IA_Schedule_D_10A_20170401_20170630.csv',
'IA_Schedule_D_10A_20170701_20170930.csv',
'Schedule_D_10A_20150315.csv',
'ERA_Schedule_D_10A_20150316_20150930.csv',
'ERA_Schedule_D_10A_20151001_20151231.csv',
'ERA_Schedule_D_10A_20160101_20160331.csv',
'ERA_Schedule_D_10A_20160401_20160630.csv',
'ERA_Schedule_D_10A_20160701_20160930.csv',
'ERA_Schedule_D_10A_20161001_20161231.csv',
'ERA_Schedule_D_10A_20170101_20170331.csv',
'ERA_Schedule_D_10A_20170401_20170630.csv',
'ERA_Schedule_D_10A_20170701_20170930.csv',
'Schedule_D_10A_20150315.csv']

csv_list_schedule_d_10A_Oct_2017 = ['IA_Schedule_D_10A_20171001_20171231.csv',
'IA_Schedule_D_10A_20180101_20180331.csv',
'IA_Schedule_D_10A_20180401_20180630.csv',
'IA_Schedule_D_10A_20180701_20180930.csv',
'IA_Schedule_D_10A_20181001_20181231.csv',
'ERA_Schedule_D_10A_20171001_20171231.csv',
'ERA_Schedule_D_10A_20180101_20180331.csv',
'ERA_Schedule_D_10A_20180401_20180630.csv',
'ERA_Schedule_D_10A_20180701_20180930.csv',
'ERA_Schedule_D_10A_20181001_20181231.csv',
'ERA_Schedule_D_10A_20190101_20190331.csv']

csv_list_schedule_d_10B = ['IA_Schedule_D_10B_20150316_20150930.csv',
'IA_Schedule_D_10B_20151001_20151231.csv',
'IA_Schedule_D_10B_20160101_20160331.csv',
'IA_Schedule_D_10B_20160401_20160630.csv',
'IA_Schedule_D_10B_20160701_20160930.csv',
'IA_Schedule_D_10B_20161001_20161231.csv',
'IA_Schedule_D_10B_20170101_20170331.csv',
'IA_Schedule_D_10B_20170401_20170630.csv',
'IA_Schedule_D_10B_20170701_20170930.csv',
'Schedule_D_10B_20150315.csv']

csv_list_schedule_d_10B_Oct_2017 = ['IA_Schedule_D_10B_20171001_20171231.csv',
'IA_Schedule_D_10B_20180101_20180331.csv',
'IA_Schedule_D_10B_20180401_20180630.csv',
'IA_Schedule_D_10B_20180701_20180930.csv',
'IA_Schedule_D_10B_20181001_20181231.csv',]

csv_list_schedule_d_Books_and_Records = ['IA_Schedule_D_Books_and_Records_20150315_20150316_20150930.csv',
'IA_Schedule_D_Books_and_Records_20150315_20151001_20151231.csv',
'IA_Schedule_D_Books_and_Records_20160101_20160331.csv',
'IA_Schedule_D_Books_and_Records_20160401_20160630.csv',
'IA_Schedule_D_Books_and_Records_20160701_20160930.csv',
'IA_Schedule_D_Books_and_Records_20161001_20161231.csv',
'IA_Schedule_D_Books_and_Records_20170101_20170331.csv',
'IA_Schedule_D_Books_and_Records_20170401_20170630.csv',
'IA_Schedule_D_Books_and_Records_20170701_20170930.csv',
'Schedule_D_Books_and_Records_20150315.csv', 'ERA_Schedule_D_Books_and_Records_20150316_20150930.csv',
'ERA_Schedule_D_Books_and_Records_20151001_20151231.csv',
'ERA_Schedule_D_Books_and_Records_20160101_20160331.csv',
'ERA_Schedule_D_Books_and_Records_20160401_20160630.csv',
'ERA_Schedule_D_Books_and_Records_20160701_20160930.csv',
'ERA_Schedule_D_Books_and_Records_20161001_20161231.csv',
'ERA_Schedule_D_Books_and_Records_20170101_20170331.csv',
'ERA_Schedule_D_Books_and_Records_20170401_20170630.csv',
'ERA_Schedule_D_Books_and_Records_20170701_20170930.csv',
'Schedule_D_1L_20150315.csv']

csv_list_schedule_d_Books_and_Records_Oct_2017 = ['IA_Schedule_D_Books_and_Records_20171001_20171231.csv',
'IA_Schedule_D_Books_and_Records_20180101_20180331.csv',
'IA_Schedule_D_Books_and_Records_20180401_20180630.csv',
'IA_Schedule_D_Books_and_Records_20180701_20180930.csv',
'IA_Schedule_D_Books_and_Records_20181001_20181231.csv',
'ERA_Schedule_D_Books_and_Records_20171001_20171231.csv',
'ERA_Schedule_D_Books_and_Records_20180101_20180331.csv',
'ERA_Schedule_D_Books_and_Records_20180401_20180630.csv',
'ERA_Schedule_D_Books_and_Records_20180701_20180930.csv',
'ERA_Schedule_D_Books_and_Records_20181001_20181231.csv',
'ERA_Schedule_D_Books_and_Records_20190101_20190331.csv']

csv_list_schedule_d_Foreign_Regulatory_Authority = ['IA_Schedule_D_Foreign_Regulatory_Authority_20150316_20150930.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20160701_20160930.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20161001_20161231.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20170101_20170331.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20170401_20170630.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20170701_20170930.csv',
'Schedule_D_Foreign_Regulatory_Authority_20150315.csv',
'Schedule_D_Foreign_Regulatory_Authority_20151001_20151231.csv',
'Schedule_D_Foreign_Regulatory_Authority_20160101_20160331.csv',
'Schedule_D_Foreign_Regulatory_Authority_20160401_20160630.csv']

csv_list_schedule_d_Foreign_Regulatory_Authority_Oct_2017 = ['IA_Schedule_D_Foreign_Regulatory_Authority_20171001_20171231.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20180101_20180331.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20180401_20180630.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20180701_20180930.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20181001_20181231.csv',]

csv_list_schedule_d_Miscellaneous = ['IA_Schedule_D_Miscellaneous_20150316_20150930.csv',
'IA_Schedule_D_Miscellaneous_20160101_20160331.csv',
'IA_Schedule_D_Miscellaneous_20160401_20160630.csv',
'IA_Schedule_D_Miscellaneous_20160701_20160930.csv',
'IA_Schedule_D_Miscellaneous_20161001_20161231.csv',
'IA_Schedule_D_Miscellaneous_20170101_20170331.csv',
'IA_Schedule_D_Miscellaneous_20170401_20170630.csv',
'IA_Schedule_D_Miscellaneous_20170701_20170930.csv',
'Schedule_D_Misc_A_20150315.csv',
'Schedule_D_Misc_B_20150315.csv',
'Schedule_D_Misc_C_20150315.csv',
'Schedule_D_Misc_D_20150315.csv']

csv_list_schedule_d_Miscellaneous_Oct_2017 = ['IA_Schedule_D_Miscellaneous_20171001_20171231.csv',
'IA_Schedule_D_Miscellaneous_20180101_20180331.csv',
'IA_Schedule_D_Miscellaneous_20180401_20180630.csv',
'IA_Schedule_D_Miscellaneous_20180701_20180930.csv',
'IA_Schedule_D_Miscellaneous_20181001_20181231.csv',]

csv_list_DRP_Advisory_Affiliates = ['IA_DRP_Advisory_Affiliates_20150316_20150930.csv',
'IA_DRP_Advisory_Affiliates_20151001_20151231.csv',
'IA_DRP_Advisory_Affiliates_20160101_20160331.csv',
'IA_DRP_Advisory_Affiliates_20160401_20160630.csv',
'IA_DRP_Advisory_Affiliates_20160701_20160930.csv',
'IA_DRP_Advisory_Affiliates_20161001_20161231.csv',
'IA_DRP_Advisory_Affiliates_20170101_20170331.csv',
'IA_DRP_Advisory_Affiliates_20170401_20170630.csv',
'IA_DRP_Advisory_Affiliates_20170701_20170930.csv',
'DRP_Advisory_Affiliates_20150315.csv',
'ERA_DRP_Advisory_Affiliates_20150316_20150930.csv',
'ERA_DRP_Advisory_Affiliates_20151001_20151231.csv',
'ERA_DRP_Advisory_Affiliates_20160101_20160331.csv',
'ERA_DRP_Advisory_Affiliates_20160401_20160630.csv',
'ERA_DRP_Advisory_Affiliates_20160701_20160930.csv',
'ERA_DRP_Advisory_Affiliates_20161001_20161231.csv',
'ERA_DRP_Advisory_Affiliates_20170101_20170331.csv',
'ERA_DRP_Advisory_Affiliates_20170401_20170630.csv',
'ERA_DRP_Advisory_Affiliates_20170701_20170930.csv']

csv_list_DRP_Advisory_Affiliates_Oct_2017 = ['IA_DRP_Advisory_Affiliates_20171001_20171231.csv',
'IA_DRP_Advisory_Affiliates_20180101_20180331.csv',
'IA_DRP_Advisory_Affiliates_20180401_20180630.csv',
'IA_DRP_Advisory_Affiliates_20180701_20180930.csv',
'IA_DRP_Advisory_Affiliates_20181001_20181231.csv',
'ERA_DRP_Advisory_Affiliates_20171001_20171231.csv',
'ERA_DRP_Advisory_Affiliates_20180101_20180331.csv',
'ERA_DRP_Advisory_Affiliates_20180401_20180630.csv',
'ERA_DRP_Advisory_Affiliates_20180701_20180930.csv',
'ERA_DRP_Advisory_Affiliates_20181001_20181231.csv',
'ERA_DRP_Advisory_Affiliates_20190101_20190331.csv']

csv_list_Schedule_R_Other_Business_Names = ['IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20180105.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20180405.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20180705.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20181004.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20190130.csv']

csv_list_Schedule_R_4A_4B = ['IA_SCH_R_4A_4B_20180105.csv', 'IA_SCH_R_4A_4B_20181004.csv',
'IA_SCH_R_4A_4B_20180405.csv', 'IA_SCH_R_4A_4B_20190130.csv',
'IA_SCH_R_4A_4B_20180705.csv']

csv_list_Schedule_R_4C = ['IA_Schedule_R_4C_20171001_20171231.csv', 'IA_Schedule_R_4C_20180701_20180930.csv',
'IA_Schedule_R_4C_20180101_20180331.csv', 'IA_Schedule_R_4C_20181001_20181231.csv',
'IA_Schedule_R_4C_20180401_20180630.csv']

csv_list_Schedule_R_4D = ['IA_SCH_R_4D_20171001_20171231.csv', 'IA_SCH_R_4D_20180701_20180930.csv',
'IA_SCH_R_4D_20180101_20180331.csv', 'IA_SCH_R_4D_20181001_20181231.csv',
'IA_SCH_R_4D_20180401_20180630.csv']