# -*- coding: utf-8 -*-
"""
Processing Raw SEC Form ADV data and store processed csv foir ingetsion into Senzing
6/27/19
Kevin Powell
"""
import pandas as pd
import numpy as np
#Base A

#csv_list =  ['ADV_Base_A_20150315.csv', 'IA_ADV_Base_A_20150316_20150930.csv', 'IA_ADV_Base_A_20151001_20151231.csv',
#'IA_ADV_Base_A_20160101_20160331.csv','IA_ADV_Base_A_20160401_20160630.csv', 'IA_ADV_Base_A_20160701_20160930.csv',
#'IA_ADV_Base_A_20161001_20161231.csv', 'IA_ADV_Base_A_20170101_20170331.csv','IA_ADV_Base_A_20170401_20170630.csv',
#'IA_ADV_Base_A_20170701_20170930.csv','IA_ADV_Base_A_20171001_20171231.csv', 'IA_ADV_Base_A_20180101_20180331.csv',
#'IA_ADV_Base_A_20180401_20180630.csv', 'IA_ADV_Base_A_20180701_20180930.csv', 'IA_ADV_Base_A_20181001_20181231.csv']

#colums to keep
#colName =  [...]

#new column names 
#new_colName = [...]

#readfile1
#df1 = read_csv(csv_list[0])

#rename
#df1.rename(['A' :'a', ... ])

#add row
#df1.index = df1.index + 1
#df1[0]= ['GES'...]
#df1.sort_index()

#Make all Upper case

#Other cleaning tasks

#next file in list
#dfNext = pd.read_csv(csv_list[1])

#.....
#Base B
os.chdir('C:/Users/LeahFawzi/Documents/finra/finfrauddata/Data/FINRA data/RIA/base b')

csv_list_base_b = ['ADV_Base_B_20150315.csv', 'IA_ADV_Base_B_20170401_20170630.csv',
'IA_ADV_Base_B_20150316_20150930.csv', 'IA_ADV_Base_B_20170701_20170930.csv',
'IA_ADV_Base_B_20151001_20151231.csv', 'IA_ADV_Base_B_20171001_20171231.csv',
'IA_ADV_Base_B_20160101_20160331.csv', 'IA_ADV_Base_B_20180101_20180331.csv',
'IA_ADV_Base_B_20160401_20160630.csv', 'IA_ADV_Base_B_20180401_20180630.csv',
'IA_ADV_Base_B_20160701_20160930.csv', 'IA_ADV_Base_B_20180701_20180930.csv',
'IA_ADV_Base_B_20161001_20161231.csv', 'IA_ADV_Base_B_20181001_20181231.csv',
'IA_ADV_Base_B_20170101_20170331.csv']

#collect files
df_temp = pd.read_csv('ADV_Base_B_20150315.csv', encoding='ISO-8859-1')
df_base_b = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_base_b:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_base_b),(len(df_base_b) + len(df_file)))
    df_base_b = pd.concat([df_base_b, df_file])
    print(file, ":\n")
    for col in df_base_b.columns:
        print(col)
                
#select columns
df_keep_bb = df_base_b[['FilingID','2A1','2A2','2A3','2A4','2A5','2A6','2A7','2A8','2A9','2A10','2A11','2A12','2A13']]

Registration_Reasons = np.empty(len(df_keep_bb), dtype = 'object')

for x in  range(len(df_keep_bb.loc[:,"2A1"])):
    str_list =''
    if df_keep_bb.loc[x,"2A1"] =="Y":
        str_list = str_list + 'A'
    if df_keep_bb.loc[x,"2A2"] =="Y":
        str_list = str_list + 'B'
    if df_keep_bb.loc[x,"2A3"] =="Y":
        str_list = str_list + 'C'
    if df_keep_bb.loc[x,"2A4"] =="Y":
        str_list = str_list + 'D'
    if df_keep_bb.loc[x,"2A5"] =="Y":
        str_list = str_list + 'E'
    if df_keep_bb.loc[x,"2A6"] =="Y":
        str_list= str_list + 'F'
    if df_keep_bb.loc[x,"2A7"] =="Y":
        str_list = str_list + 'G'
    if df_keep_bb.loc[x,"2A8"] =="Y":
        str_list = str_list + 'H'
    if df_keep_bb.loc[x,"2A9"] =="Y":
        str_list = str_list + 'I'
    if df_keep_bb.loc[x,"2A10"] =="Y":
        str_list = str_list + 'J'
    if df_keep_bb.loc[x,"2A11"] =="Y":
        str_list = str_list + 'K'
    if df_keep_bb.loc[x,"2A12"] =="Y":
        str_list = str_list + 'L'
    if df_keep_bb.loc[x,"2A13"] =="Y":
        str_list = str_list + 'M'
        
    Registration_Reasons[x] = str_list
        
df_keep_bb.insert(14, "REGREASONS", Registration_Reasons, True )

df_keep_bb = df_keep_bb[["FilingID", "REGREASONS"]]
df_keep_bb.columns = ["FILINGID_OTHER_ID_NUMBER", "REGREASONS"]

#write file
df_keep_bb.to_csv('R_BASE_B.csv')


#Schedule AB
csv_list_schedule_ab = ['IA_Schedule_A_B_20150316_20150930.csv', 'IA_Schedule_A_B_20170701_20170930.csv',
'IA_Schedule_A_B_20151001_20151231.csv', 'IA_Schedule_A_B_20171001_20171231.csv',
'IA_Schedule_A_B_20160101_20160331.csv', 'IA_Schedule_A_B_20180101_20180331.csv',
'IA_Schedule_A_B_20160401_20160630.csv', 'IA_Schedule_A_B_20180401_20180630.csv',
'IA_Schedule_A_B_20160701_20160930.csv', 'IA_Schedule_A_B_20180701_20180930.csv',
'IA_Schedule_A_B_20161001_20161231.csv', 'IA_Schedule_A_B_20181001_20181231.csv',
'IA_Schedule_A_B_20170101_20170331.csv', 'Schedule_A_B_20150315.csv',
'IA_Schedule_A_B_20170401_20170630.csv']

#collect files
df_temp = pd.read_csv('IA_Schedule_A_B_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_ab = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_ab:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_ab),(len(df_schedule_ab) + len(df_file)))
    df_schedule_ab = pd.concat([df_schedule_ab, df_file])
    print(file, ":\n")
    for col in df_schedule_ab.columns:
        print(col)
        
        
#selct columns
df_schedule_ab.head()
df_keep_ab = df_schedule_ab[['FilingID','Full Legal Name', 'DE/FE/I','Entity in Which','Title or Status','Status Acquired','Ownership Code','Control Person','PR','OwnerID']]

#add GES names
#row_ges = pd.DataFrame([['ACCT_NUM', 'FULL_NAME', 'DOM_FOR_ENT_INDV', 'WHERE_STATUS_HELD', 'TITLE_STATUS', 'WHEN_AQUIRED', 'OWNER_CODE', 'OWNER_CONTROL', 'PUB_REPORT_COMP', 'OWNER_ID']], columns = df_keep_ab.columns)
#df_keep_ab = pd.concat([row_ges, df_keep_ab]).reset_index(drop=True)


#write file
df_keep_ab.to_csv('soname.csv')

#Schedule D 1B
csv_list_schedule_d_1b = ['IA_Schedule_D_1B_20150316_20150930.csv',  'IA_Schedule_D_1B_20170701_20170930.csv',
'IA_Schedule_D_1B_20151001_20151231.csv', 'IA_Schedule_D_1B_20171001_20171231.csv',
'IA_Schedule_D_1B_20160101_20160331.csv', 'IA_Schedule_D_1B_20180101_20180331.csv',
'IA_Schedule_D_1B_20160401_20160630.csv', 'IA_Schedule_D_1B_20180401_20180630.csv',
'IA_Schedule_D_1B_20160701_20160930.csv', 'IA_Schedule_D_1B_20180701_20180930.csv',
'IA_Schedule_D_1B_20161001_20161231.csv', 'IA_Schedule_D_1B_20181001_20181231.csv',
'IA_Schedule_D_1B_20170101_20170331.csv', 'Schedule_D_1B_20150315.csv',
'IA_Schedule_D_1B_20170401_20170630.csv']

#collect files
df_temp = pd.read_csv('IA_Schedule_D_1B_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1b = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_d_1b:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1b),(len(df_schedule_d_1b) + len(df_file)))
    df_schedule_d_1b = pd.concat([df_schedule_d_1b, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1b.columns:
        print(col)
        
        
#selct columns
df_schedule_d_1b.head()
df_keep_d_1b = df_schedule_d_1b[['FilingID','Other Business Name', 'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'OTHER']]

#add GES names
#row_ges = pd.DataFrame([['ACCT_NUM', 'NAME_ORG', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION_STATE', 'LOCATION']], columns = df_keep_d_1b.columns)
#df_keep_d_1b = pd.concat([row_ges, df_keep_d_1b]).reset_index(drop=True)


#write file
df_keep_d_1b.to_csv('soname.csv')

#Schedule D 1F
csv_list_schedule_d_1f = ['IA_Schedule_D_1F_20150316_20150930.csv', 'IA_Schedule_D_1F_20170701_20170930.csv',
'IA_Schedule_D_1F_20151001_20151231.csv', 'IA_Schedule_D_1F_20171001_20171231.csv',
'IA_Schedule_D_1F_20160101_20160331.csv', 'IA_Schedule_D_1F_20180101_20180331.csv',
'IA_Schedule_D_1F_20160401_20160630.csv', 'IA_Schedule_D_1F_20180401_20180630.csv',
'IA_Schedule_D_1F_20160701_20160930.csv', 'IA_Schedule_D_1F_20180701_20180930.csv',
'IA_Schedule_D_1F_20161001_20161231.csv', 'IA_Schedule_D_1F_20181001_20181231.csv',
'IA_Schedule_D_1F_20170101_20170331.csv', 'Schedule_D_1F_20150315.csv',
'IA_Schedule_D_1F_20170401_20170630.csv',]

df_temp = pd.read_csv('IA_Schedule_D_1F_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1f = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_d_1f:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1f),(len(df_schedule_d_1f) + len(df_file)))
    df_schedule_d_1f = pd.concat([df_schedule_d_1f, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1f.columns:
        print(col)
        
        
#selct columns
df_schedule_d_1f.head()
df_keep_d_1f = df_schedule_d_1f[['FilingID', 'Street 1', 'Street 2', 'City', 'State', 'Country', 'Postal Code', 'Private Residence', 'Telephone Number', 'Facsimile Number', 'Branch Number', 'Employees', 'BD', 'Bank', 'Insurance', 'Commodity', 'Municipal', 'Accounting', 'Law', 'Other']]

#add GES names
#row_ges = pd.DataFrame([['ACCT_NUM', 'LOCATION_ADDRESS', 'LOCATION_ADDRESS', 'LOCATION_CITY', 'LOCATION_STATE', 'LOCATION_COUNTRY', 'LOCATION_POSTAL_CODE', 'PRIVATE_RESIDENCE', 'NUMBER_PHONE', 'NUMBER_FAX', 'NUMBER_BRANCH', 'NUMBER_EMPLOYEES', 'BUSINESS_TYPE', 'BUSINESS_TYPE', 'BUSINESS_TYPE', 'BUSINESS_TYPE', 'BUSINESS_TYPE', 'BUSINESS_TYPE', 'BUSINESS_TYPE', 'BUSINESS_TYPE']], columns = df_keep_d_1f.columns)
#df_keep_d_1f = pd.concat([row_ges, df_keep_d_1f]).reset_index(drop=True)


#write file
df_keep_d_1f.to_csv('soname.csv')

#Schedule D 1I
csv_list_schedule_d_1I = ['IA_Schedule_D_1I_20150316_20150930.csv', 'IA_Schedule_D_1I_20170701_20170930.csv',
'IA_Schedule_D_1I_20151001_20151231.csv', 'IA_Schedule_D_1I_20171001_20171231.csv',
'IA_Schedule_D_1I_20160101_20160331.csv', 'IA_Schedule_D_1I_20180101_20180331.csv',
'IA_Schedule_D_1I_20160401_20160630.csv', 'IA_Schedule_D_1I_20180401_20180630.csv',
'IA_Schedule_D_1I_20160701_20160930.csv', 'IA_Schedule_D_1I_20180701_20180930.csv',
'IA_Schedule_D_1I_20161001_20161231.csv', 'IA_Schedule_D_1I_20181001_20181231.csv',
'IA_Schedule_D_1I_20170101_20170331.csv', 'Schedule_D_1I_20150315.csv',
'IA_Schedule_D_1I_20170401_20170630.csv']

df_temp = pd.read_csv('IA_Schedule_D_1I_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_1i = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_d_1I:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_1i),(len(df_schedule_d_1i) + len(df_file)))
    df_schedule_d_1i = pd.concat([df_schedule_d_1i, df_file])
    print(file, ":\n")
    for col in df_schedule_d_1i.columns:
        print(col)
        
        
#selct columns
df_schedule_d_1i.head()
df_keep_d_1i = df_schedule_d_1i[['FilingID', 'Website']]

#add GES names
#row_ges = pd.DataFrame([['ACCT_NUM','WEBSITE']], columns = df_keep_d_1i.columns)
#df_keep_d_1i = pd.concat([row_ges, df_keep_d_1i]).reset_index(drop=True)


#write file
df_keep_d_1i.to_csv('soname.csv')

#Schedule D 2A
csv_list_schedule_d_2A = ['IA_Schedule_D_2A_Related_20150316_20150930.csv',
'IA_Schedule_D_2A_Related_20151001_20151231.csv',
'IA_Schedule_D_2A_Related_20160101_20160331.csv',
'IA_Schedule_D_2A_Related_20160401_20160630.csv',
'IA_Schedule_D_2A_Related_20160701_20160930.csv',
'IA_Schedule_D_2A_Related_20161001_20161231.csv',
'IA_Schedule_D_2A_Related_20170101_20170331.csv',
'IA_Schedule_D_2A_Related_20170401_20170630.csv',
'IA_Schedule_D_2A_Related_20170701_20170930.csv',
'IA_Schedule_D_2A_Related_20171001_20171231.csv',
'IA_Schedule_D_2A_Related_20180101_20180331.csv',
'IA_Schedule_D_2A_Related_20180401_20180630.csv',
'IA_Schedule_D_2A_Related_20180701_20180930.csv',
'IA_Schedule_D_2A_Related_20181001_20181231.csv',
'Schedule_D_2A_Exemptive_Order_20150315.csv',
'Schedule_D_2A_Multistate_20150315.csv',
'Schedule_D_2A_Newly_Formed_20150315.csv',
'Schedule_D_2A_Related_20150315.csv']

df_temp = pd.read_csv('IA_Schedule_D_2A_Related_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_2a = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_d_2A:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_2a),(len(df_schedule_d_2a) + len(df_file)))
    df_schedule_d_2a = pd.concat([df_schedule_d_2a, df_file])
    print(file, ":\n")
    for col in df_schedule_d_2a.columns:
        print(col)
        
        
#selct columns
df_schedule_d_2a.head()
df_keep_d_2a = df_schedule_d_2a[['FilingID', 'Name', 'CRD Number', 'SEC Number']]

#add GES names
#row_ges = pd.DataFrame([['ACCT_NUM','WEBSITE']], columns = df_keep_d_2a.columns)
#df_keep_d_2a = pd.concat([row_ges, df_keep_d_2a]).reset_index(drop=True)
#write file
df_keep_d_2a.to_csv('soname.csv')

#Schedule D 4
csv_list_schedule_d_4 = ['IA_Schedule_D_4_20150316_20150930.csv',  'IA_Schedule_D_4_20170701_20170930.csv',
'IA_Schedule_D_4_20151001_20151231.csv', 'IA_Schedule_D_4_20171001_20171231.csv',
'IA_Schedule_D_4_20160101_20160331.csv', 'IA_Schedule_D_4_20180101_20180331.csv',
'IA_Schedule_D_4_20160401_20160630.csv', 'IA_Schedule_D_4_20180401_20180630.csv',
'IA_Schedule_D_4_20160701_20160930.csv', 'IA_Schedule_D_4_20180701_20180930.csv',
'IA_Schedule_D_4_20161001_20161231.csv', 'IA_Schedule_D_4_20181001_20181231.csv',
'IA_Schedule_D_4_20170101_20170331.csv', 'Schedule_D_4_20150315.csv',
'IA_Schedule_D_4_20170401_20170630.csv']

df_temp = pd.read_csv('IA_Schedule_D_4_20150316_20150930.csv', encoding='ISO-8859-1')
df_schedule_d_4 = pd.DataFrame(columns = df_temp.columns)

for file in csv_list_schedule_d_4:
    df_file = pd.read_csv(file, encoding='ISO-8859-1')
    df_file.index = range(len(df_schedule_d_4),(len(df_schedule_d_4) + len(df_file)))
    df_schedule_d_4 = pd.concat([df_schedule_d_4, df_file])
    print(file, ":\n")
    for col in df_schedule_d_4.columns:
        print(col)
        
        
#selct columns
df_schedule_d_4.head()
df_keep_d_4 = df_schedule_d_2a[['FilingID', 'Aquired Firm', 'SEC File Number', 'CRD Number']]

#add GES names
#row_ges = pd.DataFrame([['ACCT_NUM','WEBSITE']], columns = df_keep_d_4.columns)
#df_keep_d_4 = pd.concat([row_ges, df_keep_d_4]).reset_index(drop=True)
#write file
df_keep_d_4.to_csv('soname.csv')

#Schedule D 5G3
csv_list_schedule_d_5G3 = ['IA_Schedule_D_5G3_20150316_20150930.csv',
'IA_Schedule_D_5G3_20151001_20151231.csv',
'IA_Schedule_D_5G3_20160101_20160331.csv',
'IA_Schedule_D_5G3_20160401_20160630.csv',
'IA_Schedule_D_5G3_20160701_20160930.csv',
'IA_Schedule_D_5G3_20161001_20161231.csv',
'IA_Schedule_D_5G3_20170101_20170331.csv',
'IA_Schedule_D_5G3_20170401_20170630.csv',
'IA_Schedule_D_5G3_20170701_20170930.csv',
'IA_Schedule_D_5G3_20171001_20171231.csv',
'IA_Schedule_D_5G3_20180101_20180331.csv',
'IA_Schedule_D_5G3_20180401_20180630.csv',
'IA_Schedule_D_5G3_20180701_20180930.csv',
'IA_Schedule_D_5G3_20181001_20181231.csv',
'Schedule_D_5G3_20150315.csv']



#Schedule D 5I2
csv_list_schedule_d_5I2 = ['IA_Schedule_D_5I2_20150316_20150930.csv',
'IA_Schedule_D_5I2_20151001_20151231.csv',
'IA_Schedule_D_5I2_20160101_20160331.csv',
'IA_Schedule_D_5I2_20160401_20160630.csv',
'IA_Schedule_D_5I2_20160701_20160930.csv',
'IA_Schedule_D_5I2_20161001_20161231.csv',
'IA_Schedule_D_5I2_20170101_20170331.csv',
'IA_Schedule_D_5I2_20170401_20170630.csv',
'IA_Schedule_D_5I2_20170701_20170930.csv',
'IA_Schedule_D_5I2_20171001_20171231.csv',
'IA_Schedule_D_5I2_20180101_20180331.csv',
'IA_Schedule_D_5I2_20180401_20180630.csv',
'IA_Schedule_D_5I2_20180701_20180930.csv',
'IA_Schedule_D_5I2_20181001_20181231.csv',
'Schedule_D_5I2_20150315.csv']
#Schedule D 5K1
csv_list_schedule_d_5K1 = ['IA_Schedule_D_5K1_20171001_20171231.csv',
'IA_Schedule_D_5K1_20180101_20180331.csv',
'IA_Schedule_D_5K1_20180401_20180630.csv',
'IA_Schedule_D_5K1_20180701_20180930.csv',
'IA_Schedule_D_5K1_20181001_20181231.csv']

csv_list_schedule_d_5K2 = ['IA_Schedule_D_5K2_20171001_20171231.csv',
'IA_Schedule_D_5K2_20180101_20180331.csv',
'IA_Schedule_D_5K2_20180401_20180630.csv',
'IA_Schedule_D_5K2_20180701_20180930.csv',
'IA_Schedule_D_5K2_20181001_20181231.csv']

csv_list_schedule_d_5K3 = ['IA_Schedule_D_5K3_20171001_20171231.csv',
'IA_Schedule_D_5K3_20180101_20180331.csv',
'IA_Schedule_D_5K3_20180401_20180630.csv',
'IA_Schedule_D_5K3_20180701_20180930.csv',
'IA_Schedule_D_5K3_20181001_20181231.csv']

csv_list_schedule_d_6A = ['IA_Schedule_D_6A_20150316_20150930.csv', 'IA_Schedule_D_6A_20170701_20170930.csv',
'IA_Schedule_D_6A_20151001_20151231.csv', 'IA_Schedule_D_6A_20171001_20171231.csv'
'IA_Schedule_D_6A_20160101_20160331.csv', 'IA_Schedule_D_6A_20180101_20180331.csv'
'IA_Schedule_D_6A_20160401_20160630.csv', 'IA_Schedule_D_6A_20180401_20180630.csv'
'IA_Schedule_D_6A_20160701_20160930.csv', 'IA_Schedule_D_6A_20180701_20180930.csv'
'IA_Schedule_D_6A_20161001_20161231.csv', 'IA_Schedule_D_6A_20181001_20181231.csv'
'IA_Schedule_D_6A_20170101_20170331.csv', 'Schedule_D_6A_20150315.csv'
'IA_Schedule_D_6A_20170401_20170630.csv']

csv_list_schedule_d_6B2 = ['IA_Schedule_D_6B2_20150316_20150930.csv',
'IA_Schedule_D_6B2_20151001_20151231.csv',
'IA_Schedule_D_6B2_20160101_20160331.csv',
'IA_Schedule_D_6B2_20160401_20160630.csv',
'IA_Schedule_D_6B2_20160701_20160930.csv',
'IA_Schedule_D_6B2_20161001_20161231.csv',
'IA_Schedule_D_6B2_20170101_20170331.csv',
'IA_Schedule_D_6B2_20170401_20170630.csv',
'IA_Schedule_D_6B2_20170701_20170930.csv',
'IA_Schedule_D_6B2_20171001_20171231.csv',
'IA_Schedule_D_6B2_20180101_20180331.csv',
'IA_Schedule_D_6B2_20180401_20180630.csv',
'IA_Schedule_D_6B2_20180701_20180930.csv',
'IA_Schedule_D_6B2_20181001_20181231.csv',
'Schedule_D_6B2_20150315.csv']

csv_list_schedule_d_6B3 = ['IA_Schedule_D_6B3_20150316_20150930.csv',
'IA_Schedule_D_6B3_20151001_20151231.csv',
'IA_Schedule_D_6B3_20160101_20160331.csv',
'IA_Schedule_D_6B3_20160401_20160630.csv',
'IA_Schedule_D_6B3_20160701_20160930.csv',
'IA_Schedule_D_6B3_20161001_20161231.csv',
'IA_Schedule_D_6B3_20170101_20170331.csv',
'IA_Schedule_D_6B3_20170401_20170630.csv',
'IA_Schedule_D_6B3_20170701_20170930.csv',
'IA_Schedule_D_6B3_20171001_20171231.csv',
'IA_Schedule_D_6B3_20180101_20180331.csv',
'IA_Schedule_D_6B3_20180401_20180630.csv',
'IA_Schedule_D_6B3_20180701_20180930.csv',
'IA_Schedule_D_6B3_20181001_20181231.csv',
'Schedule_D_6B3_20150315.csv']

csv_list_schedule_d_7A =['IA_Schedule_D_7A_20150316_20150930.csv', 'IA_Schedule_D_7A_20170701_20170930.csv',
'IA_Schedule_D_7A_20151001_20151231.csv', 'IA_Schedule_D_7A_20171001_20171231.csv',
'IA_Schedule_D_7A_20160101_20160331.csv', 'IA_Schedule_D_7A_20180101_20180331.csv',
'IA_Schedule_D_7A_20160401_20160630.csv', 'IA_Schedule_D_7A_20180401_20180630.csv',
'IA_Schedule_D_7A_20160701_20160930.csv', 'IA_Schedule_D_7A_20180701_20180930.csv',
'IA_Schedule_D_7A_20161001_20161231.csv', 'IA_Schedule_D_7A_20181001_20181231.csv',
'IA_Schedule_D_7A_20170101_20170331.csv', 'Schedule_D_7A_20150315.csv',
'IA_Schedule_D_7A_20170401_20170630.csv']

csv_list_schedule_d_7A_CIK =['IA_Schedule_D_7A_CIK_20171001_20171231.csv',
'IA_Schedule_D_7A_CIK_20180101_20180331.csv',
'IA_Schedule_D_7A_CIK_20180401_20180630.csv',
'IA_Schedule_D_7A_CIK_20180701_20180930.csv',
'IA_Schedule_D_7A_CIK_20181001_20181231.csv']

csv_list_schedule_d_7A10B = ['IA_Schedule_D_7A10b_20150316_20150930.csv',
'IA_Schedule_D_7A10b_20151001_20151231.csv',
'IA_Schedule_D_7A10b_20160101_20160331.csv',
'IA_Schedule_D_7A10b_20160401_20160630.csv',
'IA_Schedule_D_7A10b_20160701_20160930.csv',
'IA_Schedule_D_7A10b_20161001_20161231.csv',
'IA_Schedule_D_7A10b_20170101_20170331.csv',
'IA_Schedule_D_7A10b_20170401_20170630.csv',
'IA_Schedule_D_7A10b_20170701_20170930.csv',
'IA_Schedule_D_7A10b_20171001_20171231.csv',
'IA_Schedule_D_7A10b_20180101_20180331.csv',
'IA_Schedule_D_7A10b_20180401_20180630.csv',
'IA_Schedule_D_7A10b_20180701_20180930.csv',
'IA_Schedule_D_7A10b_20181001_20181231.csv',
'Schedule_D_7A10B_20150315.csv']

csv_list_schedule_d_7B1 = ['IA_Schedule_D_7B1_20150316_20150930.csv',
'IA_Schedule_D_7B1_20151001_20151231.csv',
'IA_Schedule_D_7B1_20160101_20160331.csv',
'IA_Schedule_D_7B1_20160401_20160630.csv',
'IA_Schedule_D_7B1_20160701_20160930.csv',
'IA_Schedule_D_7B1_20161001_20161231.csv',
'IA_Schedule_D_7B1_20170101_20170331.csv',
'IA_Schedule_D_7B1_20170401_20170630.csv',
'IA_Schedule_D_7B1_20170701_20170930.csv',
'IA_Schedule_D_7B1_20171001_20171231.csv',
'IA_Schedule_D_7B1_20180101_20180331.csv',
'IA_Schedule_D_7B1_20180401_20180630.csv',
'IA_Schedule_D_7B1_20180701_20180930.csv',
'IA_Schedule_D_7B1_20181001_20181231.csv',
'Schedule_D_7B_20150315.csv',
'Schedule_D_7B1_20150315.csv']

csv_list_schedule_d_7B1A3 = ['IA_Schedule_D_7B1A3_20150316_20150930.csv',
'IA_Schedule_D_7B1A3_20151001_20151231.csv',
'IA_Schedule_D_7B1A3_20160101_20160331.csv',
'IA_Schedule_D_7B1A3_20160401_20160630.csv',
'IA_Schedule_D_7B1A3_20160701_20160930.csv',
'IA_Schedule_D_7B1A3_20161001_20161231.csv',
'IA_Schedule_D_7B1A3_20170101_20170331.csv',
'IA_Schedule_D_7B1A3_20170401_20170630.csv',
'IA_Schedule_D_7B1A3_20170701_20170930.csv',
'Schedule_D_7B1A3_20150315.csv']

csv_list_schedule_d_7B1A3a = ['IA_Schedule_D_7B1A3a_20171001_20171231.csv',
'IA_Schedule_D_7B1A3a_20180101_20180331.csv',
'IA_Schedule_D_7B1A3a_20180401_20180630.csv',
'IA_Schedule_D_7B1A3a_20180701_20180930.csv',
'IA_Schedule_D_7B1A3a_20181001_20181231.csv',
'IA_Schedule_D_7B1A3b_20171001_20171231.csv',
'IA_Schedule_D_7B1A3b_20180101_20180331.csv',
'IA_Schedule_D_7B1A3b_20180401_20180630.csv',
'IA_Schedule_D_7B1A3b_20180701_20180930.csv',
'IA_Schedule_D_7B1A3b_20181001_20181231.csv']

csv_list_schedule_d_7B1A5 = ['IA_Schedule_D_7B1A5_20150316_20150930.csv',
'IA_Schedule_D_7B1A5_20151001_20151231.csv',
'IA_Schedule_D_7B1A5_20160101_20160331.csv',
'IA_Schedule_D_7B1A5_20160401_20160630.csv',
'IA_Schedule_D_7B1A5_20160701_20160930.csv',
'IA_Schedule_D_7B1A5_20161001_20161231.csv',
'IA_Schedule_D_7B1A5_20170101_20170331.csv',
'IA_Schedule_D_7B1A5_20170401_20170630.csv',
'IA_Schedule_D_7B1A5_20170701_20170930.csv',
'IA_Schedule_D_7B1A5_20171001_20171231.csv',
'IA_Schedule_D_7B1A5_20180101_20180331.csv',
'IA_Schedule_D_7B1A5_20180401_20180630.csv',
'IA_Schedule_D_7B1A5_20180701_20180930.csv',
'IA_Schedule_D_7B1A5_20181001_20181231.csv',
'Schedule_D_7B1A5_20150315.csv']

csv_list_schedule_d_7B1A6b = ['IA_Schedule_D_7B1A6b_20150316_20150930.csv',
'IA_Schedule_D_7B1A6b_20151001_20151231.csv',
'IA_Schedule_D_7B1A6b_20160101_20160331.csv',
'IA_Schedule_D_7B1A6b_20160401_20160630.csv',
'IA_Schedule_D_7B1A6b_20160701_20160930.csv',
'IA_Schedule_D_7B1A6b_20161001_20161231.csv',
'IA_Schedule_D_7B1A6b_20170101_20170331.csv',
'IA_Schedule_D_7B1A6b_20170401_20170630.csv',
'IA_Schedule_D_7B1A6b_20170701_20170930.csv',
'IA_Schedule_D_7B1A6b_20171001_20171231.csv',
'IA_Schedule_D_7B1A6b_20180101_20180331.csv',
'IA_Schedule_D_7B1A6b_20180401_20180630.csv',
'IA_Schedule_D_7B1A6b_20180701_20180930.csv',
'IA_Schedule_D_7B1A6b_20181001_20181231.csv',
'Schedule_D_7B1A6B_20150315.csv']

csv_list_schedule_d_7B1A7 = ['IA_Schedule_D_7B1A7_20150316_20150930.csv',
'IA_Schedule_D_7B1A7_20151001_20151231.csv',
'IA_Schedule_D_7B1A7_20160101_20160331.csv',
'IA_Schedule_D_7B1A7_20160401_20160630.csv',
'IA_Schedule_D_7B1A7_20160701_20160930.csv',
'IA_Schedule_D_7B1A7_20161001_20161231.csv',
'IA_Schedule_D_7B1A7_20170101_20170331.csv',
'IA_Schedule_D_7B1A7_20170401_20170630.csv',
'IA_Schedule_D_7B1A7_20170701_20170930.csv',
'IA_Schedule_D_7B1A7_20171001_20171231.csv',
'IA_Schedule_D_7B1A7_20180101_20180331.csv',
'IA_Schedule_D_7B1A7_20180401_20180630.csv',
'IA_Schedule_D_7B1A7_20180701_20180930.csv',
'IA_Schedule_D_7B1A7_20181001_20181231.csv',
'Schedule_D_7B1A7_20150315.csv']

csv_list_schedule_d_7B1A7d = ['IA_Schedule_D_7B1A7d_20150316_20150930.csv',
'IA_Schedule_D_7B1A7d_20151001_20151231.csv',
'IA_Schedule_D_7B1A7d_20160101_20160331.csv',
'IA_Schedule_D_7B1A7d_20160401_20160630.csv',
'IA_Schedule_D_7B1A7d_20160701_20160930.csv',
'IA_Schedule_D_7B1A7d_20161001_20161231.csv',
'IA_Schedule_D_7B1A7d_20170101_20170331.csv',
'IA_Schedule_D_7B1A7d_20170401_20170630.csv',
'IA_Schedule_D_7B1A7d_20170701_20170930.csv',
'Schedule_D_7B1A7D_20150315.csv']

csv_list_schedule_d_7B1A7d1 = ['IA_Schedule_D_7B1A7d1_20171001_20171231.csv',
'IA_Schedule_D_7B1A7d1_20180101_20180331.csv',
'IA_Schedule_D_7B1A7d1_20180401_20180630.csv',
'IA_Schedule_D_7B1A7d1_20180701_20180930.csv',
'IA_Schedule_D_7B1A7d1_20181001_20181231.csv']

csv_list_schedule_d_7B1A7d2 = ['IA_Schedule_D_7B1A7d2_20171001_20171231.csv',
'IA_Schedule_D_7B1A7d2_20180101_20180331.csv',
'IA_Schedule_D_7B1A7d2_20180401_20180630.csv',
'IA_Schedule_D_7B1A7d2_20180701_20180930.csv',
'IA_Schedule_D_7B1A7d2_20181001_20181231.csv']

csv_list_schedule_d_7B1A7f = ['IA_Schedule_D_7B1A7f_20150316_20150930.csv',
'IA_Schedule_D_7B1A7f_20151001_20151231.csv',
'IA_Schedule_D_7B1A7f_20160101_20160331.csv',
'IA_Schedule_D_7B1A7f_20160401_20160630.csv',
'IA_Schedule_D_7B1A7f_20160701_20160930.csv',
'IA_Schedule_D_7B1A7f_20161001_20161231.csv',
'IA_Schedule_D_7B1A7f_20170101_20170331.csv',
'IA_Schedule_D_7B1A7f_20170401_20170630.csv',
'IA_Schedule_D_7B1A7f_20170701_20170930.csv',
'IA_Schedule_D_7B1A7f_20171001_20171231.csv',
'IA_Schedule_D_7B1A7f_20180101_20180331.csv',
'IA_Schedule_D_7B1A7f_20180401_20180630.csv',
'IA_Schedule_D_7B1A7f_20180701_20180930.csv',
'IA_Schedule_D_7B1A7f_20181001_20181231.csv',
'Schedule_D_7B1A7F_20150315.csv']

csv_list_schedule_d_7B1A17b = ['IA_Schedule_D_7B1A17b_20150316_20150930.csv',
'IA_Schedule_D_7B1A17b_20151001_20151231.csv',
'IA_Schedule_D_7B1A17b_20160101_20160331.csv',
'IA_Schedule_D_7B1A17b_20160401_20160630.csv',
'IA_Schedule_D_7B1A17b_20160701_20160930.csv',
'IA_Schedule_D_7B1A17b_20161001_20161231.csv',
'IA_Schedule_D_7B1A17b_20170101_20170331.csv',
'IA_Schedule_D_7B1A17b_20170401_20170630.csv',
'IA_Schedule_D_7B1A17b_20170701_20170930.csv',
'IA_Schedule_D_7B1A17b_20171001_20171231.csv',
'IA_Schedule_D_7B1A17b_20180101_20180331.csv',
'IA_Schedule_D_7B1A17b_20180401_20180630.csv',
'IA_Schedule_D_7B1A17b_20180701_20180930.csv',
'IA_Schedule_D_7B1A17b_20181001_20181231.csv',
'Schedule_D_7B1A17B_20150315.csv']

csv_list_schedule_d_7B1A18b = ['IA_Schedule_D_7B1A18b_20150316_20150930.csv',
'IA_Schedule_D_7B1A18b_20151001_20151231.csv',
'IA_Schedule_D_7B1A18b_20160101_20160331.csv',
'IA_Schedule_D_7B1A18b_20160401_20160630.csv',
'IA_Schedule_D_7B1A18b_20160701_20160930.csv',
'IA_Schedule_D_7B1A18b_20161001_20161231.csv',
'IA_Schedule_D_7B1A18b_20170101_20170331.csv',
'IA_Schedule_D_7B1A18b_20170401_20170630.csv',
'IA_Schedule_D_7B1A18b_20170701_20170930.csv',
'IA_Schedule_D_7B1A18b_20171001_20171231.csv',
'IA_Schedule_D_7B1A18b_20180101_20180331.csv',
'IA_Schedule_D_7B1A18b_20180401_20180630.csv',
'IA_Schedule_D_7B1A18b_20180701_20180930.csv',
'IA_Schedule_D_7B1A18b_20181001_20181231.csv',
'Schedule_D_7B1A18B_20150315.csv']

csv_list_schedule_d_7B1A22 = ['IA_Schedule_D_7B1A22_20150316_20150930.csv',
'IA_Schedule_D_7B1A22_20151001_20151231.csv',
'IA_Schedule_D_7B1A22_20160101_20160331.csv',
'IA_Schedule_D_7B1A22_20160401_20160630.csv',
'IA_Schedule_D_7B1A22_20160701_20160930.csv',
'IA_Schedule_D_7B1A22_20161001_20161231.csv',
'IA_Schedule_D_7B1A22_20170101_20170331.csv',
'IA_Schedule_D_7B1A22_20170401_20170630.csv',
'IA_Schedule_D_7B1A22_20170701_20170930.csv',
'IA_Schedule_D_7B1A22_20171001_20171231.csv',
'IA_Schedule_D_7B1A22_20180101_20180331.csv',
'IA_Schedule_D_7B1A22_20180401_20180630.csv',
'IA_Schedule_D_7B1A22_20180701_20180930.csv',
'IA_Schedule_D_7B1A22_20181001_20181231.csv',
'Schedule_D_7B1A22_20150315.csv']

csv_list_schedule_d_7B1A23 = ['IA_Schedule_D_7B1A23_20150316_20150930.csv',
'IA_Schedule_D_7B1A23_20151001_20151231.csv',
'IA_Schedule_D_7B1A23_20160101_20160331.csv',
'IA_Schedule_D_7B1A23_20160401_20160630.csv',
'IA_Schedule_D_7B1A23_20160701_20160930.csv',
'IA_Schedule_D_7B1A23_20161001_20161231.csv',
'IA_Schedule_D_7B1A23_20170101_20170331.csv',
'IA_Schedule_D_7B1A23_20170401_20170630.csv',
'IA_Schedule_D_7B1A23_20170701_20170930.csv',
'IA_Schedule_D_7B1A23_20171001_20171231.csv',
'IA_Schedule_D_7B1A23_20180101_20180331.csv',
'IA_Schedule_D_7B1A23_20180401_20180630.csv',
'IA_Schedule_D_7B1A23_20180701_20180930.csv',
'IA_Schedule_D_7B1A23_20181001_20181231.csv',
'Schedule_D_7B1A23_20150315.csv']

csv_list_schedule_d_7B1A24 = ['IA_Schedule_D_7B1A24_20150316_20150930.csv',
'IA_Schedule_D_7B1A24_20151001_20151231.csv',
'IA_Schedule_D_7B1A24_20160101_20160331.csv',
'IA_Schedule_D_7B1A24_20160401_20160630.csv',
'IA_Schedule_D_7B1A24_20160701_20160930.csv',
'IA_Schedule_D_7B1A24_20161001_20161231.csv',
'IA_Schedule_D_7B1A24_20170101_20170331.csv',
'IA_Schedule_D_7B1A24_20170401_20170630.csv',
'IA_Schedule_D_7B1A24_20170701_20170930.csv',
'IA_Schedule_D_7B1A24_20171001_20171231.csv',
'IA_Schedule_D_7B1A24_20180101_20180331.csv',
'IA_Schedule_D_7B1A24_20180401_20180630.csv',
'IA_Schedule_D_7B1A24_20180701_20180930.csv',
'IA_Schedule_D_7B1A24_20181001_20181231.csv',
'Schedule_D_7B1A24_20150315.csv']

csv_list_schedule_d_7B1A25 = ['IA_Schedule_D_7B1A25_20150316_20150930.csv',
'IA_Schedule_D_7B1A25_20151001_20151231.csv',
'IA_Schedule_D_7B1A25_20160101_20160331.csv',
'IA_Schedule_D_7B1A25_20160401_20160630.csv',
'IA_Schedule_D_7B1A25_20160701_20160930.csv',
'IA_Schedule_D_7B1A25_20161001_20161231.csv',
'IA_Schedule_D_7B1A25_20170101_20170331.csv',
'IA_Schedule_D_7B1A25_20170401_20170630.csv',
'IA_Schedule_D_7B1A25_20170701_20170930.csv',
'IA_Schedule_D_7B1A25_20171001_20171231.csv',
'IA_Schedule_D_7B1A25_20180101_20180331.csv',
'IA_Schedule_D_7B1A25_20180401_20180630.csv',
'IA_Schedule_D_7B1A25_20180701_20180930.csv',
'IA_Schedule_D_7B1A25_20181001_20181231.csv',
'Schedule_D_7B1A25_20150315.csv']

csv_list_schedule_d_7B1A26 = ['IA_Schedule_D_7B1A26_20150316_20150930.csv',
'IA_Schedule_D_7B1A26_20151001_20151231.csv',
'IA_Schedule_D_7B1A26_20160101_20160331.csv',
'IA_Schedule_D_7B1A26_20160401_20160630.csv',
'IA_Schedule_D_7B1A26_20160701_20160930.csv',
'IA_Schedule_D_7B1A26_20161001_20161231.csv',
'IA_Schedule_D_7B1A26_20170101_20170331.csv',
'IA_Schedule_D_7B1A26_20170401_20170630.csv',
'IA_Schedule_D_7B1A26_20170701_20170930.csv',
'IA_Schedule_D_7B1A26_20171001_20171231.csv',
'IA_Schedule_D_7B1A26_20180101_20180331.csv',
'IA_Schedule_D_7B1A26_20180401_20180630.csv',
'IA_Schedule_D_7B1A26_20180701_20180930.csv',
'IA_Schedule_D_7B1A26_20181001_20181231.csv',
'Schedule_D_7B1A26_20150315.csv']

csv_list_schedule_d_7B1A28 = ['IA_Schedule_D_7B1A28_20150316_20150930.csv',
'IA_Schedule_D_7B1A28_20151001_20151231.csv',
'IA_Schedule_D_7B1A28_20160101_20160331.csv',
'IA_Schedule_D_7B1A28_20160401_20160630.csv',
'IA_Schedule_D_7B1A28_20160701_20160930.csv',
'IA_Schedule_D_7B1A28_20161001_20161231.csv',
'IA_Schedule_D_7B1A28_20170101_20170331.csv',
'IA_Schedule_D_7B1A28_20170401_20170630.csv',
'IA_Schedule_D_7B1A28_20170701_20170930.csv',
'IA_Schedule_D_7B1A28_20171001_20171231.csv',
'IA_Schedule_D_7B1A28_20180101_20180331.csv',
'IA_Schedule_D_7B1A28_20180401_20180630.csv',
'IA_Schedule_D_7B1A28_20180701_20180930.csv',
'IA_Schedule_D_7B1A28_20181001_20181231.csv',
'Schedule_D_7B1A28_20150315.csv']

csv_list_schedule_d_7B1A28_Websites = ['IA_Schedule_D_7B1A28_websites_20150316_20150930.csv'
'IA_Schedule_D_7B1A28_websites_20151001_20151231.csv'
'IA_Schedule_D_7B1A28_websites_20160101_20160331.csv'
'IA_Schedule_D_7B1A28_websites_20160401_20160630.csv'
'IA_Schedule_D_7B1A28_websites_20160701_20160930.csv'
'IA_Schedule_D_7B1A28_websites_20161001_20161231.csv'
'IA_Schedule_D_7B1A28_websites_20170101_20170331.csv'
'IA_Schedule_D_7B1A28_websites_20170401_20170630.csv'
'IA_Schedule_D_7B1A28_websites_20170701_20170930.csv'
'IA_Schedule_D_7B1A28_websites_20171001_20171231.csv'
'IA_Schedule_D_7B1A28_websites_20180101_20180331.csv'
'IA_Schedule_D_7B1A28_websites_20180401_20180630.csv'
'IA_Schedule_D_7B1A28_websites_20180701_20180930.csv'
'IA_Schedule_D_7B1A28_websites_20181001_20181231.csv'
'Schedule_D_7B1A28Websites_20150315.csv']

csv_list_schedule_d_7B2 = ['IA_Schedule_D_7B2_20150316_20150930.csv',
'IA_Schedule_D_7B2_20151001_20151231.csv',
'IA_Schedule_D_7B2_20160101_20160331.csv',
'IA_Schedule_D_7B2_20160401_20160630.csv',
'IA_Schedule_D_7B2_20160701_20160930.csv',
'IA_Schedule_D_7B2_20161001_20161231.csv',
'IA_Schedule_D_7B2_20170101_20170331.csv',
'IA_Schedule_D_7B2_20170401_20170630.csv',
'IA_Schedule_D_7B2_20170701_20170930.csv',
'IA_Schedule_D_7B2_20171001_20171231.csv',
'IA_Schedule_D_7B2_20180101_20180331.csv',
'IA_Schedule_D_7B2_20180401_20180630.csv',
'IA_Schedule_D_7B2_20180701_20180930.csv',
'IA_Schedule_D_7B2_20181001_20181231.csv',
'Schedule_D_7B2_20150315.csv']

csv_list_schedule_d_10A = ['IA_Schedule_D_10A_20150316_20150930.csv',
'IA_Schedule_D_10A_20151001_20151231.csv',
'IA_Schedule_D_10A_20160101_20160331.csv',
'IA_Schedule_D_10A_20160401_20160630.csv',
'IA_Schedule_D_10A_20160701_20160930.csv',
'IA_Schedule_D_10A_20161001_20161231.csv',
'IA_Schedule_D_10A_20170101_20170331.csv',
'IA_Schedule_D_10A_20170401_20170630.csv',
'IA_Schedule_D_10A_20170701_20170930.csv',
'IA_Schedule_D_10A_20171001_20171231.csv',
'IA_Schedule_D_10A_20180101_20180331.csv',
'IA_Schedule_D_10A_20180401_20180630.csv',
'IA_Schedule_D_10A_20180701_20180930.csv',
'IA_Schedule_D_10A_20181001_20181231.csv',
'Schedule_D_10A_20150315.csv']

csv_list_schedule_d_10B = ['IA_Schedule_D_10B_20150316_20150930.csv',
'IA_Schedule_D_10B_20151001_20151231.csv',
'IA_Schedule_D_10B_20160101_20160331.csv',
'IA_Schedule_D_10B_20160401_20160630.csv',
'IA_Schedule_D_10B_20160701_20160930.csv',
'IA_Schedule_D_10B_20161001_20161231.csv',
'IA_Schedule_D_10B_20170101_20170331.csv',
'IA_Schedule_D_10B_20170401_20170630.csv',
'IA_Schedule_D_10B_20170701_20170930.csv',
'IA_Schedule_D_10B_20171001_20171231.csv',
'IA_Schedule_D_10B_20180101_20180331.csv',
'IA_Schedule_D_10B_20180401_20180630.csv',
'IA_Schedule_D_10B_20180701_20180930.csv',
'IA_Schedule_D_10B_20181001_20181231.csv',
'Schedule_D_10B_20150315.csv']

csv_list_schedule_d_Books_and_Records = ['IA_Schedule_D_Books_and_Records_20150315_20150316_20150930.csv',
'IA_Schedule_D_Books_and_Records_20150315_20151001_20151231.csv',
'IA_Schedule_D_Books_and_Records_20160101_20160331.csv',
'IA_Schedule_D_Books_and_Records_20160401_20160630.csv',
'IA_Schedule_D_Books_and_Records_20160701_20160930.csv',
'IA_Schedule_D_Books_and_Records_20161001_20161231.csv',
'IA_Schedule_D_Books_and_Records_20170101_20170331.csv',
'IA_Schedule_D_Books_and_Records_20170401_20170630.csv',
'IA_Schedule_D_Books_and_Records_20170701_20170930.csv',
'IA_Schedule_D_Books_and_Records_20171001_20171231.csv',
'IA_Schedule_D_Books_and_Records_20180101_20180331.csv',
'IA_Schedule_D_Books_and_Records_20180401_20180630.csv',
'IA_Schedule_D_Books_and_Records_20180701_20180930.csv',
'IA_Schedule_D_Books_and_Records_20181001_20181231.csv',
'Schedule_D_Books_and_Records_20150315.csv']

csv_list_schedule_d_Foreign_Regulatory_Authority = ['IA_Schedule_D_Foreign_Regulatory_Authority_20150316_20150930.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20160701_20160930.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20161001_20161231.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20170101_20170331.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20170401_20170630.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20170701_20170930.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20171001_20171231.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20180101_20180331.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20180401_20180630.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20180701_20180930.csv',
'IA_Schedule_D_Foreign_Regulatory_Authority_20181001_20181231.csv',
'Schedule_D_Foreign_Regulatory_Authority_20150315.csv',
'Schedule_D_Foreign_Regulatory_Authority_20151001_20151231.csv',
'Schedule_D_Foreign_Regulatory_Authority_20160101_20160331.csv',
'Schedule_D_Foreign_Regulatory_Authority_20160401_20160630.csv']

csv_list_schedule_d_Miscellaneous = ['IA_Schedule_D_Miscellaneous_20150316_20150930.csv',
'IA_Schedule_D_Miscellaneous_20160101_20160331.csv',
'IA_Schedule_D_Miscellaneous_20160401_20160630.csv',
'IA_Schedule_D_Miscellaneous_20160701_20160930.csv',
'IA_Schedule_D_Miscellaneous_20161001_20161231.csv',
'IA_Schedule_D_Miscellaneous_20170101_20170331.csv',
'IA_Schedule_D_Miscellaneous_20170401_20170630.csv',
'IA_Schedule_D_Miscellaneous_20170701_20170930.csv',
'IA_Schedule_D_Miscellaneous_20171001_20171231.csv',
'IA_Schedule_D_Miscellaneous_20180101_20180331.csv',
'IA_Schedule_D_Miscellaneous_20180401_20180630.csv',
'IA_Schedule_D_Miscellaneous_20180701_20180930.csv',
'IA_Schedule_D_Miscellaneous_20181001_20181231.csv',
'Schedule_D_Misc_A_20150315.csv',
'Schedule_D_Misc_B_20150315.csv',
'Schedule_D_Misc_C_20150315.csv',
'Schedule_D_Misc_D_20150315.csv']

csv_list_DRP_Advisory_Affiliates = ['IA_DRP_Advisory_Affiliates_20150316_20150930.csv',
'IA_DRP_Advisory_Affiliates_20151001_20151231.csv',
'IA_DRP_Advisory_Affiliates_20160101_20160331.csv',
'IA_DRP_Advisory_Affiliates_20160401_20160630.csv',
'IA_DRP_Advisory_Affiliates_20160701_20160930.csv',
'IA_DRP_Advisory_Affiliates_20161001_20161231.csv',
'IA_DRP_Advisory_Affiliates_20170101_20170331.csv',
'IA_DRP_Advisory_Affiliates_20170401_20170630.csv',
'IA_DRP_Advisory_Affiliates_20170701_20170930.csv',
'IA_DRP_Advisory_Affiliates_20171001_20171231.csv',
'IA_DRP_Advisory_Affiliates_20180101_20180331.csv',
'IA_DRP_Advisory_Affiliates_20180401_20180630.csv',
'IA_DRP_Advisory_Affiliates_20180701_20180930.csv',
'IA_DRP_Advisory_Affiliates_20181001_20181231.csv']

csv_list_Schedule_R_Firm_Download = ['IA_Firm_Download_SCH_R_20180105.csv', 'IA_Firm_Download_SCH_R_20181004.csv',
'IA_Firm_Download_SCH_R_20180405.csv', 'IA_Firm_Download_SCH_R_20190130.csv',
'IA_Firm_Download_SCH_R_20180705.csv']

csv_list_Schedule_R_Other_Business_Names = ['IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20180105.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20180405.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20180705.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20181004.csv',
'IA_SCH_R_OTHER_BUSINESS_NAMES_1C_20190130.csv']

csv_list_Schedule_R_4A_4B = ['IA_SCH_R_4A_4B_20180105.csv', 'IA_SCH_R_4A_4B_20181004.csv',
'IA_SCH_R_4A_4B_20180405.csv', 'IA_SCH_R_4A_4B_20190130.csv',
'IA_SCH_R_4A_4B_20180705.csv']

csv_list_Schedule_R_4C = ['IA_Schedule_R_4C_20171001_20171231.csv', 'IA_Schedule_R_4C_20180701_20180930.csv',
'IA_Schedule_R_4C_20180101_20180331.csv', 'IA_Schedule_R_4C_20181001_20181231.csv',
'IA_Schedule_R_4C_20180401_20180630.csv']

csv_list_Schedule_R_4D = ['IA_SCH_R_4D_20171001_20171231.csv', 'IA_SCH_R_4D_20180701_20180930.csv',
'IA_SCH_R_4D_20180101_20180331.csv', 'IA_SCH_R_4D_20181001_20181231.csv',
'IA_SCH_R_4D_20180401_20180630.csv']