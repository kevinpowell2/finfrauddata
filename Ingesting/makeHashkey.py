# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 22:22:11 2019

@author: KevinPowell
"""

import os
os.chdir('C:/Users/KevinPowell/Documents/Intern2019/FincFraudData/finfrauddata/Data/FINRA data/Processed Data')
import json

import pandas as pd
import numpy as np
#returns list of unique column entries
def distinctCount(seq): # Order preserving
    cnt = 0
    i = 0
    lst = np.empty(len(seq))
    for j in seq:
        if j not in lst:
            lst[i]=j
            i+=1
            cnt+=1
        if cnt%1000== 0:
            print(cnt)
    return lst[0:cnt]



#read file
df_org = pd.read_csv('BASE_ORG.csv')

list(df_org.columns)

#column names to match on
match_list = ['LEGALNAME_NAME_ORG',
 'BUSINESSNAME_NAME_ORG',
 'SEC_NUMBER',
 'CRD_NUMBER',
 'OFFICECITY_ADDR_CITY',
 'OFFICECOUNTRY_ADDR_COUNTRY',
 'OFFICEZIP_ADDR_POSTAL',
 'OFFICESTATE_ADDR_STATE',
 'OFFICESTREET1_ADDR_LINE1',
 'OFFICESTREET2_ADDR_LINE2',
 'OFFICEPHONE_PHONE_NUMBER',
 'OFFICEFAX_PHONE_NUMBER',
 'MAILINGCITY_ADDR_CITY',
 'MAILINGCOUNTRY_ADDR_COUNTRY',
 'MAILINGZIP_ADDR_POSTAL',
 'MAILINGSTATE_ADDR_STATE',
 'MAILINGSTREET1_ADDR_LINE1',
 'MAILINGSTREET2_ADDR_LINE2',
 'CIK_NUMBER',
 'LEI_NUMBER']

#make hash
#have to drop last 24 characters, str lists row index and hashkeys are not identical otherwise
for i in range(len(df_org)):
    df_org.loc[i, 'ORGHASHKEY'] = hash(str(df_org.loc[i,match_list]).strip().replace(' ','')[:-24])
    if i%1000== 0:
              print(i)

Hashkey_list = distinctCount(df_org['ORGHASHKEY'])


#Hash CCO file

df_cco = pd.read_csv('BASE_CCO.csv')

list(df_cco.columns)

match_cco = ['CCO_ADDR_CITY',
 'CCO_ADDR_COUNTRY',
 'CCO_EMAIL_ADDRESS',
 'CCO_PHONE_NUMBER',
 'CCO_NAME_FULL',
 'CCO_PHONE_NUMBER.1',
 'CCO_ADDR_POSTAL',
 'CCO_ADDR_STATE',
 'CCO_ADDR_LINE1',
 'CCO_ADDR_LINE2',
 'JOBTITLES']

for i in range(len(df_cco)):
    df_cco.loc[i, 'CCOHASHKEY'] = hash(str(df_cco.loc[i,match_cco]).strip().replace(' ','')[:-24])
    if i%1000== 0:
              print(i)
              
Hashkey_list_cco = distinctCount(df_cco['CCOHASHKEY'])

#Hash REG file

df_reg = pd.read_csv('BASE_REG.csv')

list(df_reg.columns)

match_reg = ['REG_ADDR_CITY',
 'REG_ADDR_COUNTRY',
 'REG_EMAIL_ADDRESS',
 'REGFAX_PHONE_NUMBER',
 'REG_NAME_FULL',
 'REG_PHONE_NUMBER',
 'REG_ADDR_POSTAL',
 'REG_ADDR_STATE',
 'REG_ADDR_LINE1',
 'REG_ADDR_LINE2']

for i in range(len(df_reg)):
    df_reg.loc[i, 'REGHASHKEY'] = hash(str(df_reg.loc[i,match_reg]).strip().replace(' ','')[:-24])
    if i%1000== 0:
              print(i)
              
Hashkey_list_reg = distinctCount(df_reg['REGHASHKEY'])

df_reg.to_csv('BASE_REG_HASH.csv')
pd.DataFrame(Hashkey_list_reg).to_csv('REG_HASH_LIST.csv')

#Hash SIG file

df_sig = pd.read_csv('BASE_SIG.csv')
date_idx = pd.read_csv('Date_FileID_INDX.csv')
org_idx = pd.read_csv('Name_FileID_INDX.csv')

cnt = 0
df_sig = pd.merge(df_sig[['SIGNATORY','TITLE','FILINGID']], org_idx[['FILINGID','BUSINESSNAME_NAME_ORG']], on = 'FILINGID', how = 'right')              

list(df_sig.columns)

match_sig = ['SIGNATORY', 'TITLE', 'BUSINESSNAME_NAME_ORG']

for i in range(len(df_sig)):
    df_sig.loc[i, 'SIGHASHKEY'] = hash(str(df_sig.loc[i,match_sig]).strip().replace(' ','')[:-24])
    if i%1000== 0:
              print(i)
Hashkey_list_sig = distinctCount(df_sig['SIGHASHKEY'])

df_sig.to_csv('BASE_SIG_HASH.csv')
pd.DataFrame(Hashkey_list_sig).to_csv('SIG_HASH_LIST.csv')