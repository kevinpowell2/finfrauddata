# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 08:59:31 2019

@author: KevinPowell
"""

import os
os.chdir('C:/Users/KevinPowell/Documents/Intern2019/FincFraudData/finfrauddata/Data/FINRA data/Processed Data')
import pandas as pd
import numpy as np
import json

#read in files
df_org = pd.read_csv('BASE_ORG_HASH.csv')
hash_list = pd.read_csv('ORG_HASH_LIST.csv')

cnt = 0
org_dict = {'ENTITIES': {}}
for key in hash_list['0']:
    df_line = df_org[df_org['ORGHASHKEY']==key]
    org_dict['ENTITIES'].update(  {str(cnt): {
    "RECORD_ID"                : key,
    "NAMES"                    : [{ "NAME_TYPE": "BUSINESS", "NAME_ORG": df_line.head(1).BUSINESSNAME_NAME_ORG.item() },
                                 { "NAME_TYPE": "LEGAL",    "NAME_ORG": df_line.head(1).LEGALNAME_NAME_ORG.item()        }],
    "UMBRELLAREGISTRATION"     : df_line.UMBRELLAREGISTRATION.tolist(),
    'BUSNAMECHANGE'            : df_line.BUSNAMECHANGE.tolist(),
    'LEGNMECHNG'               : df_line.LEGALNAMECHANGE.tolist(),
    'NEW_NME'                  : df_line.NEWNAME_NAME_ORG.tolist(),
    'SEC_NUMBER'               : df_line.head(1).SEC_NUMBER.item(),
    'CRD_NUMBER'               : df_line.head(1).CRD_NUMBER.item(),
    'ADDRESSES'                : [{'ADDR_TYPE'       : 'OFFICE',
                                  'ADDR_CITY'       : df_line.head(1).OFFICECITY_ADDR_CITY.item(),
                                  'ADDR_COUNTRY'    : df_line.head(1).OFFICECOUNTRY_ADDR_COUNTRY.item(),
                                  'ADDR_POSTAL_CODE': df_line.head(1).OFFICEZIP_ADDR_POSTAL.item(),
                                  'ADDR_STATE'      : df_line.head(1).OFFICESTATE_ADDR_STATE.item(),
                                  'ADDR_LINE1'      : df_line.head(1).OFFICESTREET1_ADDR_LINE1.item(),
                                  'ADDR_LINE2'      : df_line.head(1).OFFICESTREET2_ADDR_LINE2.item()
                                 },
                                 {'ADDR_TYPE'       : 'MAILING',
                                  'ADDR_CITY'       : df_line.head(1).MAILINGCITY_ADDR_CITY.item(),
                                  'ADDR_COUNTRY'    : df_line.head(1).MAILINGCOUNTRY_ADDR_COUNTRY.item(),
                                  'ADDR_POSTAL_CODE': df_line.head(1).MAILINGZIP_ADDR_POSTAL.item(),
                                  'ADDR_STATE'      : df_line.head(1).MAILINGSTATE_ADDR_STATE.item(),
                                  'ADDR_LINE1'      : df_line.head(1).MAILINGSTREET1_ADDR_LINE1.item(),
                                  'ADDR_LINE2'      : df_line.head(1).MAILINGSTREET2_ADDR_LINE2.item()
                                 }],
    'PHONENUMS'                : [{'PHONE_TYPE'  : 'OFFICE',
                                  'PHONE_NUMBER': df_line.head(1).OFFICEPHONE_PHONE_NUMBER.item()
                                 },
                                 {'PHONE_TYPE'  : 'FAX',
                                  'PHONE_NUMBER': df_line.head(1).OFFICEFAX_PHONE_NUMBER.item()}
                                 ],
    'NUMBEROFFICES'            : df_line.NUMBEROFFICES.tolist(),
    'PUBLICCOMPANY'            : df_line.PUBLICCOMPANY.tolist(),
    'CIK_NUMBER'               : df_line.head(1).CIK_NUMBER.item(),
    '1BILLIONASSETS'           : df_line['1BILLIONASSETS'].tolist(),
    'LEI_NUMBER'               : df_line.head(1).LEI_NUMBER.item(),
    'SUCCESSIONDATE'           : df_line.SUCCESSIONDATE.tolist(),
    'NUMEMPLOYEESIA'           : df_line.NUMEMPLOYEESIA.tolist(),
    'NUMEMPLOYEESBROKERS'      : df_line.NUMEMPLOYEESBROKERS.tolist(),
    'NUMCLIENTSIA'             : df_line.NUMCLIENTSIA.tolist(),
    'PERCENTCLIENTSNONUS'      : df_line.PERCENTCLIENTSNONUS.tolist(),
    'OTHERCLIENTS'             : df_line.OTHERCLIENTS.tolist(),
    'MANAGESECURITIES'         : df_line.MANAGESECURITIES.tolist(),
    'DISCRETIONARYASSETS'      : df_line.DISCRETIONARYASSETS.tolist(),
    'NONDISCRETIONARYASSETS'   : df_line.NONDISCRETIONARYASSETS.tolist(),
    'NUMDISCRETIONARYACCTS'    : df_line.NUMDISCRETIONARYACCTS.tolist(),
    'OTHERBUSACTIVITY'         : df_line.OTHERBUSACTIVITY.tolist(),
    'AMTCLIENTFUNDSCUSTODY'    : df_line.AMTCLIENTFUNDSCUSTODY.tolist(),
    'NUMCLIENTSCUSTODY'        : df_line.NUMCLIENTSCUSTODY.tolist(),
    'RELATEDCLIENTFUNDSCUSTODY': df_line.RELATEDCLIENTFUNDSCUSTODY.tolist(),
    'RELATEDNUMCLIENTSCUSTODY' : df_line.RELATEDNUMCLIENTSCUSTODY.tolist(),
    'ACCOUNTINGEXAMDATE'       : df_line.ACCOUNTINGEXAMDATE.tolist(),
    'BUSINESSACTIVITIES'       : df_line.BUSINESSACTIVITIES.tolist(),
    'CLIENTS'                  : df_line.CLIENTS.tolist(),
    'CONFLICTINTEREST'         : df_line.CONFLICTINTEREST.tolist(),
    'CUSTODY'                  : df_line.CUSTODY.tolist(),
    'DISCLOSURES'              : df_line.DISCLOSURES.tolist(),
    'EXECUTIONDATE'            : df_line.EXECUTIONDATE.tolist(),
    'EXECUTIONTYPE'            : df_line.EXECUTIONTYPE.tolist(),
    'EXEMPTIONREASONS'         : df_line.EXEMPTIONREASONS.tolist(),
    'FILINGID'                 : df_line.FILINGID.tolist(),
    'REGREASONS'               : df_line.REGREASONS.tolist() 
    }})
    cnt +=1
    if cnt%1000== 0:
              print(cnt)


#fix missing num NonDiscrentionary accounts

for x in org_dict['ENTITIES']:
    tem_lst = []
    for j in org_dict['ENTITIES'][x]['FILINGID']:
        tem_lst.append(df_org[df_org['FILINGID']==j]['NUMNONDISCRETIONARYACCTS'].item())
    org_dict['ENTITIES'][x].update(NUMNONDISCRETIONARYACCTS = tem_lst)
    if int(x)%1000== 0:
              print(x)
#testing out dict formats
'''
for key in hash_list['0']:
    df_line = df_org[df_org['ORGHASHKEY']==key]
    org_dict['ENTITIES'].update(  {str(cnt): {
    "RECORD_ID"                : key,
    "NAMES"                    : [{ "NAME_TYPE": "BUSINESS", "NAME_ORG": df_line.head(1).BUSINESSNAME_NAME_ORG.item() },
                                 { "NAME_TYPE": "LEGAL",    "NAME_ORG": df_line.head(1).LEGALNAME_NAME_ORG.item()        }],
    "UMBRELLAREGISTRATION"     : df_line.UMBRELLAREGISTRATION.tolist()
    }})
    cnt +=1
    if cnt%1000== 0:
              print(cnt)
'''
#    	"RELATIONSHIPS": [{"RELATIONSHIP_KEY":"20012002", "RELATIONSHIP_TYPE": "1 to 2", "RELATIONSHP_ROLE": "1"},
#    					  {"RELATIONSHIP_KEY":"20012003", "RELATIONSHIP_TYPE": "1 to 3", "RELATIONSHP_ROLE": "1"}],
import pprint as pp
pp.pprint(org_dict['ENTITIES']['92242']['NAMES'])

'''
#write out file for senzing
with open('org.json', 'a') as fp:
    for j in org_dict['ENTITIES']:
        json.dump(j, fp)
        fp.write('\n')
#save dict   
with open('org_saved.json', 'w') as fp:
    json.dump(org_dict,fp)        

with open('org_saved.json', 'r') as fr:
    org_dict = json.load(fr)
'''